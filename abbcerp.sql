-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2018 at 03:37 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abbcerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountexpenses`
--

CREATE TABLE `accountexpenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `openbalance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateofexpense` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `particulars` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vch_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vch_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `debit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `report_duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_indirect_expense` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accountexpenses`
--

INSERT INTO `accountexpenses` (`id`, `report_id`, `branch_id`, `openbalance`, `payments`, `amount`, `dateofexpense`, `particulars`, `vch_type`, `vch_no`, `debit`, `report_duration`, `total_indirect_expense`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '1000000', 'Indirect Expenses', '', '', '', '', '', '', '12/09/2018 - 12/09/2018', '900000', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(2, '1', '1', '', 'Accountant Fee', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(3, '1', '1', '', 'Apartment Rent', '150000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(4, '1', '1', '', 'Apartment Wifi & Other Utilities', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(5, '1', '1', '', 'Article Publish & Advertising Expenses', '2000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(6, '1', '1', '', 'Blockchain Summit Booking', '5000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(7, '1', '1', '', 'Car Loan Payment', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(8, '1', '1', '', 'Cell Phone Bill Payments', '2000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(9, '1', '1', '', 'CEO Personal Expenses', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(10, '1', '1', '', 'Charges for Cleaning Services', '40000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(11, '1', '1', '', 'Charges for Emirates Post', '78000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(12, '1', '1', '', 'Charges for Uber Taxi', '22000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(13, '1', '1', '', 'Charity &Events', '32000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(14, '1', '1', '', 'Computer & Other Accessories', '5000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(15, '1', '1', '', 'Dewa  Expense', '6000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(16, '1', '1', '', 'Digital Music MP3', '8000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(17, '1', '1', '', 'Flight Ticket Charges', '9000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(18, '1', '1', '', 'Food&Refreshment', '11000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(19, '1', '1', '', 'Nexmo Purchase', '12000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(20, '1', '1', '', 'Office Rent', '13000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(21, '1', '1', '', 'Office Use', '14500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(22, '1', '1', '', 'Office Wifi& Utilities', '17500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(23, '1', '1', '', 'Other Expenses', '12541', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(24, '1', '1', '', 'Parking Fee', '13521', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(25, '1', '1', '', 'Petrol Expenses', '7500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(26, '1', '1', '', 'Printing&Stationary', '11000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(27, '1', '1', '', 'Quick Art Technical Services LLC', '12000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(28, '1', '1', '', 'Salary Advance', '13000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(29, '1', '1', '', 'Salik Card', '14500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(30, '1', '1', '', 'Smart Wifi Video Doorbell', '17500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(31, '1', '1', '', 'Software Charges', '12541', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(32, '1', '1', '', 'Staff Salary', '13521', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(33, '1', '1', '', 'Tax Payment to Government', '14141', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(34, '1', '1', '', 'Time Attendance Device', '15151', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(35, '1', '1', '', 'Trademark Fee', '16084', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(36, '1', '1', '', 'Visa&Legal Expenses', '100000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(37, '1', '1', '', 'Closing Balance', '', '', '', '', '', '', '12/09/2018 - 12/09/2018', '100000.00', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(38, '1', '1', '', 'Cash-in-Hand', '100000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(39, '1', '1', '1000000', 'Total', '', '', '', '', '', '', '12/09/2018 - 12/09/2018', '1000000', '2018-12-08 22:01:03', '2018-12-08 22:01:03'),
(40, '2', '1', '', '', '', '2018-11-01 12:00:00', 'CEO Personal Expenses', 'Payment', '207', '8000', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(41, '2', '1', '', '', '', '2018-11-01 12:00:00', 'Food&Refreshment', 'Payment', '208', '106', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(42, '2', '1', '', '', '', '2018-11-05 12:00:00', 'Visa&Legal Expenses', 'Payment', '209', '29015', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(43, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Visa&Legal Expenses', 'Payment', '210', '9283', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(44, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Visa&Legal Expenses', 'Payment', '211', '6363', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(45, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Other Expenses', 'Payment', '212', '1165', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(46, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Petrol Expenses', 'Payment', '213', '364', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(47, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Cell Phone Bill Payments', 'Payment', '214', '215', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(48, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Car Loan Payment', 'Payment', '215', '2000', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(49, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Charges for Cleaning Services', 'Payment', '216', '1197', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(50, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Food&Refreshment', 'Payment', '217', '1607', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(51, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Other Expenses', 'Payment', '218', '162.75', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(52, '2', '1', '', '', '', '2018-11-09 12:00:00', 'Charges for Cleaning Services', 'Payment', '219', '210', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(53, '2', '1', '', '', '', '2018-11-15 12:00:00', 'Flight Ticket Charges', 'Payment', '220', '2029.51', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(54, '2', '1', '', '', '', '2018-11-15 12:00:00', 'Other Expenses', 'Payment', '221', '3281.44', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(55, '2', '1', '', '', '', '2018-11-15 12:00:00', 'Office Wifi& Utilities', 'Payment', '222', '4040', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(56, '2', '1', '', '', '', '2018-11-16 12:00:00', 'CEO Personal Expenses', 'Payment', '223', '8000', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(57, '2', '1', '', '', '', '2018-11-19 12:00:00', 'Food&Refreshment', 'Payment', '224', '74', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(58, '2', '1', '', '', '', '2018-11-19 12:00:00', 'Article Publish & Advertising Expenses', 'Payment', '225', '1920', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(59, '2', '1', '', '', '', '2018-11-19 12:00:00', 'Food&Refreshment', 'Payment', '226', '76.65', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(60, '2', '1', '', '', '', '2018-11-19 12:00:00', 'Charges for Uber Taxi', 'Payment', '227', '101.5', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(61, '2', '1', '', '', '', '2018-11-19 12:00:00', 'Office Rent', 'Payment', '228', '117000', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(62, '2', '1', '', '', '', '2018-11-22 12:00:00', 'Staff Salary', 'Payment', '229', '38850', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(63, '2', '1', '', '', '', '2018-11-22 12:00:00', 'Staff Salary', 'Payment', '230', '55616', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(64, '2', '1', '', '', '', '2018-11-22 12:00:00', 'Other Expenses', 'Payment', '231', '556', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(65, '2', '1', '', '', '', '2018-11-22 12:00:00', 'Food&Refreshment', 'Payment', '232', '1745.25', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(66, '2', '1', '', '', '', '2018-11-22 12:00:00', 'Charges for Cleaning Services', 'Payment', '233', '168', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(67, '2', '1', '', '', '', '2018-11-22 12:00:00', 'Other Expenses', 'Payment', '234', '220.5', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(68, '2', '1', '', '', '', '2018-11-26 12:00:00', 'Charges for Uber Taxi', 'Payment', '235', '76', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(69, '2', '1', '', '', '', '2018-11-26 12:00:00', 'Food&Refreshment', 'Payment', '236', '88', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(70, '2', '1', '', '', '', '1970-01-01 12:00:00', '', '', '', '295253.15', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(71, '2', '1', '', '', '', '1970-01-01 12:00:00', '', '', '', '295253.15', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:01:21', '2018-12-08 22:01:21'),
(72, '1', '1', '1000000', 'Indirect Expenses', '', '', '', '', '', '', '12/09/2018 - 12/09/2018', '900000', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(73, '1', '1', '', 'Accountant Fee', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(74, '1', '1', '', 'Apartment Rent', '150000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(75, '1', '1', '', 'Apartment Wifi & Other Utilities', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(76, '1', '1', '', 'Article Publish & Advertising Expenses', '2000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(77, '1', '1', '', 'Blockchain Summit Booking', '5000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(78, '1', '1', '', 'Car Loan Payment', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(79, '1', '1', '', 'Cell Phone Bill Payments', '2000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(80, '1', '1', '', 'CEO Personal Expenses', '50000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(81, '1', '1', '', 'Charges for Cleaning Services', '40000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(82, '1', '1', '', 'Charges for Emirates Post', '78000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(83, '1', '1', '', 'Charges for Uber Taxi', '22000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(84, '1', '1', '', 'Charity &Events', '32000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(85, '1', '1', '', 'Computer & Other Accessories', '5000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(86, '1', '1', '', 'Dewa  Expense', '6000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(87, '1', '1', '', 'Digital Music MP3', '8000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(88, '1', '1', '', 'Flight Ticket Charges', '9000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(89, '1', '1', '', 'Food&Refreshment', '11000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(90, '1', '1', '', 'Nexmo Purchase', '12000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(91, '1', '1', '', 'Office Rent', '13000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(92, '1', '1', '', 'Office Use', '14500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(93, '1', '1', '', 'Office Wifi& Utilities', '17500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(94, '1', '1', '', 'Other Expenses', '12541', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(95, '1', '1', '', 'Parking Fee', '13521', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(96, '1', '1', '', 'Petrol Expenses', '7500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(97, '1', '1', '', 'Printing&Stationary', '11000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(98, '1', '1', '', 'Quick Art Technical Services LLC', '12000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(99, '1', '1', '', 'Salary Advance', '13000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(100, '1', '1', '', 'Salik Card', '14500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(101, '1', '1', '', 'Smart Wifi Video Doorbell', '17500', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(102, '1', '1', '', 'Software Charges', '12541', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(103, '1', '1', '', 'Staff Salary', '13521', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(104, '1', '1', '', 'Tax Payment to Government', '14141', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(105, '1', '1', '', 'Time Attendance Device', '15151', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(106, '1', '1', '', 'Trademark Fee', '16084', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(107, '1', '1', '', 'Visa&Legal Expenses', '100000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(108, '1', '1', '', 'Closing Balance', '', '', '', '', '', '', '12/09/2018 - 12/09/2018', '100000.00', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(109, '1', '1', '', 'Cash-in-Hand', '100000', '', '', '', '', '', '12/09/2018 - 12/09/2018', '', '2018-12-08 22:18:01', '2018-12-08 22:18:01'),
(110, '1', '1', '1000000', 'Total', '', '', '', '', '', '', '12/09/2018 - 12/09/2018', '1000000', '2018-12-08 22:18:01', '2018-12-08 22:18:01');

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `user_id`, `text`, `source_type`, `source_id`, `ip_address`, `action`, `created_at`, `updated_at`) VALUES
(1, 3, 'Lead was completed by testing1', 'App\\Models\\Lead', 3, '', 'updated_status', '2018-11-22 09:13:10', '2018-11-22 09:13:10'),
(2, 1, 'shopping Mall was created by Admin and assigned to claude', 'App\\Models\\Lead', 5, '', 'created', '2018-11-23 04:25:02', '2018-11-23 04:25:02'),
(3, 1, 'Pop up was created by Admin and assigned to IT Employee1', 'App\\Models\\Task', 5, '', 'created', '2018-11-23 04:29:03', '2018-11-23 04:29:03'),
(4, 1, 'Application Form was created by Admin and assigned to IT Employee1', 'App\\Models\\Task', 6, '', 'created', '2018-11-23 04:30:11', '2018-11-23 04:30:11'),
(5, 1, 'Marketing Task1 was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 7, '', 'created', '2018-11-23 04:59:01', '2018-11-23 04:59:01'),
(6, 1, 'Marketing Article Task1  was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 8, '', 'created', '2018-11-23 04:59:33', '2018-11-23 04:59:33'),
(7, 1, 'Marketing SNSTask2 was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 9, '', 'created', '2018-11-23 05:00:12', '2018-11-23 05:00:12'),
(8, 1, 'Marketing Article Task2 was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 10, '', 'created', '2018-11-23 05:00:41', '2018-11-23 05:00:41'),
(9, 1, 'Task was completed by Admin', 'App\\Models\\Task', 6, '', 'updated_status', '2018-11-23 05:11:18', '2018-11-23 05:11:18'),
(10, 1, 'ERP Project was created by Admin and assigned to claude', 'App\\Models\\Lead', 10, '', 'created', '2018-11-23 05:24:46', '2018-11-23 05:24:46'),
(11, 12, 'QT wallet was created by claude and assigned to IT Employee1', 'App\\Models\\Task', 11, '', 'created', '2018-11-23 07:57:38', '2018-11-23 07:57:38'),
(12, 1, 'Zeeshan task today was created by Admin and assigned to IT Employee1', 'App\\Models\\Task', 12, '', 'created', '2018-11-23 09:29:42', '2018-11-23 09:29:42'),
(13, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 3, '', 'updated_deadline', '2018-11-25 03:49:18', '2018-11-25 03:49:18'),
(14, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 3, '', 'updated_deadline', '2018-11-25 04:13:05', '2018-11-25 04:13:05'),
(15, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 3, '', 'updated_deadline', '2018-11-25 04:29:58', '2018-11-25 04:29:58'),
(16, 1, 'test SNS2 was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 13, '', 'created', '2018-11-25 13:19:13', '2018-11-25 13:19:13'),
(17, 1, 'TestSNS Urgent Task was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 14, '', 'created', '2018-11-25 13:20:44', '2018-11-25 13:20:44'),
(18, 1, 'Test SNS Urgent issue was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 15, '', 'created', '2018-11-25 13:23:47', '2018-11-25 13:23:47'),
(19, 1, 'test SNS Posting std was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 16, '', 'created', '2018-11-25 13:28:19', '2018-11-25 13:28:19'),
(20, 1, 'SNS urgent was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 17, '', 'created', '2018-11-25 13:28:56', '2018-11-25 13:28:56'),
(21, 1, 'Article Standard Task was created by Admin and assigned to Marketing Employee1', 'App\\Models\\Task', 18, '', 'created', '2018-11-25 13:29:44', '2018-11-25 13:29:44'),
(22, 1, 'IT urgent task was created by Admin and assigned to IT Employee1', 'App\\Models\\Task', 19, '', 'created', '2018-11-25 13:30:38', '2018-11-25 13:30:38'),
(23, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 3, '', 'updated_deadline', '2018-11-26 03:02:03', '2018-11-26 03:02:03'),
(24, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 5, '', 'updated_deadline', '2018-11-26 03:11:01', '2018-11-26 03:11:01'),
(25, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 5, '', 'updated_deadline', '2018-11-26 03:12:23', '2018-11-26 03:12:23'),
(26, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 5, '', 'updated_deadline', '2018-11-26 03:13:11', '2018-11-26 03:13:11'),
(27, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 5, '', 'updated_deadline', '2018-11-26 03:14:01', '2018-11-26 03:14:01'),
(28, 1, 'Admin updated the deadline for this task', 'App\\Models\\Task', 5, '', 'updated_deadline', '2018-11-26 03:46:52', '2018-11-26 03:46:52'),
(29, 1, 'Admin updated the deadline for this task', 'App\\Models\\Task', 5, '', 'updated_deadline', '2018-11-26 03:47:05', '2018-11-26 03:47:05'),
(30, 1, 'Admin updated the deadline for this task', 'App\\Models\\Task', 7, '', 'updated_deadline', '2018-11-26 03:48:16', '2018-11-26 03:48:16'),
(31, 1, 'new market 26 Nov 18 was created by Admin and assigned to Marketing Manager', 'App\\Models\\Lead', 11, '', 'created', '2018-11-26 04:11:33', '2018-11-26 04:11:33'),
(32, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 10, '', 'updated_deadline', '2018-11-26 04:46:23', '2018-11-26 04:46:23'),
(33, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 10, '', 'updated_deadline', '2018-11-26 04:46:33', '2018-11-26 04:46:33'),
(34, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 3, '', 'updated_deadline', '2018-11-26 06:27:27', '2018-11-26 06:27:27'),
(35, 1, 'Admin updated the deadline for this lead', 'App\\Models\\Lead', 3, '', 'updated_deadline', '2018-11-29 07:03:58', '2018-11-29 07:03:58'),
(36, 1, 'Task was completed by Admin', 'App\\Models\\Task', 19, '', 'updated_status', '2018-12-05 02:34:28', '2018-12-05 02:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primary_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `industry_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `source_id` int(10) UNSIGNED NOT NULL,
  `source_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `commentable_id` int(10) UNSIGNED NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `description`, `source_id`, `source_type`, `user_id`, `commentable_type`, `commentable_id`, `body`, `created_at`, `updated_at`, `status`) VALUES
(1, '', 0, '', 2, 'App\\Models\\Topic', 16, 'cxvcvvxv', '2018-11-13 09:29:50', '2018-11-13 09:29:50', 0),
(2, '', 0, '', 2, 'App\\Models\\Topic', 16, 'cxvcvvxv', '2018-11-13 09:30:24', '2018-11-13 09:30:24', 0),
(3, '', 0, '', 2, 'App\\Models\\Topic', 16, 'cxvcvvxv', '2018-11-13 09:31:02', '2018-11-13 09:31:02', 0),
(4, '', 0, '', 2, 'App\\Models\\Topic', 16, 'sdssddssdds', '2018-11-13 09:52:47', '2018-11-13 09:52:47', 0),
(5, '', 0, '', 2, 'App\\Models\\Topic', 16, 'sdssddssdds', '2018-11-13 09:53:27', '2018-11-13 09:53:27', 0),
(8, '', 0, '', 2, 'App\\Models\\Topic', 17, 'testinggggg', '2018-11-13 10:05:04', '2018-11-15 07:31:33', 0),
(9, '', 0, '', 2, 'App\\Models\\Topic', 17, 'Hemz', '2018-11-13 10:05:29', '2018-11-15 07:27:09', 0),
(10, '', 0, '', 2, 'App\\Models\\Topic', 17, 'testinggggg2222', '2018-11-13 10:05:57', '2018-11-15 07:38:58', 0),
(11, '', 0, '', 2, 'App\\Models\\Topic', 17, 'testingg333333', '2018-11-13 10:07:12', '2018-11-15 07:35:40', 0),
(12, '', 0, '', 1, 'App\\Models\\Topic', 18, 'first comment', '2018-11-14 02:25:01', '2018-11-14 08:30:58', 0),
(14, '', 0, '', 2, 'App\\Models\\Topic', 18, 'this is third comment from Kana', '2018-11-14 02:40:28', '2018-11-14 02:40:28', 0),
(15, '', 0, '', 2, 'App\\Models\\Topic', 18, 'xcvvbbvbv', '2018-11-14 02:41:14', '2018-11-14 02:41:14', 0),
(16, '', 0, '', 3, 'App\\Models\\Topic', 18, 'This is the test comment from the mgr1', '2018-11-14 02:49:55', '2018-11-14 02:49:55', 0),
(17, '', 0, '', 1, 'App\\Models\\Comment', 16, 'reply1', '2018-11-14 04:33:24', '2018-11-14 04:33:24', 0),
(18, '', 0, '', 3, 'App\\Models\\Comment', 17, 'reply2', '2018-11-14 04:36:25', '2018-11-15 07:35:57', 1),
(19, '', 0, '', 1, 'App\\Models\\Comment', 16, 'reply to testing11111', '2018-11-14 04:47:09', '2018-11-14 04:47:09', 0),
(20, '', 0, '', 1, 'App\\Models\\Comment', 16, '11111111111111', '2018-11-14 04:47:38', '2018-11-14 04:47:38', 0),
(21, '', 0, '', 1, 'App\\Models\\Comment', 20, '222222222', '2018-11-14 04:51:19', '2018-11-14 04:51:19', 0),
(22, '', 0, '', 1, 'App\\Models\\Comment', 20, '22222222', '2018-11-14 04:52:02', '2018-11-14 04:52:02', 0),
(23, '', 0, '', 1, 'App\\Models\\Comment', 20, '1111ghjhklkjhgfdsadfghjgfds', '2018-11-14 04:52:29', '2018-11-14 09:15:38', 0),
(24, '', 0, '', 3, 'App\\Models\\Comment', 16, 'test replyyyyy', '2018-11-14 04:53:42', '2018-11-14 04:53:42', 0),
(25, '', 0, '', 3, 'App\\Models\\Comment', 16, '11111111111111', '2018-11-14 04:55:52', '2018-11-14 04:55:52', 0),
(26, '', 0, '', 1, 'App\\Models\\Comment', 15, 'dfgddg', '2018-11-14 06:27:19', '2018-11-14 06:27:19', 0),
(27, '', 0, '', 1, 'App\\Models\\Topic', 18, 'xxcvv', '2018-11-14 06:42:06', '2018-11-14 06:42:06', 0),
(28, '', 0, '', 1, 'App\\Models\\Comment', 27, '2123456543', '2018-11-14 06:42:18', '2018-11-14 06:42:18', 0),
(29, '', 0, '', 1, 'App\\Models\\Comment', 27, 'new replyyyyy', '2018-11-14 06:43:47', '2018-11-14 06:43:47', 0),
(30, '', 0, '', 1, 'App\\Models\\Comment', 27, '2nd new replyyy', '2018-11-14 06:47:03', '2018-11-14 06:47:03', 0),
(31, '', 0, '', 1, 'App\\Models\\Comment', 27, '2nd new replyyy', '2018-11-14 06:48:20', '2018-11-14 06:48:20', 0),
(32, '', 0, '', 1, 'App\\Models\\Comment', 27, 'FDFFGDFSDFF', '2018-11-14 06:48:34', '2018-11-14 06:48:34', 0),
(33, '', 0, '', 1, 'App\\Models\\Comment', 27, 'KJHGFDS', '2018-11-14 06:50:02', '2018-11-14 06:50:02', 0),
(34, '', 0, '', 1, 'App\\Models\\Comment', 27, 'MJHGFDSD', '2018-11-14 06:51:48', '2018-11-14 06:51:48', 0),
(35, '', 0, '', 1, 'App\\Models\\Comment', 27, 'MJHGFDSD', '2018-11-14 06:52:43', '2018-11-14 06:52:43', 0),
(36, '', 0, '', 1, 'App\\Models\\Comment', 27, 'FGHJHKJHGFDSAS', '2018-11-14 06:52:54', '2018-11-14 06:52:54', 0),
(37, '', 0, '', 1, 'App\\Models\\Comment', 27, 'LKJHGFDSFGH', '2018-11-14 06:53:57', '2018-11-14 06:53:57', 0),
(38, '', 0, '', 1, 'App\\Models\\Comment', 14, 'sfdghfdsaSDFG', '2018-11-14 06:56:54', '2018-11-14 06:56:54', 0),
(39, '', 0, '', 1, 'App\\Models\\Comment', 14, 'DSFGHGFDSA', '2018-11-14 06:57:29', '2018-11-14 06:57:29', 0),
(40, '', 0, '', 1, 'App\\Models\\Comment', 14, 'DFGDSADFD', '2018-11-14 06:58:06', '2018-11-14 06:58:06', 0),
(41, '', 0, '', 1, 'App\\Models\\Comment', 14, 'DFGDSADFD', '2018-11-14 06:58:25', '2018-11-14 06:58:25', 0),
(42, '', 0, '', 1, 'App\\Models\\Comment', 14, 'dsfghgfdsa', '2018-11-14 07:00:00', '2018-11-14 07:00:00', 0),
(43, '', 0, '', 1, 'App\\Models\\Comment', 12, 'SDFBGFDSAsdds', '2018-11-14 07:00:39', '2018-11-21 07:03:44', 0),
(44, '', 0, '', 1, 'App\\Models\\Comment', 15, 'CVBNMJHGFDS', '2018-11-14 07:01:21', '2018-11-14 07:01:21', 0),
(48, '', 0, '', 1, 'App\\Models\\Topic', 18, 'fgghfhfh', '2018-11-14 07:09:55', '2018-11-14 07:09:55', 0),
(51, '', 0, '', 1, 'App\\Models\\Topic', 18, 'srdfghj', '2018-11-14 08:38:04', '2018-11-14 08:38:04', 0),
(52, '', 0, '', 1, 'App\\Models\\Topic', 18, 'srdfghj', '2018-11-14 08:39:29', '2018-11-14 08:39:29', 0),
(53, '', 0, '', 1, 'App\\Models\\Comment', 52, 'testinhg1111', '2018-11-14 08:39:58', '2018-11-14 08:39:58', 0),
(54, '', 0, '', 1, 'App\\Models\\Topic', 18, 'xcvxvxvx', '2018-11-14 08:41:00', '2018-11-14 08:41:00', 0),
(55, '', 0, '', 1, 'App\\Models\\Topic', 18, 'vvxxvvxvx', '2018-11-14 08:41:05', '2018-11-14 08:41:05', 0),
(56, '', 0, '', 1, 'App\\Models\\Topic', 18, 'vxxvvvxvx', '2018-11-14 08:41:09', '2018-11-14 08:41:09', 0),
(57, '', 0, '', 1, 'App\\Models\\Topic', 18, 'vxvxvxvxvxvx', '2018-11-14 08:41:14', '2018-11-14 08:41:14', 0),
(58, '', 0, '', 1, 'App\\Models\\Topic', 22, 'this is issue by me', '2018-11-14 09:28:04', '2018-11-14 09:28:17', 0),
(60, '', 0, '', 2, 'App\\Models\\Topic', 22, 'dsfgfggf', '2018-11-14 09:30:25', '2018-11-14 09:30:25', 0),
(61, '', 0, '', 1, 'App\\Models\\Topic', 22, 'ukygfuykjgyughynhuhhuuioioaaa\r\n', '2018-11-14 09:33:19', '2018-11-14 09:33:19', 0),
(62, '', 0, '', 2, 'App\\Models\\Topic', 22, 'this user want to delete his email from ms wallet ', '2018-11-14 09:36:32', '2018-11-15 08:49:40', 1),
(63, '', 0, '', 1, 'App\\Models\\Topic', 19, 'dsdsdsd', '2018-11-15 03:16:03', '2018-11-15 03:16:03', 0),
(64, '', 0, '', 1, 'App\\Models\\Comment', 10, 'dfghjgf', '2018-11-15 03:27:43', '2018-11-15 03:27:43', 0),
(65, '', 0, '', 1, 'App\\Models\\Topic', 23, 'sdfdfgfdffg', '2018-11-15 03:29:32', '2018-11-15 03:29:32', 0),
(66, '', 0, '', 1, 'App\\Models\\Comment', 65, 'ssffsfsgsg', '2018-11-15 03:29:51', '2018-11-15 03:29:51', 0),
(67, '', 0, '', 1, 'App\\Models\\Comment', 43, 'sddsdssd', '2018-11-15 03:31:26', '2018-11-15 03:31:26', 0),
(68, '', 0, '', 1, 'App\\Models\\Topic', 24, 'From: jclaude@gmail.com\r\nI can\'t login to my MC Wallet', '2018-11-15 03:41:26', '2018-11-15 03:41:26', 0),
(69, '', 0, '', 1, 'App\\Models\\Comment', 68, 'This is solved by deleting the account\r\n', '2018-11-15 03:42:30', '2018-11-15 03:42:30', 0),
(70, '', 0, '', 1, 'App\\Models\\Comment', 9, 'test commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest commentstest comments', '2018-11-15 07:54:51', '2018-11-15 07:57:18', 0),
(71, '', 0, '', 1, 'App\\Models\\Comment', 9, 'like you a lot', '2018-11-15 07:55:08', '2018-11-15 07:55:08', 0),
(75, '', 0, '', 1, 'App\\Models\\Topic', 12, 'dsdsdsd', '2018-11-21 07:04:16', '2018-11-21 07:04:16', 0),
(76, '', 0, '', 1, 'App\\Models\\Topic', 21, 'ujhjjjnnj', '2018-11-21 07:10:48', '2018-11-21 07:10:48', 0),
(77, '', 0, '', 1, 'App\\Models\\Topic', 26, 'sddsdsd aasasasasasasasas', '2018-11-21 07:59:50', '2018-11-21 08:17:59', 1),
(78, '', 0, '', 1, 'App\\Models\\Comment', 77, 'sdsdsd', '2018-11-21 08:00:32', '2018-11-21 08:00:32', 0),
(79, '', 0, '', 1, 'App\\Models\\Topic', 26, 'sdsdsdsdsdsd', '2018-11-21 08:19:05', '2018-11-21 08:19:09', 1),
(80, '', 0, '', 1, 'App\\Models\\Topic', 26, 'sxsxssxsxs', '2018-11-21 08:19:56', '2018-11-21 10:40:29', 1),
(81, '', 0, '', 1, 'App\\Models\\Comment', 80, 'sdsdsdsd', '2018-11-21 08:20:53', '2018-11-21 08:20:53', 0),
(82, '', 0, '', 1, 'App\\Models\\Comment', 80, 'sdsdsdsds', '2018-11-21 08:20:59', '2018-11-21 08:20:59', 0),
(83, 'cdsdsds', 3, 'App\\Models\\Lead', 1, '', 0, '', '2018-11-25 03:48:34', '2018-11-25 03:48:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Managment', NULL, '2018-11-13 03:21:43', '2018-11-13 03:21:43'),
(2, 'Marketing', 'Marketing department', '2018-11-14 02:48:43', '2018-11-14 02:48:43'),
(3, 'IT', 'IT department', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department_user`
--

CREATE TABLE `department_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `department_user`
--

INSERT INTO `department_user` (`id`, `department_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(12, 2, 11, NULL, NULL),
(13, 3, 12, NULL, NULL),
(14, 3, 13, NULL, NULL),
(15, 3, 14, NULL, NULL),
(16, 2, 15, NULL, NULL),
(17, 2, 16, NULL, NULL),
(18, 2, 17, NULL, NULL),
(19, 2, 18, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_display` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`) VALUES
(1, 'Accommodations'),
(2, 'Accounting'),
(3, 'Auto'),
(4, 'Beauty & Cosmetics'),
(5, 'Carpenter'),
(6, 'Communications'),
(7, 'Computer & IT'),
(8, 'Construction'),
(9, 'Consulting'),
(10, 'Education'),
(11, 'Electronics'),
(12, 'Entertainment'),
(13, 'Food & Beverages'),
(14, 'Legal Services'),
(15, 'Marketing'),
(16, 'Real Estate'),
(17, 'Retail'),
(18, 'Sports'),
(19, 'Technology'),
(20, 'Tourism'),
(21, 'Transportation'),
(22, 'Travel'),
(23, 'Utilities'),
(24, 'Web Services'),
(25, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `integrations`
--

CREATE TABLE `integrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_secret` int(11) DEFAULT NULL,
  `api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `org_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `payment_received_at` datetime DEFAULT NULL,
  `due_at` datetime DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_lines`
--

CREATE TABLE `invoice_lines` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_assigned_id` int(10) UNSIGNED NOT NULL,
  `dept_id` int(10) UNSIGNED NOT NULL,
  `user_created_id` int(10) UNSIGNED NOT NULL,
  `contact_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(50) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `title`, `description`, `status`, `user_assigned_id`, `dept_id`, `user_created_id`, `contact_date`, `created_at`, `updated_at`, `priority`) VALUES
(1, 'Events Project', 'Events Project', 2, 12, 3, 1, '2018-11-15 00:00:00', '2018-11-13 20:00:00', '2018-11-13 20:00:00', 1),
(2, 'MC Wallet Project', 'Events Project', 2, 12, 3, 1, '2018-11-15 00:00:00', '2018-08-31 20:00:00', '2018-08-31 20:00:00', 1),
(3, 'Marketing Project1', 'Marketing Project1', 1, 2, 2, 1, '2018-12-06 11:00:00', '2018-11-19 20:00:00', '2018-11-29 07:03:57', 3),
(4, 'Marketing Project2', 'Marketing Project2', 2, 2, 2, 1, '2018-11-30 00:00:00', '2018-11-20 20:00:00', '2018-11-20 20:00:00', 1),
(5, 'shopping Mall', 'this is ABBC shopping  mall', 1, 12, 3, 1, '2018-12-03 11:00:00', '2018-11-23 04:25:01', '2018-11-26 03:14:01', 2),
(6, 'Events Project2', 'Events Project2', 2, 12, 3, 1, '2018-11-15 00:00:00', '2018-11-19 20:00:00', '2018-11-19 20:00:00', 1),
(7, 'MC Wallet Project2', 'Events Project2', 1, 12, 3, 1, '2018-11-15 00:00:00', '2018-07-08 20:00:00', '2018-07-08 20:00:00', 1),
(8, 'Marketing Project3', 'Marketing Project3', 2, 2, 2, 1, '2018-11-29 00:00:00', '2018-11-19 20:00:00', '2018-11-22 09:13:09', 1),
(9, 'Marketing Project4', 'Marketing Project4', 2, 2, 2, 1, '2018-11-30 00:00:00', '2018-11-29 20:00:00', '2018-11-29 20:00:00', 1),
(10, 'ERP Project', 'ABBC Company ERP Program', 1, 12, 3, 1, '2018-12-03 11:00:00', '2018-11-23 05:24:45', '2018-11-26 04:46:33', 2),
(11, 'new market 26 Nov 18', 'new market 26 Nov 18', 1, 2, 2, 1, '2018-12-03 11:00:00', '2018-11-26 04:11:33', '2018-11-26 04:11:33', 3);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `likeable_id` int(10) UNSIGNED NOT NULL,
  `likeable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_06_04_124835_create_industries_table', 1),
(4, '2015_12_28_163028_create_clients_table', 1),
(5, '2015_12_29_150049_create_invoice_table', 1),
(6, '2015_12_29_204031_tasks_table', 1),
(7, '2016_01_10_204413_create_comments_table', 1),
(8, '2016_01_18_113656_create_leads_table', 1),
(9, '2016_01_23_144854_settings', 1),
(10, '2016_01_26_003903_documents', 1),
(11, '2016_01_31_211926_invoice_lines_table', 1),
(12, '2016_03_21_171847_create_department_table', 1),
(13, '2016_03_21_171847_create_taskcategory_table', 1),
(14, '2016_03_21_172416_create_department_user_table', 1),
(15, '2016_04_06_230504_integrations', 1),
(16, '2016_05_21_205532_create_activity_log_table', 1),
(17, '2016_08_26_205017_entrust_setup_tables', 1),
(18, '2016_11_04_200855_create_notifications_table', 1),
(19, '2018_05_01_204802_create_topics_table', 1),
(20, '2018_05_03_022254_add_user_id_to_topics', 1),
(21, '2018_05_08_013400_create_page_views_table', 1),
(22, '2018_05_09_075353_add_best_answer_to_topics', 1),
(23, '2018_05_09_181452_create_likes_table', 1),
(24, '2018_05_13_014234_add_provider_column_to_users', 1),
(25, '2018_05_14_004243_add_slug_column_to_topics', 1),
(26, '2018_05_14_004402_add_slug_column_to_users', 1),
(27, '2018_11_13_103823_create_project_uploads_table', 2),
(28, '2018_11_26_084346_create_officials_table', 3),
(29, '2018_12_05_123327_create_items_table', 4),
(30, '2018_12_05_133728_create_products_table', 5),
(31, '2018_12_05_123327_create_accountexpenses_table', 6),
(32, '2018_12_06_111828_create_accountexpenses_table', 7),
(33, '2018_12_06_135847_create_accountexpenses_table', 8),
(34, '2018_12_09_083307_create_reports_table', 9),
(35, '2018_12_09_083954_create_reports_table', 10),
(36, '2018_12_09_084519_create_reports_table', 11),
(37, '2018_12_09_084714_create_reports_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('061a706f-be62-44f2-9759-180e26a1d935', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"Admin updated the deadline for this ERP Project\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":10,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/10\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 04:46:23', '2018-11-26 04:46:23'),
('0c36044e-c838-4f23-90d6-05a3f696622a', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"Article Standard Task was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":18,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/18\",\"action\":\"created\"}', NULL, '2018-11-25 13:29:44', '2018-11-25 13:29:44'),
('154622e3-34ec-42f2-8048-884e4a3f8c00', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"Admin updated the deadline for this shopping Mall\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:12:22', '2018-11-26 03:12:22'),
('1abeb568-009f-48c0-acbc-e5be40b3f39a', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":12,\"message\":\"Admin updated the deadline for this Pop up task\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:47:05', '2018-11-26 03:47:05'),
('206a3183-700d-4012-ae7f-6e110f5eeb27', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":1,\"message\":\"Pop up was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/5\",\"action\":\"created\"}', NULL, '2018-11-23 04:29:03', '2018-11-23 04:29:03'),
('240e904c-953c-46d1-b139-dc80b2c5f127', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"Admin updated the deadline for this shopping Mall\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:11:01', '2018-11-26 03:11:01'),
('252162a2-c9ad-44df-aba5-95616f2fe789', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"Test SNS Urgent issue was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":15,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/15\",\"action\":\"created\"}', NULL, '2018-11-25 13:23:47', '2018-11-25 13:23:47'),
('3d4bf79b-5b8f-4bce-b34a-2a1b76644773', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"Admin updated the deadline for this shopping Mall\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:14:01', '2018-11-26 03:14:01'),
('3e7df468-8dde-481e-bb66-20c595883d32', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":1,\"message\":\"Application Form was completed by Admin\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":6,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/6\",\"action\":\"updated_status\"}', NULL, '2018-11-23 05:11:18', '2018-11-23 05:11:18'),
('444db60a-3697-496b-9223-ccdd84aea2c2', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"Admin updated the deadline for this Marketing Project1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_deadline\"}', NULL, '2018-11-25 03:49:18', '2018-11-25 03:49:18'),
('494136ac-e0fa-43e7-8ba9-468eb8e4a3b1', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":12,\"message\":\"Admin updated the deadline for this Pop up task\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:46:52', '2018-11-26 03:46:52'),
('4b46d025-ea86-46d4-be80-a0d75b5887f5', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"shopping Mall was created by Admin and assigned to you\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/5\",\"action\":\"created\"}', NULL, '2018-11-23 04:25:02', '2018-11-23 04:25:02'),
('56dc83a2-4ca7-4f00-a6b6-6846fbdfea9d', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":3,\"message\":\"twst1 was completed by testing1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_status\"}', NULL, '2018-11-22 09:13:09', '2018-11-22 09:13:09'),
('72318503-cc16-46b4-b98b-fb0983bfab30', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"Admin updated the deadline for this Marketing Project1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_deadline\"}', NULL, '2018-11-25 04:29:58', '2018-11-25 04:29:58'),
('7878a524-6e3b-4570-8105-e62c80231009', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":2,\"message\":\"Admin updated the deadline for this Marketing Task1\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":7,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/7\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:48:16', '2018-11-26 03:48:16'),
('79eda0c1-baf5-4078-abb6-31e71fb96985', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":12,\"message\":\"Admin updated the deadline for this Pop up task\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:45:46', '2018-11-26 03:45:46'),
('80fc54a3-20a5-4be0-b4a2-a138f1fc1e4a', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":1,\"message\":\"IT urgent task was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":19,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/19\",\"action\":\"created\"}', NULL, '2018-11-25 13:30:38', '2018-11-25 13:30:38'),
('859e2c9d-4424-43c6-b83c-4ba6102fe449', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":12,\"message\":\"Admin updated the deadline for this Pop up task\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:45:58', '2018-11-26 03:45:58'),
('8688d859-7f0e-4bdb-a391-2c98be9ccf50', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"new market 26 Nov 18 was created by Admin and assigned to you\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":11,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/11\",\"action\":\"created\"}', NULL, '2018-11-26 04:11:33', '2018-11-26 04:11:33'),
('89ca7d5d-9f39-4c0e-aebb-dcdeb2f3f5ce', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":1,\"message\":\"Zeeshan task today was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":12,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/12\",\"action\":\"created\"}', NULL, '2018-11-23 09:29:42', '2018-11-23 09:29:42'),
('8bb8772d-b172-40a6-9ef0-afc0fa1fb01a', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"SNS urgent was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":17,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/17\",\"action\":\"created\"}', NULL, '2018-11-25 13:28:56', '2018-11-25 13:28:56'),
('8e7ccd2b-8b7b-4b39-9bd5-e908898e3d2f', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"Marketing SNSTask2 was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":9,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/9\",\"action\":\"created\"}', NULL, '2018-11-23 05:00:12', '2018-11-23 05:00:12'),
('9145b45f-45e8-4604-8977-2c3bc7cc9872', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"ERP Project was created by Admin and assigned to you\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":10,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/10\",\"action\":\"created\"}', '2018-11-23 05:25:14', '2018-11-23 05:24:46', '2018-11-23 05:25:14'),
('91d8d449-f478-4d96-8bf8-977ac485fb35', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"Admin updated the deadline for this shopping Mall\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":5,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/5\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:13:11', '2018-11-26 03:13:11'),
('973061b9-a5a7-4d50-9692-d0364ac14ca1', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"Marketing Article Task2 was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":10,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/10\",\"action\":\"created\"}', NULL, '2018-11-23 05:00:41', '2018-11-23 05:00:41'),
('9af8d213-b9a0-4369-9dc7-dec4f78aff1c', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"test SNS2 was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":13,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/13\",\"action\":\"created\"}', NULL, '2018-11-25 13:19:13', '2018-11-25 13:19:13'),
('a8ff558d-1fc6-47f3-9a75-661f23d0806a', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":12,\"message\":\"QT wallet was created by claude, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":11,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/11\",\"action\":\"created\"}', NULL, '2018-11-23 07:57:38', '2018-11-23 07:57:38'),
('c5b322dc-96f3-4a6d-be38-439ea46a651f', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"Marketing Article Task1  was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":8,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/8\",\"action\":\"created\"}', NULL, '2018-11-23 04:59:33', '2018-11-23 04:59:33'),
('c94362e3-8944-4f14-a138-560a5beb6457', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"Admin updated the deadline for this Marketing Project1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 06:27:27', '2018-11-26 06:27:27'),
('cb884229-2180-484c-8e65-554219df63ee', 'App\\Notifications\\TaskActionNotification', 3, 'App\\Models\\User', '{\"assigned_user\":3,\"created_user\":1,\"message\":\"Application Form was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":6,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/6\",\"action\":\"created\"}', NULL, '2018-11-23 04:30:11', '2018-11-23 04:30:11'),
('cb995beb-ae02-43d8-b1f7-4f82e613157a', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"Admin updated the deadline for this Marketing Project1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 03:02:03', '2018-11-26 03:02:03'),
('d34513fc-99e8-4204-8165-2e3823eee578', 'App\\Notifications\\TaskActionNotification', 14, 'App\\Models\\User', '{\"assigned_user\":14,\"created_user\":1,\"message\":\"IT urgent task was completed by Admin\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":19,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/19\",\"action\":\"updated_status\"}', NULL, '2018-12-05 02:34:28', '2018-12-05 02:34:28'),
('d4e20d6f-af89-4025-9054-62db2f4b574c', 'App\\Notifications\\LeadActionNotification', 12, 'App\\Models\\User', '{\"assigned_user\":12,\"created_user\":1,\"message\":\"Admin updated the deadline for this ERP Project\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":10,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/10\",\"action\":\"updated_deadline\"}', NULL, '2018-11-26 04:46:33', '2018-11-26 04:46:33'),
('ec5a155d-90f7-4685-a2be-bc84a1962d4a', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"TestSNS Urgent Task was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":14,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/14\",\"action\":\"created\"}', NULL, '2018-11-25 13:20:44', '2018-11-25 13:20:44'),
('ef8201fe-79a6-44d7-9f3a-2e85054ffb68', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"test SNS Posting std was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":16,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/16\",\"action\":\"created\"}', NULL, '2018-11-25 13:28:19', '2018-11-25 13:28:19'),
('f0c8c6cb-faa1-41ba-85d6-7cb357f5381e', 'App\\Notifications\\TaskActionNotification', 11, 'App\\Models\\User', '{\"assigned_user\":11,\"created_user\":1,\"message\":\"Marketing Task1 was created by Admin, and assigned to you\",\"type\":\"App\\\\Models\\\\Task\",\"type_id\":7,\"url\":\"http:\\/\\/127.0.0.1:8000\\/tasks\\/7\",\"action\":\"created\"}', NULL, '2018-11-23 04:59:01', '2018-11-23 04:59:01'),
('f8e2394a-970a-4c16-bf03-b668f8446b00', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"Admin updated the deadline for this Marketing Project1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_deadline\"}', NULL, '2018-11-25 04:13:05', '2018-11-25 04:13:05'),
('ff24ed7a-a517-47bb-ab89-12cb92833663', 'App\\Notifications\\LeadActionNotification', 2, 'App\\Models\\User', '{\"assigned_user\":2,\"created_user\":1,\"message\":\"Admin updated the deadline for this Marketing Project1\",\"type\":\"App\\\\Models\\\\Lead\",\"type_id\":3,\"url\":\"http:\\/\\/127.0.0.1:8000\\/leads\\/3\",\"action\":\"updated_deadline\"}', NULL, '2018-11-29 07:03:58', '2018-11-29 07:03:58');

-- --------------------------------------------------------

--
-- Table structure for table `officials`
--

CREATE TABLE `officials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `officials`
--

INSERT INTO `officials` (`id`, `title`, `description`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 'Attention Please', 'Office will remain close Friday, Saturday from 1st week of December', '2018-11-26 07:01:41', '2018-11-26 07:01:41', 1),
(2, 'New ANNOUNCEMENT', 'Now the airdrop is finished! Your coins will be sent to your wallet address in few days. Be noted that the user who did not enter their wallet address will not receive 100 ABBC and referral coins. Thank you for your participation', '2018-11-29 03:02:00', '2018-12-04 08:46:00', 1),
(3, '100 ABBC Free AirDrop', 'To commemorate this historic event, they will be giving away 100 ABBC coins to first 500,000 users. This exclusive event was created due to the extensive response received from the public and total amount of 50,000,000ABBC will be given out during this evdfgfdgfdsfgfds', '2018-11-29 03:02:25', '2018-12-04 08:53:04', 1),
(4, 'Coming friday is Holiday ', 'Hooray, Coming friday is Holiday ', '2018-12-04 02:13:45', '2018-12-04 08:59:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page-views`
--

CREATE TABLE `page-views` (
  `id` int(10) UNSIGNED NOT NULL,
  `visitable_id` bigint(20) UNSIGNED NOT NULL,
  `visitable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'user-create', 'Create user', 'Permission to create user', '2018-11-13 03:21:43', '2018-11-13 03:21:43'),
(2, 'user-update', 'Update user', 'Permission to update user', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(3, 'user-delete', 'Delete user', 'Permission to update delete', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(4, 'client-create', 'Create client', 'Permission to create client', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(5, 'client-update', 'Update client', 'Permission to update client', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(6, 'client-delete', 'Delete client', 'Permission to delete client', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(7, 'task-create', 'Create task', 'Permission to create task', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(8, 'task-update', 'Update task', 'Permission to update task', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(9, 'lead-create', 'Create lead', 'Permission to create lead', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(10, 'lead-update', 'Update lead', 'Permission to update lead', '2018-11-13 03:21:44', '2018-11-13 03:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'hdtuto.com', 'Online tutorials', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projectuploads`
--

CREATE TABLE `projectuploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `taskcategory_id` int(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projectuploads`
--

INSERT INTO `projectuploads` (`id`, `project_name`, `project_id`, `user_id`, `status`, `created_at`, `updated_at`, `taskcategory_id`) VALUES
(1, 'XCVXBNBNMN,M.MNBVCX', 1, 1, '2', '2018-11-21 06:13:04', '2018-11-21 06:13:04', 0),
(2, 'Completed ERP Project', 10, 12, '2', '2018-11-23 05:28:57', '2018-11-23 05:28:57', 0),
(3, 'test sns docuemnt', 8, 1, '2', '2018-11-26 08:38:45', '2018-11-26 08:38:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `title`, `user_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Receipts and Payments ', 1, 'Receipts and Payments Report Details', '2018-12-09 07:00:27', '2018-12-09 07:00:27'),
(2, 'Day Book', 1, 'Day Book details', '2018-12-09 07:01:30', '2018-12-09 07:01:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Administrator', 'System Administrator', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(2, 'manager', 'Manager', 'System Manager', '2018-11-13 03:21:44', '2018-11-13 03:21:44'),
(3, 'employee', 'Employee', 'Employee', '2018-11-13 03:21:44', '2018-11-13 03:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(11, 3),
(12, 2),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_complete_allowed` int(11) NOT NULL,
  `task_assign_allowed` int(11) NOT NULL,
  `lead_complete_allowed` int(11) NOT NULL,
  `lead_assign_allowed` int(11) NOT NULL,
  `time_change_allowed` int(11) NOT NULL,
  `comment_allowed` int(11) NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `task_complete_allowed`, `task_assign_allowed`, `lead_complete_allowed`, `lead_assign_allowed`, `time_change_allowed`, `comment_allowed`, `country`, `company`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 2, 2, 2, 'en', 'Media', NULL, '2018-11-23 03:16:05');

-- --------------------------------------------------------

--
-- Table structure for table `taskcategories`
--

CREATE TABLE `taskcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taskcategories`
--

INSERT INTO `taskcategories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'SNS Posting', 'SNS Posting', '2018-11-19 09:40:02', '2018-11-19 09:40:02'),
(2, 'Articles', 'ArticlesArticlesArticlesArticlesArticles', '2018-11-19 09:40:25', '2018-11-19 09:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `user_assigned_id` int(10) UNSIGNED NOT NULL,
  `user_created_id` int(10) UNSIGNED NOT NULL,
  `lead_id` int(10) UNSIGNED NOT NULL,
  `taskcategory_id` int(10) UNSIGNED NOT NULL,
  `deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(50) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `description`, `status`, `user_assigned_id`, `user_created_id`, `lead_id`, `taskcategory_id`, `deadline`, `created_at`, `updated_at`, `priority`) VALUES
(5, 'Pop up task', 'Pop up task description', 1, 13, 12, 5, 2, '2018-11-26', '2018-11-23 04:29:03', '2018-11-26 03:47:05', 2),
(6, 'Application Form', 'This is It Task', 2, 14, 12, 2, 1, '2018-11-26', '2018-11-23 04:30:11', '2018-11-23 05:11:18', 1),
(7, 'Marketing Task1', 'Marketing Task1', 2, 11, 2, 3, 1, '2018-12-02', '2018-11-23 04:59:01', '2018-11-26 03:48:16', 3),
(8, 'Marketing Article Task1 ', 'Marketing Article Task1 ', 1, 17, 2, 3, 2, '2018-11-26', '2018-11-23 04:59:32', '2018-12-02 04:59:32', 1),
(9, 'Marketing SNSTask2', 'Marketing SNSTask2', 1, 11, 2, 8, 1, '2018-11-26', '2018-11-23 05:00:12', '2018-11-23 05:00:12', 1),
(10, 'Marketing Article Task2', 'Marketing Article Task2', 1, 15, 2, 3, 2, '2018-11-26', '2018-11-23 05:00:41', '2018-11-23 05:00:41', 3),
(11, 'QT wallet', 'this is task for IT', 1, 3, 12, 10, 1, '2018-11-26', '2018-11-23 07:57:38', '2018-11-23 07:57:38', 1),
(12, 'Zeeshan task today', 'Please do....', 1, 3, 1, 10, 1, '2018-11-26', '2018-11-23 09:29:41', '2018-11-23 09:29:41', 1),
(13, 'test SNS2', 'test SNS2', 1, 18, 1, 8, 1, '2018-11-28', '2018-11-25 13:19:11', '2018-11-25 13:19:11', 1),
(14, 'TestSNS Urgent Task', 'TestSNS Urgent Task', 1, 11, 1, 4, 1, '2018-11-28', '2018-11-25 13:20:43', '2018-11-25 13:20:43', 1),
(15, 'Test SNS Urgent issue', 'Test SNS Urgent issue', 1, 15, 1, 3, 1, '2018-11-28', '2018-11-25 13:23:47', '2018-11-25 13:23:47', 1),
(16, 'test SNS Posting std', 'test SNS Posting std', 1, 17, 1, 8, 1, '2018-11-28', '2018-11-25 13:28:19', '2018-11-25 13:28:19', 2),
(17, 'SNS urgent', 'SNS urgent', 1, 16, 1, 8, 1, '2018-11-28', '2018-11-25 13:28:55', '2018-11-25 13:28:55', 3),
(18, 'Article Standard Task', 'Article Standard Task', 1, 18, 1, 9, 2, '2018-11-28', '2018-11-25 13:29:44', '2018-11-25 13:29:44', 2),
(19, 'IT urgent task', 'IT urgent task', 2, 14, 1, 5, 0, '2018-11-28', '2018-11-25 13:30:38', '2018-12-05 02:34:27', 3);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `best_answer` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `title`, `details`, `created_at`, `updated_at`, `user_id`, `best_answer`, `slug`, `status`) VALUES
(12, 'Why do we use it', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2018-11-13 07:47:25', '2018-11-13 07:47:25', 1, NULL, NULL, 1),
(14, 'Where does it come from', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '2018-11-13 07:49:50', '2018-11-13 07:49:50', 2, NULL, NULL, 1),
(15, 'Lorem Ipsum1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2018-11-13 07:50:25', '2018-11-13 07:50:25', 2, NULL, NULL, 2),
(16, 'Lorem Ipsum21', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.1', '2018-11-13 07:53:08', '2018-11-16 09:04:41', 1, NULL, NULL, 1),
(17, 'Where can I get some', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', '2018-11-13 09:54:43', '2018-11-13 09:54:43', 1, NULL, NULL, 1),
(18, 'What is Lorem Ipsumfdggdhgfdc', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2018-11-14 01:57:38', '2018-11-14 08:50:25', 1, NULL, NULL, 2),
(19, 'Test Topic Title1 Test Topic ', 'Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1Test Topic Title1', '2018-11-14 08:52:29', '2018-11-14 08:52:29', 2, NULL, NULL, 1),
(20, 'TEST title123456789fghfdsadfghfds', 'TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789TEST title123456789', '2018-11-14 09:11:38', '2018-11-14 09:11:38', 1, NULL, NULL, 1),
(21, 'kandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk 1111', 'kandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskl dklsfjkedk lasdkfdkgjfkskdgmka ndskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskldklsfjkedk lasdkfdkgjfkskdgmkandskskl dklsfjkedk lasdkfdkgjfkskdgmkand skskldklsfjkedk lasdkfdkgjfkskdgmkandskskld klsfjkedk lasdkfdkgjfkskdgm', '2018-11-14 09:12:21', '2018-11-15 01:31:54', 2, NULL, NULL, 1),
(22, 'MC wallet Issues for support to post now', 'all MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be under this topicall MC wallet issues will be underjhhhjh', '2018-11-14 09:27:22', '2018-11-14 09:29:50', 1, NULL, NULL, 1),
(23, 'dffggfgfdfgfhgfdsfghjkhgfdgh', 'dffggfgfdfgfhgfdsfghjkhgfdghd ffggfgfdfgfhgfdsfghjkhgfdghd ffggfgfdfgfhgfdsfghjkhgfdghdffg gfgfdfgfhgfdsfghjkhgfdghdffggfgf dfgfhgfdsfghjkhgfdghdffggfgfdfgfh gfdsfghjkhgfdghdffggfgfdfgfhgfdsf ghjkhgfdghdffggfgfdfgfhgfdsfghjk hgfdghdffggfgfdfgfhgfdsfghjkhgfd ghdffggfgfdfgfhgfdsfghjkhgfdghdf fggfgfdfgfhgfdsfghjkhgfdghdffgg fgfdfgfhgfdsfghjkhgfdghdffggfgf dfgfhgfdsfghjkhgfdghdffggfgfdfgfh gfdsfghjkhgfdghdffggfgfdfgfhgfdsf ghjkhgfdghdffggfgfdfgfhgfdsfghjk hgfdgh', '2018-11-15 03:29:21', '2018-11-15 03:29:21', 1, NULL, NULL, 1),
(24, 'MC WALLET LOGIN ISSUES', '\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\nAll Login Issues Will Be here\r\n', '2018-11-15 03:38:58', '2018-11-15 03:38:58', 1, NULL, NULL, 1),
(26, 'MC WALLET ISSUES', 'Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues Please Paste here all Mc Wallet Isuues ', '2018-11-16 06:48:21', '2018-11-21 07:14:51', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `work_number`, `personal_number`, `image_path`, `remember_token`, `created_at`, `updated_at`, `provider`, `provider_id`, `slug`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$Iw5St.1.qaRsBBXmTSIgQOY0emWu/hu1XVMreWcUC61BqWER5cPOy', 'test address', '6789023874', '9789023874', 'BmubsnBF_abbc_logo.png', 'qE2UO1Hvm9b0g4UeIpQq5yjc9tCjuU6Ow8QKE1e9M21EpbKrS6OZmdBUsKVO', '2016-06-04 09:42:19', '2018-11-26 09:15:59', NULL, NULL, NULL),
(2, 'Suresh', 'suresh@gmail.com', '$2y$10$Rlbgk3rcNfEUoDACg2mp.eaRHvfcnKP0lf1Ss3iIN54MJL5UDwuzu', 'testingaddress', '4789023874', '6789023874', 'OjqUAEJ9_suresh-kanan.png', 'qez4Ul3BXQADiJHk3TUbtoEvWB9ebXmnmXtT9BNj9MECQzPLYqV1hZMdC2Ww', '2018-11-13 03:26:14', '2018-11-23 04:18:25', NULL, NULL, NULL),
(3, 'Zeeshan', 'zeeshan@gmail.com', '$2y$10$kFONX7rkQ9PgRAC1lZ.X2eTbhif3QtgNxKbVQbyN8ETAOrWxs6WtC', 'testingaddress', '7890876578', '7654356781', '5KgtsCJe_zeeshan-butt.png', 'xui9HZkLTo9qcLjiWX48iCHnO4xbq0d8tdbT5z4JofNLH78aXYJn3YA8IzBh', '2018-11-14 02:48:29', '2018-11-23 04:16:33', NULL, NULL, NULL),
(11, 'Yahya Khan', 'yahyakhan@gmail.com', '$2y$10$qUM2/PtsPnjFeFNJXgXGpOwm/tzzgD10CzG62Yl77BQX./0S6uJFu', 'testingaddress', '6789023874', '5437829248', 'RugOjl3g_ahy.png', NULL, '2018-11-21 10:07:12', '2018-11-23 04:19:00', NULL, NULL, NULL),
(12, 'Jene Claude', 'claude@gmail.com', '$2y$10$h5.dUisjl5k0JbKaXFfVXOZyNKB4SUDu2vlQwUpdE8YLJLSFVlMXy', 'dubai', '0252525', '025252525', 'lQfgZGwi_jene-claude.png', NULL, '2018-11-23 04:22:55', '2018-11-23 04:22:55', NULL, NULL, NULL),
(13, 'Awais', 'awais@gmail.com', '$2y$10$eG/pOiQSQOKe1kPvjaQvNet1gAF1.iqwzZsBT41mso7Bb6CBwXjiO', 'test address', '87654356789', '97654356789', 'q73vUQBz_awais-sakhi.png', NULL, '2018-12-05 02:09:15', '2018-12-05 02:09:15', NULL, NULL, NULL),
(14, 'Kanagaraj', 'raj@gmail.com', '$2y$10$TwcwAv9rI8Fe.Eau9IQjRuMZaFM116jLiWoK579JNDFdv9mW2z/uS', 'New address', '987654356756', '687654356756', 'A3sZGD2q_raj.png', NULL, '2018-12-05 02:12:13', '2018-12-05 02:12:13', NULL, NULL, NULL),
(15, 'Tim', 'tim@gmail.com', '$2y$10$Snod9ROZKTa0dR4xwAwvtOaDktJFh1yK2FKCBGZW3SxEt2TWFLgrC', 'New address', '1234567654', '2234567654', 'IPZcvON6_Timothy.png', NULL, '2018-12-05 02:13:35', '2018-12-05 02:13:35', NULL, NULL, NULL),
(16, 'Lems', 'lems@gmail.com', '$2y$10$hiixCZ/Tb4l/wBrYxu.gU.BU7xF5EcRUNO710XSl32h0ee2O3L7xe', 'New address', '65435678765', '35435678765', 'ObgEIxNu_lemuel.png', NULL, '2018-12-05 02:14:24', '2018-12-05 02:14:24', NULL, NULL, NULL),
(17, 'Shabaz', 'shabaz@gmail.com', '$2y$10$CMl.Ca463PRuy78k0poTf.4LZWI4xNXPud1R14CfKKMFyr2horVPi', 'New address', '456789876543', '256789876543', 'RGCbh5eN_shabaz.png', NULL, '2018-12-05 02:15:44', '2018-12-05 02:15:44', NULL, NULL, NULL),
(18, 'Maya', 'maya@gmail.com', '$2y$10$0GqDN/8PpmtmjSHus8TPoe6lnUFEAqKzJUbuznQZJZuqk97jViBMq', 'New address', '7890876578', '8765432345', 'ZXxYTl5X_maya-ahmed.png', NULL, '2018-12-05 04:25:50', '2018-12-05 04:25:50', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountexpenses`
--
ALTER TABLE `accountexpenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clients_email_unique` (`email`),
  ADD KEY `clients_user_id_foreign` (`user_id`),
  ADD KEY `clients_industry_id_foreign` (`industry_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_source_id_source_type_index` (`source_id`,`source_type`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_commentable_id_index` (`commentable_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_user`
--
ALTER TABLE `department_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_user_department_id_foreign` (`department_id`),
  ADD KEY `department_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_client_id_foreign` (`client_id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `integrations`
--
ALTER TABLE `integrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_client_id_foreign` (`client_id`);

--
-- Indexes for table `invoice_lines`
--
ALTER TABLE `invoice_lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_lines_invoice_id_foreign` (`invoice_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leads_user_assigned_id_foreign` (`user_assigned_id`),
  ADD KEY `leads_user_created_id_foreign` (`user_created_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `officials`
--
ALTER TABLE `officials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page-views`
--
ALTER TABLE `page-views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projectuploads`
--
ALTER TABLE `projectuploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taskcategories`
--
ALTER TABLE `taskcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_user_assigned_id_foreign` (`user_assigned_id`),
  ADD KEY `tasks_user_created_id_foreign` (`user_created_id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountexpenses`
--
ALTER TABLE `accountexpenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department_user`
--
ALTER TABLE `department_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `integrations`
--
ALTER TABLE `integrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_lines`
--
ALTER TABLE `invoice_lines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `officials`
--
ALTER TABLE `officials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `page-views`
--
ALTER TABLE `page-views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `projectuploads`
--
ALTER TABLE `projectuploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `taskcategories`
--
ALTER TABLE `taskcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`),
  ADD CONSTRAINT `clients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `department_user`
--
ALTER TABLE `department_user`
  ADD CONSTRAINT `department_user_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `department_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `invoice_lines`
--
ALTER TABLE `invoice_lines`
  ADD CONSTRAINT `invoice_lines_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`);

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `leads_user_assigned_id_foreign` FOREIGN KEY (`user_assigned_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `leads_user_created_id_foreign` FOREIGN KEY (`user_created_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_user_assigned_id_foreign` FOREIGN KEY (`user_assigned_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `tasks_user_created_id_foreign` FOREIGN KEY (`user_created_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
