<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            ['users.show'],
            'App\Http\ViewComposers\UserHeaderComposer'
        );

        view()->composer(
            ['tasks.show'],
            'App\Http\ViewComposers\TaskHeaderComposer'
        );
        view()->composer(
            ['leads.show'],
            'App\Http\ViewComposers\LeadHeaderComposer'
        );

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
