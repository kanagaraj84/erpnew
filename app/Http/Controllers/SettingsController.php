<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Session;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Department;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Http\Requests\Setting\UpdateSettingOverallRequest;

class SettingsController extends Controller
{
    protected $settings;
    protected $roles;
    protected $users;
    protected $departments;

    /**
     * SettingsController constructor.
     * @param SettingRepositoryContract $settings
     * @param RoleRepositoryContract $roles
     */
    public function __construct(
        SettingRepositoryContract $settings,
        UserRepositoryContract $users,
        DepartmentRepositoryContract $departments,
        RoleRepositoryContract $roles
    )
    {
        $this->settings = $settings;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->users = $users;
        $this->middleware('user.is.admin', ['only' => ['index']]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $settings = $this->settings->getSetting();
        $permission = $this->roles->allPermissions();
        $roles = $this->roles->allRoles();
        return view('settings.index', compact('departments','allUsersdepartments','settings','permission','roles'));

    }

    /**
     * @param UpdateSettingOverallRequest $request
     * @return mixed
     */
    public function updateOverall(UpdateSettingOverallRequest $request)
    {
        $this->settings->updateOverall($request);
        Session::flash('flash_message', 'Overall settings successfully updated');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function permissionsUpdate(Request $request)
    {
        $this->roles->permissionsUpdate($request);
        Session::flash('flash_message', 'Role is updated');
        return redirect()->back();
    }
}
