<?php
 
namespace App\Http\Controllers;
 
use App\Accountexpenses;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;


class MaatwebsiteDemoController extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExport()
    {
        return view('importExport');
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcel($type)
    {
        $data = Item::get()->toArray();
            
        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {
        /*$request->validate([
            'import_file' => 'required'
        ]);*/
 
        $path = $request->file('import_file')->getRealPath();

        $data = \Excel::load($path)->get();

        /*$data = \Excel::load($path, function($reader) {
        })->get();*/

        $title = $request->title;
        $branch_id = $request->branch_id;

        //echo "<pre>";print_r($data); die;

        if($data->count()){

            $arr = array();
            foreach ($data as $key => $value) {

                $curdate = date('Y-m-d h:i:s');
                $payments = $value->payments;

                if($payments){
                    $arr[] = ['title' => $title, 'branch_id' => $branch_id, 'openbalance' => $value->ob, 'payments' => $value->payments,
                        'amount' => $value->amount, 'total_indirect_expense' => $value->total_indirect_expense,
                        'created_at' => $curdate, 'updated_at' => $curdate];
                }


            }

           echo "<pre>";print_r($arr); die;
 
            if(!empty($arr)){
                Accountexpenses::insert($arr);
            }
        }
 
        return back()->with('success', 'Insert Record successfully.');
    }
}