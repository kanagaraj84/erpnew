<?php
namespace App\Http\Controllers;

use DB;
use Carbon;
use Yajra\Datatables\Datatables;
use App\Models\User;
use App\Models\Task;
use App\Models\Department;
use App\Models\Officials;
use Illuminate\Http\Request;
use App\Models\Lead;
use App\Models\Projectuploads;
use App\Models\Taskcategory;
use App\Models\Comment;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Taskcategory\TaskcategoryRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Comment\CommentRepositoryContract;
use Illuminate\Http\File;


class PagesController extends Controller
{

    protected $users;
    protected $clients;
    protected $settings;
    protected $tasks;
    protected $leads;
    protected $roles;
    protected $departments;
    protected $taskcategories;
    protected $comments;
    protected $request;

    public function __construct(
        UserRepositoryContract $users,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads,
        CommentRepositoryContract $comments,
        TaskcategoryRepositoryContract $taskcategories
    ) {
        $this->users = $users;
        $this->clients = $clients;
        $this->settings = $settings;
        $this->tasks = $tasks;
        $this->comments = $comments;
        $this->leads = $leads;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->taskcategories = $taskcategories;
    }

    /**
     * Dashobard view
     * @return mixed
     */
    public function dashboard(Request $request)
    {
        $sureDelete = __('Are you sure want to Delete?');
        if(isset($request->dates)){
            $dates = $request->dates;
            $dateArr = explode(' - ',$dates);
            $startDate = $dateArr[0];
            $endDate = $dateArr[1];
            $explDate = explode('/',$startDate);
            $csDate = $explDate[2]."-".$explDate[0]."-".$explDate[1];

            $endexplDate = explode('/',$endDate);
            $ceDate = $endexplDate[2]."-".$endexplDate[0]."-".$endexplDate[1];

            /**
             * Statistics for leads between two dates             *
             */
            $leadCompletedBetweenDates = $this->leads->leadCompletedBetweenDates($csDate,$ceDate);

            foreach($leadCompletedBetweenDates as $thisDate){
                $totalLeadCompletedBetweenDates = $thisDate->total;
            }

            $leadPendingBetweenDates = $this->leads->leadPendingBetweenDates($csDate,$ceDate);
            foreach($leadPendingBetweenDates as $thisDate){
                $totalLeadPendingBetweenDates = $thisDate->total;
            }

            /**
             * Statistics for tasks between two dates
             *
             */
            $completedTasksBetweenDatesCount = $this->tasks->completedTasksBetweenDatesCount($csDate,$ceDate);
            foreach($completedTasksBetweenDatesCount as $thisDate){
                $totalCompletedTasksBetweenDates = $thisDate->total;
            }
            $pendingTasksBetweenDatesCount = $this->tasks->pendingTasksBetweenDatesCount($csDate,$ceDate);
            foreach($pendingTasksBetweenDatesCount as $thisDate){
                $totalPendingTasksBetweenDates = $thisDate->total;
            }

            /**
             * Statistics for Email Issues between two dates
             *
             */
            $CompEmailBetDateCnt = $this->comments->allCompletedEmailIssuesBetweenDatesCount($csDate,$ceDate);
            foreach($CompEmailBetDateCnt as $thisDate){
                $totalCompEmailBetDateCnt = $thisDate->total;
            }
            $PendingEmailBetDateCnt = $this->comments->allPendingEmailIssuesBetweenDatesCount($csDate,$ceDate);
            foreach($PendingEmailBetDateCnt as $thisDate){
                $totalPendingEmailBetDateCnt = $thisDate->total;
            }

            /**
             * Statistics for Articles between two dates
             *
             */
            $allArticleMarketTasksBetDates = $this->tasks->allArticleMarketTasksBetweenDates($csDate,$ceDate);
            foreach($allArticleMarketTasksBetDates as $thisDate){
                $totalArticleMarketTasksBetDates = $thisDate->total;
            }
            /**
             * Statistics for SNS between two dates
             *
             */
            $allSNSMarketTasksBetDates = $this->tasks->allSNSMarketTasksBetweenDates($csDate,$ceDate);
            foreach($allSNSMarketTasksBetDates as $thisDate){
                $totalSNSMarketTasksBetDates = $thisDate->total;
            }

            $dateBetweenchartVal = $totalLeadCompletedBetweenDates.",".$totalLeadPendingBetweenDates.",".$totalCompletedTasksBetweenDates.",".$totalPendingTasksBetweenDates.",".$totalCompEmailBetDateCnt.",".$totalPendingEmailBetDateCnt.",".$totalArticleMarketTasksBetDates.','.$totalSNSMarketTasksBetDates;

            $startDateVal = $csDate;
            $endDateVal = $ceDate;
        } else{
            $dateBetweenchartVal = '0,0,0,0,0,0,0,0';
            $startDateVal = $endDateVal = '';
        }

      /**
         * Other Statistics
         *
         */
        $companyname = $this->settings->getCompanyName();
        $users = $this->users->getAllUsers();

        $userswithAdmin = $this->users->getAllUserswithAdmin();
        $totalClients = $this->clients->getAllClientsCount();
        $totalUsers = $this->users->getAllUsersCount();
        $totalTimeSpent = $this->tasks->totalTimeSpent();

        $alluserManagers = $this->users->getAllUsersManagers();
        $allLeads = $this->leads->getAllLeads();

     /**
      * Statistics for all-time tasks.
      *
      */
        $alltasks = $this->tasks->tasks();
        $allCompletedTasks = $this->tasks->allCompletedTasks();
        $allPendingTasks = $this->tasks->allPendingTasks();

        $totalPercentageTasks = $this->tasks->percantageCompleted();

     /**
      * Statistics for today tasks.
      *
      */
        $completedTasksToday =  $this->tasks->completedTasksToday();
        $pendingTasksToday =  $this->tasks->pendingTasksToday();
        $createdTasksToday = $this->tasks->createdTasksToday();

     /**
      * Statistics for tasks this month.
      *
      */
         $taskCompletedThisMonth = $this->tasks->completedTasksThisMonth();
    
         $taskPendingThisMonth = $this->tasks->pendingTasksThisMonth();
     /**
      * Statistics for tasks each month(For Charts).
      *
      */
        $createdTasksMonthly = $this->tasks->createdTasksMothly();
        $completedTasksMonthly = $this->tasks->completedTasksMothly();

     /**
      * Statistics for all-time Leads.
      *
      */
     
        $allleads = $this->leads->leads();
        $allCompletedLeads = $this->leads->allCompletedLeads();
        $allPendingLeads = $this->leads->allPendingLeads();
        $totalPercentageLeads = $this->leads->percantageCompleted();
     /**
      * Statistics for today leads.
      *
      */
        $completedLeadsToday = $this->leads->completedLeadsToday();
        $pendingLeadsToday = $this->leads->pendingLeadsToday();
        $createdLeadsToday = $this->leads->createdLeadsToday();

     /**
      * Statistics for leads this month.
      *
      */
        $leadCompletedThisMonth = $this->leads->completedLeadsThisMonth();
        $leadPendingThisMonth = $this->leads->pendingLeadsThisMonth();

     /**
      * Statistics for leads each month(For Charts).
      *
      */
        $completedLeadsMonthly = $this->leads->createdLeadsMonthly();
        $createdLeadsMonthly = $this->leads->completedLeadsMonthly();

        $departments = $this->departments->getAllDepartments();
        $taskcategories = $this->taskcategories->listAllTaskcategory();
        $roles = $this->roles->listAllRoles();
        $alluserEmployees = $this->users->getAllUsersEmployees();
        $totalMarketingLeads = $this->leads->totalMarketingLeads();
        $getAllMarketingLeads = $this->leads->getAllMarketingLeads();

        $getAllMarketingLeadArr = array();
        $i = 0;
        foreach ($getAllMarketingLeads as $getAllMarketingLead)
        {
            $getAllMarketingLeadArr[] .= $getAllMarketingLeads[$i]['id'];
            $i++;
        }

        $allCompletedMarketLeads = $this->leads->allCompletedMarketLeads();
        $allPendingMarketLeads = $this->leads->allPendingMarketLead();

        $allCompletedMarketTasks = $this->tasks->allCompletedMarketTasks($getAllMarketingLeadArr);
        $allPendingMarketTasks = $this->tasks->allPendingMarketTasks($getAllMarketingLeadArr);

        $allArticleMarketTasks = $this->tasks->allArticleMarketTasks();
        $allSNSMarketTasks = $this->tasks->allSNSMarketTasks();

        $allCompletedEmailIssues = $this->comments->allCompletedEmailIssues();
        $allPendingEmailIssues = $this->comments->allPendingEmailIssues();

        //Monthly Email Issues
        $allMonthlyCompletedEmailIssues = $this->comments->allMonthlyCompletedEmailIssues();
        $allMonthlyPendingEmailIssues = $this->comments->allMonthlyPendingEmailIssues();

        //Daily Email Issues
        $allCompletedEmailIssuesToday = $this->comments->allCompletedEmailIssuesToday();
        $allPendingEmailIssuesToday = $this->comments->allPendingEmailIssuesToday();

        //Monthly Articles/SNS
        $allMonthlyArticleMarketTasks = $this->tasks->allMonthlyArticleMarketTasks();
        $allMonthlySNSMarketTasks = $this->tasks->allMonthlySNSMarketTasks();

        //Daily Articles/SNS
        $allArticleMarketTasksToday = $this->tasks->allArticleMarketTasksToday();
        $allSNSMarketTasksToday = $this->tasks->allSNSMarketTasksToday();

        $getAllITLeads = $this->leads->getAllItLeads();

        $getAllITLeadArr = array();
        $i = 0;
        foreach ($getAllITLeads as $getAllITLead) {
            $getAllITLeadArr[] .= $getAllITLeads[$i]['id'];
            $i++;
        }


        $allCompletedITTasks = $this->tasks->allCompletedITTasks($getAllITLeadArr);
        $allPendingITTasks = $this->tasks->allPendingITTasks($getAllITLeadArr);
        $allitCompletedLeads = $this->leads->allCompletedITLeads();
        $allPendingITLeads = $this->leads->allPendingITLead();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();

        $allOffcicials = Officials::all();

        return view('pages.dashboard', compact(
             'taskPendingThisMonth',
            'createdTasksMonthly',
            'completedTasksMonthly',
            'completedLeadsMonthly',
            'createdLeadsMonthly',
            'taskCompletedThisMonth',
            'leadCompletedThisMonth',
            'leadPendingThisMonth',
            'totalTimeSpent',
            'totalClients',
            'totalUsers',
            'users',
            'companyname',
            'alltasks',
            'allCompletedTasks',
            'allPendingTasks',
            'totalPercentageTasks',
            'allleads',
            'allCompletedLeads',
            'allPendingLeads',
            'totalPercentageLeads',
            'alluserManagers',
            'allLeads',
            'departments',
            'taskcategories',
            'roles',
            'alluserEmployees',
            'totalMarketingLeads',
            'allCompletedMarketLeads',
            'allPendingMarketLeads',
            'allCompletedMarketTasks',
            'allPendingMarketTasks',
            'allCompletedEmailIssues',
            'allPendingEmailIssues',
            'allArticleMarketTasks',
            'allSNSMarketTasks',

            'allMonthlyCompletedEmailIssues',
            'allMonthlyPendingEmailIssues',
            'allMonthlyArticleMarketTasks',
            'allMonthlySNSMarketTasks',

            'createdTasksToday',
            'createdLeadsToday',
            'pendingLeadsToday',
            'pendingTasksToday',
            'completedTasksToday',
            'completedLeadsToday',
            'allCompletedEmailIssuesToday',
            'allPendingEmailIssuesToday',
            'allArticleMarketTasksToday',
            'allSNSMarketTasksToday',
            'alliteleadscount',
            'allCompletedITTasks',
            'allPendingITTasks',
            'allitCompletedLeads',
            'allPendingITLeads',
            'allUsersdepartments',
            'allOffcicials',
            'dateBetweenchartVal',
            'startDateVal',
            'endDateVal',
             'sureDelete',
            'userswithAdmin'
        ));
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function taskData()
    {
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
            )->get();

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('lead_id', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$tasks->lead->title. '</a>';
            })

            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$user->name. '</a>';
            })

            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })

            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->addColumn('edit', function ($tasks) {
                return '<a href="' . route("tasks.edit", $tasks->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })

            ->addColumn('documents', function ($tasks) {
                $fpath = url('/task_documents/'.$tasks->id);
                $dir_path = public_path() . "/task_documents/".$tasks->id."/*";
                $fileList = glob($dir_path);
                $fileArr = array();
                foreach($fileList as $fname){
                    $filenamestr= explode('/',$fname);
                    $filenamestr = end($filenamestr);
                    $fileArr[] .= $filenamestr;
                }
                $i=1;
                $allfileStr = '';
                foreach($fileArr as $filename){
                    //Simply print them out onto the screen.
                    $allfileStr .= "<a href='".$fpath."/".$filename."'>File ".$i."</a> <br />";
                    $i++;
                }
                return $allfileStr;
             })
            ->make(true);
    }


    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function markettaskData()
    {
        $leads = Lead::select(
            ['id', 'title', 'description','user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status']
        )->where('dept_id', '=',2)->get();

        $leadArr = array();
        foreach ($leads as $lead){
            $leadArr[] = $lead->id;
        }
        
        if(count($leadArr)>0){
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
            )->whereIn('lead_id', $leadArr)->get();
        } else {
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
            )->get();
        }

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('lead_id', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->lead->title. '</a>';
            })

            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' . '<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' . '<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' . '<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })

            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($user->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$user->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$imgpath. $user->name. '</a>';
            })

            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="' . route('tasks.show', $tasks->id) . '">' . '<button type="button" class="btn btn-success btn-sm btn-block ">Open</button>'. '</a>' : '<a href="' . route('tasks.show', $tasks->id) . '">' . '<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->addColumn('edit', function ($tasks) {
                return '<a href="' . route("tasks.edit", $tasks->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })
            ->make(true);
    }


    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function leadData()
    {
        $leads = Lead::select(
                ['id', 'title', 'user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status', 'priority']
            )->get();

        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="' . route('leads.show', $leads->id) . '">' . $leads->title . '</a>';
            })
            ->editColumn('created_at', function ($leads) {
                return $leads->created_at ? with(new Carbon($leads->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('contact_date', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('user_assigned_id', function ($leads) {
                $userdetails = User::findOrFail($leads->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('leads.show', $leads->id) . '">' .$imgpath.$userdetails->name. '</a>';
            })

            ->editColumn('dept_id', function ($leads) {
                $deptdata = Department::findOrFail($leads->dept_id);
                return '<a href="' . route('leads.show', $leads->id) . '">' .$deptdata->name. '</a>';
            })

            ->editColumn('status', function ($leads) {
                return $leads->status == 1 ? '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->editColumn('priority', function ($leads) {
                if($leads->priority==1){
                    return '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($leads->priority==2){
                    return '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($leads->priority==3) {
                    return '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })

            ->addColumn('documents', function ($leads) {
                $fpath = url('/project_documents/'.$leads->id);
                $dir_path = public_path() . "/project_documents/".$leads->id."/*";
                $fileList = glob($dir_path);
                $fileArr = array();
                foreach($fileList as $fname){
                    $filenamestr= explode('/',$fname);
                    $filenamestr = end($filenamestr);
                    $fileArr[] .= $filenamestr;
                }
                $i=1;
                $allfileStr = '';
                foreach($fileArr as $filename){
                    $allfileStr .= "<a href='".$fpath."/".$filename."' class='btn btn-primary btn-sm'>File ".$i."</a> &nbsp; &nbsp;";
                    $i++;
                }
                return $allfileStr;
            })

            ->make(true);
    }


    public function itleaddata()
    {
        $leads = Lead::select(
            ['id', 'title', 'description', 'user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status', 'priority']
        )->where('dept_id', '=', 3);

        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="' . route('leads.show', $leads->id) . '">' . $leads->title . '</a>';
            })
            ->editColumn('user_assigned_id', function ($leads) {
                $userdetails = User::findOrFail($leads->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('leads.show', $leads->id) . '">' . $imgpath.$userdetails->name. '</a>';
            })
            ->editColumn('created_at', function ($leads) {
                return $leads->created_at ? with(new Carbon($leads->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('status', function ($leads) {
                return $leads->status == 1 ? '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->editColumn('priority', function ($leads) {
                if($leads->priority==1){
                    return '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($leads->priority==2){
                    return '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($leads->priority==3) {
                    return '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })
            ->make(true);
    }

    public function ittaskData()
    {
        $leads = Lead::select(
            ['id', 'title', 'description', 'user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status','priority']
        )->where('dept_id', '=', 3)->get();

        $leadArr = array();
        foreach ($leads as $lead) {
            $leadArr[] = $lead->id;
        }

        if (count($leadArr) > 0) {
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
            )->whereIn('lead_id', $leadArr)->get();
        } else {
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
            )->get();
        }

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('lead_id', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$tasks->lead->title. '</a>';
            })
            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })
            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($user->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$user->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$imgpath.$user->name. '</a>';
            })
            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('tasks.show', $tasks->id) . '">'.'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->addColumn('edit', function ($tasks) {
                return '<a href="' . route("tasks.edit", $tasks->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })
            ->make(true);
    }


    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function marketleadData()
    {
        $leads = Lead::select(
            ['id', 'title', 'description','user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status','priority']
        )->where('dept_id', '=',2);

        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="' . route('leads.show', $leads->id) . '">' . $leads->title . '</a>';
            })

            ->editColumn('user_assigned_id', function ($leads) {
                $userdetails = User::findOrFail($leads->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('leads.show', $leads->id) . '">' .$imgpath.$userdetails->name. '</a>';
            })
            ->editColumn('created_at', function ($leads) {
                return $leads->created_at ? with(new Carbon($leads->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('priority', function ($leads) {
                if($leads->priority==1){
                    return '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($leads->priority==2){
                    return '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($leads->priority==3) {
                    return '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })

            ->editColumn('status', function ($leads) {
                return $leads->status == 1 ? '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('leads.show', $leads->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })
            ->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function repoMarketdata()
    {
        $leads = Lead::select(
            ['id', 'title', 'description', 'user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status','priority']
        )->where('dept_id', '=', 2)->get();
        $leadArr = array();
        foreach ($leads as $lead) {
            $leadArr[] = $lead->id;
        }
        $fileuploads = Projectuploads::where('taskcategory_id', "!=",0)
            ->whereIn('project_id', $leadArr)->get();

        return Datatables::of($fileuploads)
            ->addColumn('titlelink', function ($fileuploads) {
                return $fileuploads->project_name;
            })

            ->editColumn('project_id', function ($fileuploads) {
                $leads = Lead::select(
                    ['id', 'title', 'user_assigned_id', 'dept_id']
                )->where('dept_id', '=', 2)->get();

                foreach ($leads as $leads) {

                    if($leads->id==$fileuploads->project_id){
                        return $leads->title;
                    }
                }
            })
            ->editColumn('taskcategory_id', function ($fileuploads) {
                $taskcategories = Taskcategory::select(
                    ['id', 'name', 'description', 'created_at', 'updated_at']
                )->get();
                foreach($taskcategories as $taskcategory){
                    if($fileuploads->taskcategory_id==$taskcategory->id){
                        return $taskcategory->name;
                    }
                }
            })
            ->editColumn('user_id', function ($fileuploads) {
                $userdetails = User::findOrFail($fileuploads->user_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('leads.show', $fileuploads->project_id) . '">' .$imgpath.$userdetails->name. '</a>';
            })
            ->editColumn('created_at', function ($fileuploads) {
                return $fileuploads->created_at ? with(new Carbon($fileuploads->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('uploaded_files', function ($fileuploads) {

                    $fpath = url('/completed_market_projects/'.$fileuploads->taskcategory_id."_".$fileuploads->project_id."/");
                    $dir_path = public_path() . '/completed_market_projects/'.$fileuploads->taskcategory_id."_".$fileuploads->project_id.'/';
                    $dir = new \DirectoryIterator($dir_path);
                    $i=1;
                if (file_exists($dir_path)) {
                    foreach ($dir as $fileinfo) {
                        if (!$fileinfo->isDot()) {
                            return "<a href='" . $fpath . "/" . $fileinfo . "' class='btn btn-primary btn-sm' target='_blank'>View File</a> &nbsp; &nbsp;";
                            $i++;
                        }
                    }
                } else {
                    return "No Attachments";
                }
            })->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function repoITdata()
    {
        $leads = Lead::select(
            ['id', 'title', 'description', 'user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status','priority']
        )->where('dept_id', '=', 3)->get();

        $leadArr = array();
        foreach ($leads as $lead) {
            $leadArr[] = $lead->id;
        }
        $fileuploads = Projectuploads::where('taskcategory_id', "=",0)
            ->whereIn('project_id', $leadArr)->get();
        return Datatables::of($fileuploads)
            ->editColumn('titlelink', function ($fileuploads) {
                return $fileuploads->project_name;
            })

            ->editColumn('project_id', function ($fileuploads) {
                $leads = Lead::select(['id', 'title', 'user_assigned_id', 'dept_id'])->where('dept_id', '=', 3)->get();
                foreach ($leads as $leads) {
                    if($leads->id==$fileuploads->project_id){
                        return $leads->title;
                    }
                }
            })

            ->editColumn('user_id', function ($fileuploads) {
                $userdetails = User::findOrFail($fileuploads->user_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('leads.show', $fileuploads->project_id) . '">' .$imgpath.$userdetails->name. '</a>';
            })
            ->editColumn('created_at', function ($fileuploads) {
                return $fileuploads->created_at ? with(new Carbon($fileuploads->created_at))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('uploaded_files', function ($fileuploads) {
                $fpath = url('/completed_it_projects/'.$fileuploads->taskcategory_id."_".$fileuploads->project_id."/");
                $dir_path = public_path() . '/completed_it_projects/'.$fileuploads->taskcategory_id."_".$fileuploads->project_id.'/';
                $dir = new \DirectoryIterator($dir_path);
                $i=1;
               if (file_exists($dir_path)) {
                     foreach ($dir as $fileinfo) {
                         if (!$fileinfo->isDot()) {
                             return "<a href='" . $fpath . "/" . $fileinfo . "' class='btn btn-primary btn-sm' target='_blank'>View File</a> &nbsp; &nbsp;";
                             $i++;
                         }
                     }
                 } else {
                     return "No Attachments";
                 }
            })->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function userData()
    {
        $users = User::select(
            ['id', 'name', 'email', 'address', 'work_number', 'personal_number', 'created_at', 'updated_at']
        )->get();

        return Datatables::of($users)
            ->addColumn('name', function ($users) {
                return $users->name;
            })

            ->editColumn('email', function ($users) {
                return $users->email;
            })

            ->editColumn('address', function ($users) {
                return $users->address;
            })

            ->editColumn('work_number', function ($users) {
                return $users->work_number;
            })

            ->editColumn('personal_number', function ($users) {
                return $users->personal_number;
            })

            ->editColumn('created_at', function ($users) {
                return $users->created_at ? with(new Carbon($users->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('updated_at', function ($users) {
                return $users->updated_at ? with(new Carbon($users->updated_at))
                    ->format('d/m/Y') : '';
            })
            ->make(true);
    }


    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function officialdata(Request $request)
    {
        if(isset($request->id)){
            $id = $request->id;
            $officials = Officials::where('id', $id)->get();
        } else {
            $officials = Officials::select(
                ['id', 'title', 'user_id', 'description', 'created_at', 'updated_at']
            )->get();
        }
        $details ='<table width="100%">';
        foreach ($officials as $official){
            $userdetails = User::findOrFail($official->user_id);
            $created_at = $official->created_at ? with(new Carbon($official->created_at))
                ->format('d/m/Y') : '';
            $details .= "<tr><td width='20%'>Title :</td> <td> ".$official->title."</td></tr>"."<tr>
                            <td valign='top'>Details :</td> <td> ".$official->description."</td>
                            </tr>"."<tr><td>Posted By : </td><td>".$userdetails->name."</td></tr>"."<tr>
                            <td>Created at : </td><td>".$created_at."</td></tr>";

        }

        $details .='</table>';
        return $details;
    }



    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function pagetaskcategorydata()
    {
        $taskcategories = Taskcategory::select(
            ['id', 'name', 'description', 'created_at', 'updated_at']
        )->get();

        return Datatables::of($taskcategories)
            ->editColumn('name', function ($taskcategories) {
                return $taskcategories->name;
            })

            ->editColumn('description', function ($taskcategories) {
                return $taskcategories->description;
            })

            ->editColumn('created_at', function ($taskcategories) {
                return $taskcategories->created_at ? with(new Carbon($taskcategories->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('updated_at', function ($taskcategories) {
                return $taskcategories->updated_at ? with(new Carbon($taskcategories->updated_at))
                    ->format('d/m/Y') : '';
            })
            ->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function snstaskData()
    {
        $leads = Lead::select(
            ['id', 'title', 'description','user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status']
        )->where('dept_id', '=',2)->get();
        $leadArr = array();
        foreach ($leads as $lead){
            $leadArr[] = $lead->id;
        }

        $tasks = Task::select(
            ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
        )->where('taskcategory_id', '=',1)
         ->whereIn('lead_id', $leadArr)->get();

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })
            ->editColumn('lead_id', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$tasks->lead->title. '</a>';
            })
            ->addColumn('edit', function ($tasks) {
                return '<a href="' . route("tasks.edit", $tasks->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })
            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($user->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$user->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$imgpath.$user->name. '</a>';
            })
            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function articletaskData()
    {
        $leads = Lead::select(
            ['id', 'title', 'description','user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status']
        )->where('dept_id', '=',2)->get();
        $leadArr = array();
        foreach ($leads as $lead){
            $leadArr[] = $lead->id;
        }
        $tasks = Task::select(
            ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
        )->where('taskcategory_id', '=',2)->whereIn('lead_id', $leadArr)->get();

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('lead_id', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$tasks->lead->title. '</a>';
            })

            ->addColumn('edit', function ($tasks) {
                return '<a href="' . route("tasks.edit", $tasks->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })

            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })
            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($user->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="images/'.$companyname.'/'.$user->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$imgpath.$user->name. '</a>';
            })
            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })->make(true);
    }

}
