<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use DB;

use App\Product;

use Maatwebsite\Excel\Facades\Excel;



class ExcelController extends Controller

{

	/**

     * Create a new controller instance.

     *

     * @return void

     */

    public function importExportView(){

        return view('import_export');

    }



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function importFile(Request $request){

        if($request->hasFile('sample_file')){

            $path = $request->file('sample_file')->getRealPath();

            /*$data = \Excel::load($path, function($reader) {
                $reader->toArray();
            })->get();*/

            $data = Excel::load($path, function($reader) {
                // Getting all results
                $reader->takeColumns(10);


            });


            echo "<pre>"; print_r($data); die;


            // echo "<pre>";print_r($data); die;


            if($data->count()){

                foreach ($data as $key => $value) {

                    $arr[] = ['title' => $value->accountant_fee, 'body' => $value->body];

                }

                echo "<pre>";print_r($arr); die;

                if(!empty($arr)){

                    DB::table('products')->insert($arr);

                    dd('Insert Recorded successfully.');

                }

            }

        }

        dd('Request data does not have any files to import.');      

    } 



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function exportFile($type){

        $products = Product::get()->toArray();



        return \Excel::create('hdtuto_demo', function($excel) use ($products) {

            $excel->sheet('sheet name', function($sheet) use ($products)

            {

                $sheet->fromArray($products);

            });

        })->download($type);

    }      

}