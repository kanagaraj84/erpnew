<?php

namespace App\Http\Controllers;

use DB;
use Carbon;
use Datatables;
use App\Models\User;
use App\Models\Task;
use App\Models\Department;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Lead;
use App\Models\Taskcategory;
use App\Models\Projectuploads;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Taskcategory\TaskcategoryRepositoryContract;

class MarketingController extends Controller
{
    protected $users;
    protected $clients;
    protected $settings;
    protected $tasks;
    protected $leads;
    protected $roles;
    protected $departments;
    protected $taskcategories;

    public function __construct(
        UserRepositoryContract $users,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads,
        TaskcategoryRepositoryContract $taskcategories
    ) {
        $this->users = $users;
        $this->clients = $clients;
        $this->settings = $settings;
        $this->tasks = $tasks;
        $this->leads = $leads;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->taskcategories = $taskcategories;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        /**
         * Other Statistics
         *
         */
        $companyname = $this->settings->getCompanyName();
        $users = $this->users->getAllUsers();
        $totalClients = $this->clients->getAllClientsCount();
        $totalUsers = $this->users->getAllUsersCount();
        $totalTimeSpent = $this->tasks->totalTimeSpent();

        $alluserManagers = $this->users->getAllUsersManagers();
        $allLeads = $this->leads->getAllMarketingLeads();


        /**
         * Statistics for tasks each month(For Charts).
         *
         */
        $createdTasksMonthly = $this->tasks->createdTasksMothly();
        $completedTasksMonthly = $this->tasks->completedTasksMothly();

        /**
         * Statistics for all-time Leads.
         *
         */

        $allleads = $this->leads->leads();
        $allCompletedLeads = $this->leads->allCompletedLeads();
        $totalPercentageLeads = $this->leads->percantageMarketCompleted();
        /**
         * Statistics for today leads.
         *
         */
        $completedLeadsToday = $this->leads->completedLeadsMarketToday();
        $createdLeadsToday = $this->leads->createdLeadsMarketToday();

        /**
         * Statistics for leads this month.
         *
         */
        $leadCompletedThisMonth = $this->leads->completedLeadsMarketThisMonth();

        /**
         * Statistics for leads each month(For Charts).
         *
         */
        $completedLeadsMonthly = $this->leads->createdLeadsMonthly();
        $createdLeadsMonthly = $this->leads->completedLeadsMonthly();

        $departments = $this->departments->getAllDepartments();
        $taskcategories = $this->taskcategories->listAllTaskcategory();
        $roles = $this->roles->listAllRoles();
        $alluserEmployees = $this->users->getAllUsersEmployees();

        $totalMarketingLeads = $this->leads->totalMarketingLeads();
        $getAllMarketingLeads = $this->leads->getAllMarketingLeads();

        $getAllMarketingLeadArr = array();
        $i = 0;
        foreach ($getAllMarketingLeads as $getAllMarketingLead)
        {
            $getAllMarketingLeadArr[] .= $getAllMarketingLeads[$i]['id'];
            $i++;
        }

        $allCompletedMarketLeads = $this->leads->allCompletedMarketLeads();
        $allPendingMarketLeads = $this->leads->allPendingMarketLead();


        /**
         * Statistics for tasks this month.
         *
         */
        $taskCompletedThisMonth = $this->tasks->completedMarketTasksThisMonth($getAllMarketingLeadArr);

        $taskPendingThisMonth = $this->tasks->pendingTasksThisMonth();

        /**
         * Statistics for today tasks.
         *
         */
        $completedTasksToday =  $this->tasks->completedMarketTasksToday($getAllMarketingLeadArr);
        $createdTasksToday = $this->tasks->createdMarketTasksToday($getAllMarketingLeadArr);


        /**
         * Statistics for all-time tasks.
         *
         */

        $alltasks = $this->tasks->tasks();
        $allMarketTasks = $this->tasks->allMarketTasks($getAllMarketingLeadArr);
        $allCompletedTasks = $this->tasks->allCompletedTasks();
        $totalPercentageTasks = $this->tasks->percantageMarketCompleted($getAllMarketingLeadArr);

        $allCompletedMarketTasks = $this->tasks->allCompletedMarketTasks($getAllMarketingLeadArr);
        $allPendingMarketTasks = $this->tasks->allPendingMarketTasks($getAllMarketingLeadArr);
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $fileuploads = Projectuploads::where('taskcategory_id', "!=",0)->get();

        return view('marketing.dashboard', compact(
            'completedTasksToday',
            'completedLeadsToday',
            'taskPendingThisMonth',
            'createdTasksToday',
            'createdLeadsToday',
            'createdTasksMonthly',
            'completedTasksMonthly',
            'completedLeadsMonthly',
            'createdLeadsMonthly',
            'taskCompletedThisMonth',
            'leadCompletedThisMonth',
            'totalTimeSpent',
            'totalClients',
            'totalUsers',
            'users',
            'companyname',
            'alltasks',
            'allCompletedTasks',
            'totalPercentageTasks',
            'allleads',
            'allCompletedLeads',
            'totalPercentageLeads',
            'departments',
            'alluserManagers',
            'allLeads',
            'taskcategories',
            'roles',
            'alluserEmployees',
            'totalMarketingLeads',
            'allCompletedMarketLeads',
            'allPendingMarketLeads',
            'allCompletedMarketTasks',
            'allPendingMarketTasks',
            'allMarketTasks',
            'allUsersdepartments',
            'fileuploads'
        ));
    }


}
