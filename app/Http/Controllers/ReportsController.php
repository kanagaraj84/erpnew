<?php
namespace App\Http\Controllers;

use App\Accountexpenses;
use Gate;
use Illuminate\Support\Facades\DB;
use Carbon;
use Datatables;
use App\Models\Reports;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Reports\ReportsRepositoryContract;

class ReportsController extends Controller
{
    protected $departments;
    protected $settings;
    protected $users;
    protected $roles;
    protected $reports;

    public function __construct(
        UserRepositoryContract $users,
        DepartmentRepositoryContract $departments,
        RoleRepositoryContract $roles,
        SettingRepositoryContract $settings,
        ReportsRepositoryContract $reports
    )
    {
        $this->users = $users;
        $this->settings = $settings;
        $this->departments = $departments;
        $this->roles = $roles;
        $this->reports = $reports;
    }

    /**
     * @return mixed
     */
    public function index()
    {

    }

    public function reportsData()
    {
        $reports = Reports::all();
        return Datatables::of($reports)
            ->editColumn('title', function ($reports) {
                return $reports->title;
            })
            ->editColumn('description', function ($reports) {
                return $reports->description;
            })

            ->addColumn('total', function ($reports) {
                $countAccExpenses = DB::table('accountexpenses')->where('report_id','=',$reports->id)
                    ->groupBy('created_at')
                    ->distinct()->get();
                return count($countAccExpenses);
            })

            ->editColumn('user_id', function ($reports) {
                $userdetails = User::findOrFail($reports->user_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="/images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="/images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('users.users', $reports->user_id) . '">' . $imgpath.$userdetails->name. '</a>';
            })

            ->editColumn('created_at', function ($leads) {
                return $leads->created_at ? with(new Carbon($leads->created_at))
                    ->format('d/m/Y') : '';
            })->make(true);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function storedata(Request $request)
    {
        $getInsertedId = $this->reports->create($request);
        Session()->flash('flash_message', 'Report is created');
        return redirect()->route('account');
    }


}
