<?php
namespace App\Http\Controllers;

use App\Models\Lead;
use Gate;
use Carbon;
use Datatables;
use App\Models\Task;
use App\Models\Taskcategory;
use App\Models\Integration;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\User;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\Taskcategory\TaskcategoryRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;

class TasksController extends Controller
{
    protected $request;
    protected $tasks;
    protected $taskcategories;
    protected $settings;
    protected $users;
    protected $leads;
    protected $departments;
    protected $roles;

    public function __construct(
        TaskRepositoryContract $tasks,
        UserRepositoryContract $users,
        SettingRepositoryContract $settings,
        LeadRepositoryContract $leads,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        TaskcategoryRepositoryContract $taskcategories
    )
    {
        $this->tasks = $tasks;
        $this->users = $users;
        $this->settings = $settings;
        $this->leads = $leads;
        $this->departments = $departments;
        $this->taskcategories = $taskcategories;
        $this->roles = $roles;

        $this->middleware('task.create', ['only' => ['create']]);
        $this->middleware('task.update.status', ['only' => ['updateStatus']]);
        $this->middleware('task.assigned', ['only' => ['updateAssign', 'updateTime']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $status = 'all';
        if(isset($request->st)){
            $status = $request->st;
        }
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $taskcategories = $this->taskcategories->listAllTaskcategory();
        return view('tasks.index',  compact('departments', 'taskcategories', 'status','allUsersdepartments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function anyData(Request $request)
    {
        $getAllITLeads = $this->leads->getAllItLeads();
        $getAllITLeadArr = array();
        $i = 0;
        foreach ($getAllITLeads as $getAllITLead) {
            $getAllITLeadArr[] .= $getAllITLeads[$i]['id'];
            $i++;
        }

        $getAllMarketingLeads = $this->leads->getAllMarketingLeads();
        $getAllMarketingLeadArr = array();
        $i = 0;
        foreach ($getAllMarketingLeads as $getAllMarketingLead)
        {
            $getAllMarketingLeadArr[] .= $getAllMarketingLeads[$i]['id'];
            $i++;
        }

        if(isset($request->st) && $request->st=='pdg'){
            $tasks = Task::select(
                ['id', 'title', 'created_at', 'deadline', 'lead_id', 'user_assigned_id', 'status', 'priority'])
                ->where('status', "=",1)->get();
        } elseif($request->st=='cmp') {
            $tasks = Task::select(
                ['id', 'title', 'created_at', 'deadline', 'lead_id', 'user_assigned_id', 'status', 'priority'])
                ->where('status', "=",2)->get();
        } elseif($request->st=='art') {
            $tasks = Task::select(
                ['id', 'title', 'created_at', 'deadline', 'lead_id', 'user_assigned_id', 'status', 'priority'])
                ->where('taskcategory_id', "=",2)->get();
        } elseif($request->st=='sns') {
            $tasks = Task::select(
                ['id', 'title', 'created_at', 'deadline', 'lead_id', 'user_assigned_id', 'status', 'priority'])
                ->where('taskcategory_id', "=",1)->get();
        } elseif($request->st=='it') {
            $tasks = $this->tasks->allITTasksData($getAllITLeadArr);
        } elseif($request->st=='mark') {
            $tasks = $this->tasks->allMarketTasksData($getAllMarketingLeadArr);
        } elseif($request->st=='itcmp') {
            $tasks = $this->tasks->allCompletedITTasksData($getAllITLeadArr);
        } elseif($request->st=='itpdg') {
            $tasks = $this->tasks->allPendingITTasksData($getAllITLeadArr);
        } elseif($request->st=='markcmp') {
            $tasks = $this->tasks->allCompletedMarketTasksData($getAllMarketingLeadArr);
        } elseif($request->st=='markpdg') {
            $tasks = $this->tasks->allPendingMarketTasksData($getAllMarketingLeadArr);
        } else {
            $tasks = Task::select(
                ['id', 'title', 'created_at', 'deadline', 'lead_id', 'user_assigned_id', 'status', 'priority'])->get();
        }

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="tasks/' . $tasks->id . '" ">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('lead_id', function ($tasks) {
                return $tasks->lead->title;
            })

            ->editColumn('status', function ($tasks) {
                if($tasks->status==1){
                    return '<button type="button" class="btn btn-danger btn-sm btn-block">Pending</button>';
                } else {
                    return '<button type="button" class="btn btn-success btn-sm btn-block">Completed</button>';
                }
            })

            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>';
                } elseif($tasks->priority==2){
                    return '<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>';
                } elseif($tasks->priority==3) {
                    return '<button type="button" class="btn btn-danger btn-sm btn-block">High</button>';
                }
            })

            ->editColumn('user_assigned_id', function ($tasks) {
                return $tasks->user->name;
            })->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function memberTasksData(Request $request)
    {
        $tasks = $this->tasks->findMemberTasks($request->id);
        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="/tasks/' . $tasks->id . '" ">' . $tasks->title . '</a>';
            })

            ->editColumn('user_assigned_id', function ($tasks) {

                return '<a href="/tasks/' . $tasks->id . '" ">' . '<img class="small-profile-picture" src="../../'.$tasks->user->avatar.'" width="30" height="30" /> '.$tasks->user->name. '</a>';
            })

           ->editColumn('deadline', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="/tasks/' . $tasks->id . '" ">' . '<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="/tasks/' . $tasks->id . '" ">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="/tasks/' . $tasks->id . '" ">' . '<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="/tasks/' . $tasks->id . '" ">' . '<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="/tasks/' . $tasks->id . '" ">' . '<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('tasks.create')
            ->withUsers($this->users->getAllUsersEmployees())
            ->withTaskcategories($this->taskcategories->listAllTaskcategory())
            ->withLeads($this->leads->getAllLeads());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function edit($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $taskcategories = $this->taskcategories->listAllTaskcategory();
        $tasks = $this->tasks->find($id);
        return view('tasks.edit',compact('tasks', 'allUsersdepartments', 'departments', 'taskcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function update(Request $request, Task $tasks)
    {
        $tasks->update($request->all());
        return redirect(route('tasks.show',$tasks->id))->withMessage(__('Task Has Been Updated!'));
    }


    /**
     * @param StoreTaskRequest $request
     * @return mixed
     */
    public function store(StoreTaskRequest $request) // uses __contrust request
    {
        $getInsertedId = $this->tasks->create($request);

        if($request->hasfile('uploaded_files'))
        {
            foreach($request->file('uploaded_files') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/task_documents/'.$getInsertedId."/", $name);
            }
        }

        Session()->flash('flash_message', 'Task is created');
        return redirect()->route("dashboard", $getInsertedId);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function show($id)
    {
        $tasks = $this->tasks->find($id);
        $settings = $this->settings->getSetting();
        $permission = $this->roles->allPermissions();
        $roles = $this->roles->allRoles();
        $companyname = $this->settings->getCompanyName();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $users = $this->users->getAllUsersWithDepartments();
        $departments = $this->departments->getAllDepartments();
        $companyname = $this->settings->getCompanyName();
        return view('tasks.show', compact('allUsersdepartments','companyname','tasks','users','departments','companyname', 'settings','permission','roles'));
    }


    /**
     * Sees if the Settings from backend allows all to complete taks
     * or only assigned user. if only assigned user:
     * @param $id
     * @param Request $request
     * @return
     * @internal param $ [Auth]  $id Checks Logged in users id
     * @internal param $ [Model] $task->user_assigned_id Checks the id of the user assigned to the task
     * If Auth and user_id allow complete else redirect back if all allowed excute
     * else stmt
     */
    public function updateStatus($id, Request $request)
    {       
        $this->tasks->updateStatus($id, $request);
        Session()->flash('flash_message', 'Task is completed');
        return redirect()->back();
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function updateAssign($id, Request $request)
    {
        $this->tasks->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');
        return redirect()->back();
    }

    /**
     * Update the follow up date (Deadline)
     * @param UpdateLeadFollowUpRequest $request
     * @param $id
     * @return mixed
     */
    public function updateFollowup(Request $request, $id)
    {
        $this->tasks->updateFollowup($id, $request);
        Session()->flash('flash_message', 'Deadline and Priority has been updated');
        return redirect()->back();
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function updateTime($id, Request $request)
    {
        $this->tasks->updateTime($id, $request);
        Session()->flash('flash_message', 'Time has been updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @return mixed
     * @internal param int $id
     */
    public function marked()
    {
        Notifynder::readAll(\Auth::id());
        return redirect()->back();
    }
}
