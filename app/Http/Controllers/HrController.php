<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Support\Facades\DB;
use Carbon;
use Yajra\Datatables\Datatables;
use App\Models\User;
use App\Models\Task;
use App\Models\Department;
use App\Models\Userinformation;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use Illuminate\Support\Facades\Auth;
use App\Models\Companies;

class HrController extends Controller
{
    protected $users;
    protected $roles;
    protected $departments;
    protected $settings;
    protected $leads;
    protected $tasks;
    protected $companies;

    public function __construct(
        UserRepositoryContract $users,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads
    )
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->settings = $settings;
        $this->tasks = $tasks;
        $this->leads = $leads;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $users = User::all();
        $departments = $this->departments->getAllDepartments();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        return view('hr.index', compact('users', 'departments', 'allUsersdepartments'));
    }

    public function hrUsersData()
    {
        $users = $this->users->getAllUsers();
        return Datatables::of($users)
            ->addColumn('namelink', function ($users) {
                $userdetails = User::findOrFail($users->id);
                $companyname = $this->settings->getCompanyName();
                if ($userdetails->image_path != "") {
                    $imgpath = '<img class="small-profile-picture" src="images/' . $companyname . '/' . $userdetails->image_path . '" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture" src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="users/' . $users->id . '" >' . $imgpath . $users->name . '</a>';
            })

            ->editColumn('email', function ($user) {
                return $user->email;
            })
            ->editColumn('work_number', function ($user) {
                return $user->work_number;
            })
            ->editColumn('personal_number', function ($user) {
                return $user->personal_number;
            })
            ->editColumn('add_details', function ($user) {
                return '<a href="hr/' . $user->id . '/edit" class="btn btn-success btn-sm"> 
                        <i class="fa fa-plus-circle" style="font-size:24px;"></i> Add Information </a>';
            })->make(true);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function companyData(Request $request)
    {
        $allCompanyDetails = Companies::all();
        //echo "<pre>";print_r($allCompanyDetails); die;
        return Datatables::of($allCompanyDetails)
            ->addColumn('companyname', function ($allCompanyDetails) {
                return  $allCompanyDetails->companyname;
            })
            ->editColumn('address', function ($allCompanyDetails) {
                return  $allCompanyDetails->address;
            })

            ->editColumn('country', function ($allCompanyDetails) {
                return $allCompanyDetails->country;
            })
            ->editColumn('created_at', function ($allCompanyDetails) {
                return $allCompanyDetails->created_at ? with(new Carbon($allCompanyDetails->created_at))
                    ->format('d/m/Y h:i:s') : '';
            })
            ->editColumn('uploaded_files', function ($allCompanyDetails) {
                $fpath = url('/company_documents/'.$allCompanyDetails->id."/");
                $dir_path = public_path() . '/company_documents/'.$allCompanyDetails->id.'/';
                if (file_exists($dir_path)) {
                    $dir = new \DirectoryIterator($dir_path);
                    $i=1;
                    foreach ($dir as $fileinfo) {
                        if (!$fileinfo->isDot()) {
                            return "<br /><a href='" . $fpath . "/" . $fileinfo . "' class='btn btn-primary btn-sm btn-block' target='_blank'>View File</a> &nbsp; &nbsp;";
                        }
                        $i++;
                    }
                }else {
                    return "No Attachments";
                }
            })
            ->editColumn('edit', function ($allCompanyDetails) {
                return '<a href="' . route("company.edit", $allCompanyDetails->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })

            ->editColumn('delete', function ($allCompanyDetails) {
                return '<form action="hr/destroy/' . $allCompanyDetails->id . '" method="DELETE">
            <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm btn-block" onClick="return confirm(\'Are you sure want to delete?\')"">
            ' . csrf_field() . method_field('DELETE') . '</form>';
            })->make(true);
    }



    /**
     * @param $id
     * @return mixed
     */
    public function editData($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $userDetails = User::find($id);
        $userAdditionalData = Userinformation::where('user_id','=',$id)->get();
        $departments = $this->departments->getAllDepartments();
        return view('hr.edit', compact('userDetails', 'userAdditionalData','allUsersdepartments','departments','roles'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function companyEditData($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $userDetails = User::find($id);
        $companyData = Companies::find($id);
        $departments = $this->departments->getAllDepartments();
        return view('hr.companyedit', compact('userDetails', 'companyData','allUsersdepartments','departments','roles'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateData($id, Request $requestData)
    {
        $userData = Userinformation::where('user_id','=',$id)->get();
        $countUserData = count($userData);
        if($countUserData==0)
        {
            $created_at = date("Y-m-d h:i:s");
            $InsertedData = Userinformation::create([
                'join_date' => $requestData->join_date,
                'passport_no' => $requestData->passport_no,
                'passport_expiry_date' => $requestData->passport_expiry_date,
                'visa_expiry_date' => $requestData->visa_expiry_date,
                'nationality' => $requestData->nationality,
                'user_id' => $requestData->user_id,
                'dob' => $requestData->dob,
                'created_at' => $created_at,
                'updated_at' => $created_at
            ]);
            $insertedId = $InsertedData->id;
            Session()->flash('flash_message', 'User Details Added successfully');
            return redirect()->route('hr.index', $insertedId);
        } else {
            foreach ($userData as $data){
                $newid = isset($data->id) ? $data->id : '';
            }
            $requestData =
                ['join_date' => $requestData->join_date, 'passport_no' => $requestData->passport_no,
                    'passport_expiry_date' => $requestData->passport_expiry_date,
                    'visa_expiry_date' => $requestData->visa_expiry_date,
                    'nationality' => $requestData->nationality, 'dob' => $requestData->dob];

            DB::table('userinformation')->where('id', $newid)->limit(1)->update($requestData);
            Session()->flash('flash_message', 'User Details Updated successfully');
            return redirect()->route('hr.index', $newid);
        }

    }


    /**
     * @param $id
     * @return mixed
     */
    public function updateCompanyData($id, Request $requestData)
    {
        $companies = Companies::find($id);
        $requestData =
            ['companyname' => $requestData->companyname, 'address' => $requestData->address,
                'country' => $requestData->country, 'phone' => $requestData->phone, 'amount' => $requestData->amount];
        $companies->fill($requestData)->save();
        Session()->flash('flash_message', 'Company successfully updated');
        return redirect()->route('hr.index', $id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function companyStoredata(Request $request)
    {
        $created_at = date("Y-m-d h:i:s");
        $InsertedData = Companies::create([
            'companyname' => $request->companyname,
            'address' => $request->address,
            'country' => $request->country,
            'phone' => $request->phone,
            'created_at' => $created_at,
            'updated_at' => $created_at
        ]);
        $getInsertedId = $InsertedData->id;
        if ($request->hasfile('import_file')) {
            foreach ($request->file('import_file') as $files) {
                $name = $files->getClientOriginalName();
                $files->move(public_path() . '/company_documents/' .$getInsertedId. "/", $name);
            }
        }
        Session()->flash('flash_message', 'Company Inserted successfully');
        return redirect()->route('hr.index', $getInsertedId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function companydestroy($id)
    {
        Companies::destroy($id);
        Session()->flash('flash_message', 'Company has been deleted');
        return redirect()->route('hr.index', $id);
    }

}
