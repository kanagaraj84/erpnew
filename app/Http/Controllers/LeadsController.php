<?php
namespace App\Http\Controllers;

use App\Models\Department;
use DB;
use Auth;
use Carbon;
use Illuminate\Support\Facades\Session;
use Datatables;
use App\Models\Lead;
use App\Models\Officials;
use App\Models\Projectuploads;
use App\Http\Requests;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Lead\StoreLeadRequest;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Http\Requests\Lead\UpdateLeadFollowUpRequest;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Task\TaskRepositoryContract;

class LeadsController extends Controller
{
    protected $leads;
    protected $departments;
    protected $settings;
    protected $users;
    protected $roles;
    protected $tasks;

    public function __construct(
        LeadRepositoryContract $leads,
        TaskRepositoryContract $tasks,
        UserRepositoryContract $users,
        DepartmentRepositoryContract $departments,
        RoleRepositoryContract $roles,
        SettingRepositoryContract $settings
    )
    {
        $this->users = $users;
        $this->settings = $settings;
        $this->departments = $departments;
        $this->leads = $leads;
        $this->tasks = $tasks;
        $this->roles = $roles;
        $this->middleware('lead.create', ['only' => ['create']]);
        $this->middleware('lead.assigned', ['only' => ['updateAssign']]);
        $this->middleware('lead.update.status', ['only' => ['updateStatus']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = 'all';
        if(isset($request->st)){
            $status = $request->st;
        }
        $settings = $this->settings->getSetting();
        $permission = $this->roles->allPermissions();
        $roles = $this->roles->allRoles();
        $departments = $this->departments->getAllDepartments();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        return view('leads.index',  compact('departments', 'status', 'allUsersdepartments','settings','permission','roles'));
    }

    /**
     * Data for Data tables
     * @return mixed
     */
    public function anyData(Request $request)
    {
        if(isset($request->st) && $request->st=='pdg'){
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','    priority'])
                ->where('status', "=",1)->get();
        } elseif($request->st=='cmp') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('status', "=",2)->get();
        } elseif($request->st=='itcmp') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('status', "=",2)->where('dept_id', "=",3)->get();
        } elseif($request->st=='itpdg') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('status', "=",1)->where('dept_id', "=",3)->get();
        } elseif($request->st=='markcmp') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('status', "=",2)->where('dept_id', "=",2)->get();
        } elseif($request->st=='markpdg') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('status', "=",1)->where('dept_id', "=",2)->get();
        } elseif($request->st=='it') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('dept_id', "=",3)->get();
        } elseif($request->st=='mark') {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])
                ->where('dept_id', "=",2)->get();
        } else {
            $leads = Lead::select(
                ['id', 'title', 'user_created_id', 'dept_id', 'user_assigned_id', 'contact_date', 'status','priority'])->get();
        }

        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="leads/' . $leads->id . '" ">' . $leads->title . '</a>';
            })
            ->editColumn('user_created_id', function ($leads) {
                return $leads->creator->name;
            })
            ->editColumn('contact_date', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('dept_id', function ($leads) {
                $dept = Department::findOrFail($leads->dept_id);
                return $dept->name;
            })
            ->editColumn('status', function ($leads) {
                if($leads->status==1){
                    return '<button type="button" class="btn btn-danger btn-sm btn-block">Pending</button>';
                } else {
                    return '<button type="button" class="btn btn-success btn-sm btn-block">Completed</button>';
                }
            })

            ->editColumn('priority', function ($leads) {
                if($leads->priority==1){
                    return '<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>';
                } elseif($leads->priority==2){
                    return '<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>';
                } elseif($leads->priority==3) {
                    return '<button type="button" class="btn btn-danger btn-sm btn-block">High</button>';
                }
            })

            ->editColumn('user_assigned_id', function ($leads) {
                return $leads->user->name;
            })->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $users = $this->users->getAllUsersManagers();
        return view('leads.create', compact('users','departments', 'allUsersdepartments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLeadRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLeadRequest $request)
    {
        $getInsertedId = $this->leads->create($request);
        if($request->hasfile('uploaded_files'))
        {
            foreach($request->file('uploaded_files') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/project_documents/'.$getInsertedId."/", $name);
            }
        }
        Session()->flash('flash_message', 'Project is created');
        return redirect()->route('dashboard', $getInsertedId);
    }

    public function uploadform()
    {
        $fileuploads = Projectuploads::all();
        $users =  User::all();
        $projects = Lead::all();
        return view('it.dashboard')->with('fileuploads', $fileuploads)->with('users', $users)->with('allITLeads', $projects);
    }
    public function storeleadfile(Request $request)
    {
        $countFileupload = Projectuploads::where('project_id', "=",$request->lead_id)->count();
        if($countFileupload==0) {
            if ($request->hasfile('uploaded_files')) {
                foreach ($request->file('uploaded_files') as $files) {
                    $name = $files->getClientOriginalName();
                    $files->move(public_path() . '/completed_projects/' . $request->lead_id . "/", $name);
                }
            }
            Projectuploads::create([
                'project_name' => $request->title,
                'project_id' => $request->lead_id,
                'user_id' => $request->user_id,
                'status' => $request->status
            ]);
            Session()->flash('flash_message', 'Project Uploaded');
        } else {
            Session()->flash('flash_message', 'Project Cannot be Duplicated');
        }
        return redirect()->route('itdashboard') ;
    }

    public function storeMarketleadfile(Request $request)
    {
            if ($request->hasfile('uploaded_files')) {
                foreach ($request->file('uploaded_files') as $files) {
                    $name = $files->getClientOriginalName();
                    $files->move(public_path() . '/completed_market_projects/' .$request->taskcategory_id."_".$request->lead_id . "/", $name);
                }
            }
            Projectuploads::create([
                'project_name' => $request->title,
                'project_id' => $request->lead_id,
                'user_id' => $request->user_id,
                'taskcategory_id' => $request->taskcategory_id,
                'status' => $request->status
            ]);
            Session()->flash('flash_message', 'Document has been Uploaded');
        return redirect()->route('marketing') ;
    }

    public function storeITleadfile(Request $request)
    {
        if ($request->hasfile('uploaded_files')) {
            foreach ($request->file('uploaded_files') as $files) {
                $name = $files->getClientOriginalName();
                $files->move(public_path() . '/completed_it_projects/' .$request->taskcategory_id."_".$request->lead_id . "/", $name);
            }
        }
        Projectuploads::create([
            'project_name' => $request->title,
            'project_id' => $request->lead_id,
            'user_id' => $request->user_id,
            'taskcategory_id' => $request->taskcategory_id,
            'status' => $request->status
        ]);
        Session()->flash('flash_message', 'Document has been Uploaded');
        return redirect()->route('itdashboard') ;
    }



    public function storeAnnouncement(Request $request)
    {
        $getInsertedId = Officials::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $request->user_id
        ]);
        Session()->flash('flash_message', 'New Announcement is created');
        return redirect()->route('dashboard', $getInsertedId);
    }

    public function editAnnouncement($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $official = Officials::find($id);
        return view('announcement.edit',compact('official', 'allUsersdepartments', 'departments'));
    }

    public function updateAnnouncement($id, Request $request)
    {
        $offcial = Officials::findorFail($id);
        $offcial->update($request->all());
        return redirect('dashboard')->withMessage(__('Announcement Has Been Updated!'));
    }

    public function destroyAnnouncement($id)
    {
        $offcial = Officials::findorFail($id);
        $offcial->delete();
        return redirect('/')->withMessage(__('Announcement Has Been Deleted!'));
    }

    public function updateAssign($id, Request $request)
    {
        $this->leads->updateAssign($id, $request);
        Session()->flash('flash_message', 'New user is assigned');
        return redirect()->back();
    }

    /**
     * Update the follow up date (Deadline)
     * @param UpdateLeadFollowUpRequest $request
     * @param $id
     * @return mixed
     */
    public function updateFollowup(UpdateLeadFollowUpRequest $request, $id)
    {
        $this->leads->updateFollowup($id, $request);
        Session()->flash('flash_message', 'New follow up date is set');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $settings = $this->settings->getSetting();
        $permission = $this->roles->allPermissions();
        $roles = $this->roles->allRoles();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $lead = $this->leads->find($id);
        $memberTasks = $this->tasks->findMemberTasks($id);
        $users = $this->users->getAllUsersWithDepartments();
        $departments = $this->departments->getAllDepartments();
        $companyname = $this->settings->getCompanyName();
        return view('leads.show', compact('allUsersdepartments','memberTasks','lead','users','departments','companyname','settings','permission','roles'));
    }

    public function loadModal($id)
    {
        // write your process if any
        return view('leads.loadmodal',['data'=>$id]);
    }

    /**
     * Complete lead
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function updateStatus($id, Request $request)
    {
        $this->leads->updateStatus($id, $request);
        Session()->flash('flash_message', 'Project is completed');
        return redirect()->back();
    }
}
