<?php
namespace App\Http\Controllers;

use App\Accountexpenses;
use App\Models\ExpenseCategory;
use Gate;
use Illuminate\Support\Facades\DB;
use Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Reports;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\ExpenseCategory\ExpenseCatRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\User\UserRepositoryContract;

class ExpenseCategoryController extends Controller
{
    protected $expense;
    protected $departments;
    protected $users;

    public function __construct(
        ExpenseCatRepositoryContract $expense,
        UserRepositoryContract $users,
        DepartmentRepositoryContract $departments
    )
    {
        $this->expense = $expense;
        $this->users = $users;
        $this->departments = $departments;
    }

    /**
     * @return mixed
     */
    public function index()
    {

    }

    public function expenseData()
    {
        $expense = ExpenseCategory::all();

        return Datatables::of($expense)
            ->editColumn('title', function ($expense) {
                return $expense->title;
            })
            ->editColumn('description', function ($expense) {
                return $expense->description;
            })
            ->editColumn('created_at', function ($expense) {
                return $expense->created_at ? with(new Carbon($expense->created_at))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('edit', function ($expense) {
                return '<a href="' . route("expensecat.edit", $expense->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })

            ->editColumn('delete', function ($expense) {
                return '<form action="expensecat/destroy/' . $expense->id . '" method="DELETE">
            <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm btn-block" onClick="return confirm(\'Are you sure want to delete?\')"">
            ' . csrf_field() . method_field('DELETE') . '</form>';
            })

            ->editColumn('total_expense', function ($expense) {
                $totalexpense = Accountexpenses::where('category_id','=',$expense->id)
                    ->sum('amount');
                return "AED ".number_format($totalexpense, 2);
            })->make(true);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function storedata(Request $request)
    {
        $getInsertedId = $this->expense->create($request);
        Session()->flash('flash_message', 'Report is created');
        return redirect()->route('account', $getInsertedId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        ExpenseCategory::destroy($id);
        Session()->flash('flash_message', 'Category has been deleted');
        return redirect()->route('account.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function editData($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $expenseCategories = ExpenseCategory::find($id);
        $departments = $this->departments->getAllDepartments();
        return view('expensecat.edit', compact('expenseCategories','allUsersdepartments','departments','roles'));
    }


    /**
     * @param $id
     * @return mixed
     */
    public function updateData($id, Request $requestData)
    {
        $accExpenses = ExpenseCategory::find($id);
        $requestData =
            ['title' => $requestData->title, 'description' => $requestData->description];
        $accExpenses->fill($requestData)->save();
        Session()->flash('flash_message', 'Category successfully updated');
        return redirect()->route('account.index');
    }


}
