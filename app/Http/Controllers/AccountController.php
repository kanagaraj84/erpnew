<?php
namespace App\Http\Controllers;
use App\Models\Department;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon;
use Yajra\Datatables\Datatables;
use App\Models\Reports;
use App\Models\User;
use App\Models\ExpenseCategory;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Accountexpenses;
use App\Repositories\Reports\ReportsRepositoryContract;
use App\Repositories\ExpenseCategory\ExpenseCatRepositoryContract;
use Maatwebsite\Excel\Excel;

class AccountController extends Controller
{
    protected $departments;
    protected $users;
    protected $reports;
    public function __construct(
        UserRepositoryContract $users,
        DepartmentRepositoryContract $departments,
        ReportsRepositoryContract $reports,
        ExpenseCatRepositoryContract $reportcategories
    )
    {
        $this->users = $users;
        $this->departments = $departments;
        $this->reports = $reports;
        $this->reportcategories = $reportcategories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departments = $this->departments->getAllDepartments();
        $reports = $this->reports->getAllReports();
        $expenseCategories = $this->reportcategories->getAllExpenseCat();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();

        if(isset($request->datesearch) && $request->datesearch!='') {
            $dates = $request->datesearch;
            $dateArr = explode(' - ', $dates);

            $startDate = isset($dateArr) ? $dateArr[0] : '';
            $endDate = isset($dateArr) ? $dateArr[1] : '';
            $explDate = explode('/', $startDate);
            $csDate = $explDate[2] . "-" . $explDate[0] . "-" . $explDate[1];

            $endexplDate = explode('/', $endDate);
            $ceDate = $endexplDate[2] . "-" . $endexplDate[0] . "-" . $endexplDate[1];
            $startDateVal = $csDate;
            $endDateVal = $ceDate;
        } else{
            $startDateVal = '';
            $endDateVal = '';
        }
        return view('account.index',compact('departments','allUsersdepartments', 'reports',
            'expenseCategories', 'startDateVal', 'endDateVal'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function accountdata(Request $request)
    {
        if($request->sval!="" && $request->eval!="") {
            $allAccountDetails = DB::table('accountexpenses')
                ->select(DB::raw('id, title, branch_id, category_id, report_duration, created_at, amount'))
                ->whereBetween('created_at', [$request->sval, $request->eval])
                ->groupBy('created_at')
                ->distinct()->get();
        } else {
            $allAccountDetails = DB::table('accountexpenses')
                ->select(DB::raw('id, title, branch_id, category_id, report_duration, created_at, amount'))
                ->groupBy('created_at')
                ->distinct()->get();
        }
        //echo "<pre>";print_r($allAccountDetails); die;
        return Datatables::of($allAccountDetails)
            ->addColumn('titlelink', function ($allAccountDetails) {
                return  $allAccountDetails->title;
            })
            ->editColumn('branch_id', function ($allAccountDetails) {
                if($allAccountDetails->branch_id==1){
                    return "ABBC Dubai";
                } else if($allAccountDetails->branch_id==2){
                    return "Belarus";
                } else if($allAccountDetails->branch_id==3){
                    return "Manara";
                } else if($allAccountDetails->branch_id==4){
                    return "All";
                }
            })

            ->editColumn('category_id', function ($allAccountDetails) {
                $expenseCategory = ExpenseCategory::findOrFail($allAccountDetails->category_id);
                return $expenseCategory->title;
            })

            ->editColumn('report_duration', function ($allAccountDetails) {
                return $allAccountDetails->report_duration;
            })
            ->editColumn('created_at', function ($allAccountDetails) {
                return $allAccountDetails->created_at ? with(new Carbon($allAccountDetails->created_at))
                    ->format('d/m/Y h:i:s') : '';
            })
            ->editColumn('uploaded_files', function ($allAccountDetails) {
                    $fpath = url('/expense_documents/'.$allAccountDetails->id."/");
                    $dir_path = public_path() . '/expense_documents/'.$allAccountDetails->id.'/';
                if (file_exists($dir_path)) {
                    $dir = new \DirectoryIterator($dir_path);
                    $i=1;
                    foreach ($dir as $fileinfo) {
                       if (!$fileinfo->isDot()) {
                            return "<br /><a href='" . $fpath . "/" . $fileinfo . "' class='btn btn-primary btn-sm btn-block' target='_blank'>View File</a> &nbsp; &nbsp;";
                        }
                        $i++;
                    }
                }else {
                    return "No Attachments";
                }
            })
            ->editColumn('amount', function ($allAccountDetails) {
                return "AED ".number_format($allAccountDetails->amount, 2);
            })

            ->editColumn('edit', function ($allAccountDetails) {
                return '<a href="' . route("account.edit", $allAccountDetails->id) . '" class="btn btn-success btn-sm btn-block"> Edit</a>';
            })

            ->editColumn('delete', function ($allAccountDetails) {
                    return '<form action="account/destroy/' . $allAccountDetails->id . '" method="DELETE">
            <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm btn-block" onClick="return confirm(\'Are you sure want to delete?\')"">
            ' . csrf_field() . method_field('DELETE') . '</form>';
            })->make(true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function accountDetails($date)
    {
        $revcreated_at = date("Y-m-d h:i:s", $date);
        $allAccountDetails = DB::table('accountexpenses')
            ->where('created_at','=',$revcreated_at)->get();
        $departments = $this->departments->getAllDepartments();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();

        foreach ($allAccountDetails as $allAccountDetail){
            $reports = Reports::find($allAccountDetail->report_id);
        }
        return view('account.details',  compact('departments', 'allUsersdepartments', 'allAccountDetails', 'reports'));
    }



    /**
     * @param $id
     * @return mixed
     */
    public function editData($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $accExpenses = Accountexpenses::find($id);
        $departments = $this->departments->getAllDepartments();
        return view('account.edit', compact('accExpenses','allUsersdepartments','departments','roles'));
    }


    /**
     * @param $id
     * @return mixed
     */
    public function updateData($id, Request $requestData)
    {
        $accExpenses = Accountexpenses::find($id);
        $requestData =
            ['title' => $requestData->title, 'description' => $requestData->description,
                'branch_id' => $requestData->branch_id, 'category_id' => $requestData->category_id, 'amount' => $requestData->amount];
        $accExpenses->fill($requestData)->save();
        Session()->flash('flash_message', 'Expense successfully updated');
        return redirect()->route('account.index');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function totalData(Request $request)
    {
        if($request->sval!="" && $request->eval!="") {
            $totalexpense = DB::table('accountexpenses')
                ->select(DB::raw('sum(amount) as total, created_at, amount'))
                ->whereBetween('created_at', [$request->sval, $request->eval])->get();
            $countVal = count($totalexpense);
            if($countVal==0){
                $totalexpense = 0;
            } else {
                foreach ($totalexpense as $totalexpense){
                    $totalexpense = $totalexpense->total;
                }
            }
        } else {
            $totalexpense = Accountexpenses::sum('amount');
        }
        return "<p style='float:right; color:#fff'>Summary of Overall Expense<br /> 
                AED ".number_format($totalexpense, 2)." </p>";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cateTotalData(Request $request)
    {
        $cateid = $request->cateid;
        if($cateid=='all'){
            $totalexpense = Accountexpenses::sum('amount');
        } else {
            $totalexpense = Accountexpenses::where('category_id','=',$cateid)->sum('amount');
        }
        return "<p style='float:right; color:#fff'>Summary of Overall Expense<br /> 
                AED ".number_format($totalexpense, 2)." </p>";
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function branchTotalData(Request $request)
    {
        $branchid = $request->branchid;
        if($branchid=='all'){
            $totalexpense = Accountexpenses::sum('amount');
        } else {
            $totalexpense = Accountexpenses::where('branch_id','=',$branchid)->sum('amount');
        }
        return "<p style='float:right; color:#fff'>Summary of Overall Expense<br /> 
                AED ".number_format($totalexpense, 2)." </p>";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storedata(Request $request)
    {
        $created_at = date("Y-m-d h:i:s");
        $InsertedData = Accountexpenses::create([
            'title' => $request->title,
            'branch_id' => $request->branch_id,
            'category_id' => $request->category_id,
            'description' => $request->description,
            'amount' => $request->amount,
            'report_duration' => $request->report_duration,
            'created_at' => $created_at,
            'updated_at' => $created_at
        ]);
        $getInsertedId = $InsertedData->id;
        if ($request->hasfile('import_file')) {
            foreach ($request->file('import_file') as $files) {
                $name = $files->getClientOriginalName();
                $files->move(public_path() . '/expense_documents/' .$getInsertedId. "/", $name);
            }
        }
        Session()->flash('flash_message', 'Record Inserted successfully');
        return redirect()->route('account', $getInsertedId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        Accountexpenses::destroy($id);
        Session()->flash('flash_message', 'Expense has been deleted');
        return redirect()->route('account.index');
    }

}
