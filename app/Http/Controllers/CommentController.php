<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\Task;
use App\Models\Lead;
use App\Http\Requests;
use App\Models\Comment;
use Illuminate\Http\Request;

use App\Notifications\CommentNotification;
use App\Models\Topic;

use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * store
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {   
        $this->validate($request, [
            'description' => 'required'
        ]);

        $source = $request->type == "task" ? Task::find($request->id) : Lead::find($request->id); 
        $comment = $source->addComment(['description' => $request->description, 'user_id' => auth()->user()->id]);
        event(new \App\Events\NewComment($comment));
        Session::flash('flash_message', 'Comment successfully added!'); //Snippet in Master.blade.php
        return redirect()->back();
    }

    /**
     * storeComment
     * @param Request $request
     * @param $comment
     * @return mixed
     */
	public function storeComment(Request $request,Topic $topic)
    {
        $validator = Validator::make($request->all(), [
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('commentcreateerror','Comment is Required');
            return redirect(route('topic.show',$topic->id.'#lf_comment_create_form'));
        }
        $comment = new Comment();
        $comment->body = $request->body;
        $comment->user_id = Auth::user()->id;
        $comment = $topic->comments()->save($comment);
        if($request->hasfile('uploaded_files'))
        {
            foreach($request->file('uploaded_files') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/comment_documents/'.$comment->id."/", $name);
            }
        }
        return redirect(route('topic.show', $topic->id))->withMessage(__('Comment Has Been Replied Successfully!'));
    }

    /**
     * storeReply
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function storeReply(Request $request,Comment $comment)
    {
        $validator = Validator::make($request->all(), [
            'replybody' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('replybody'.$comment->id,'Reply is Required');
            return redirect(route('topic.show',$comment->commentable_id.'#commentno'.$comment->id));
        }
        $reply = new Comment();
        $reply->body = $request->replybody;
        $reply->user_id = Auth::user()->id;
        $comment->comments()->save($reply);
        return redirect(route('topic.show',$request->topic_id.'#commentno'.$comment->id))->withMessage(__('Reply added Successfully!'));
    }

    /**
     * update
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function update(Request $request, Comment $comment)
    {
        $validator = Validator::make($request->all(), [
            'editcommentbody' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('editcommentbody'.$comment->id,'Comment is Required');
            return redirect(route('topic.show',$request->topic_id.'#commentno'.$comment->id));
        }
        $comment->update([
            'body' => $request->editcommentbody,
        ]);
        return redirect(route('topic.show',$request->topic_id.'#commentno'.$comment->id))->withMessage(__('Comment Has Been Updated Successfully!'));
    }

    /**
     * replyUpdate
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function replyUpdate(Request $request,Comment $comment)
    {
        $validator = Validator::make($request->all(), [
            'editreplybody' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('editreplybody'.$request->topic_id,'Reply is Required');
            return redirect(route('topic.show',$request->topic_id.'#commentno'.$request->comment_id));
        }
        $comment->update([
            'body' => $request->editreplybody,
        ]);
        return redirect(route('topic.show',$request->topic_id.'#commentno'.$request->comment_id))->withMessage(__('Reply Has Been Updated Successfully!'));
    }

    /**
     * replyUpdate
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function replyDestroy(Request $request,Comment $comment)
    {
        $comment->delete();
        Session::flash('commentmessage', "Reply Deleted");
        return redirect(route('topic.show',$request->topic_id.'#commentno'.$request->comment_id));
    }

    /**
     * destroy
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function destroy(Request $request,Comment $comment)
    {
        $comments = Comment::where('commentable_id',$comment->id)->get();
        foreach ($comments as $reply){
            Comment::where('id',$reply->id)->delete();
        }
        $comment->delete();
        Session::flash('commentmessage', "Comment Deleted");
        return redirect(route('topic.show',$request->topic_id.'#lf_comments_wrap'));
    }

    /**
     * solveComment
     * @param Request $request
     * @param $comment
     * @return mixed
     */
    public function solveComment(Request $request, Comment $comment)
    {
        $comment->update($request->all());
        return redirect(route('topic.show',$request->topic_id.'#commentno'.$request->comment_id))->withMessage(__('Email Issue Has Been Fixed'));
    }

}
