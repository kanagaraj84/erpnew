<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Topic;
use Yajra\Datatables\Datatables;
use Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Department;
use Illuminate\Support\Facades\Input;
use App\Models\ParentCat;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;

class TopicController extends Controller
{
    protected $users;
    protected $departments;
    protected $parentcat;

    public function __construct(
        UserRepositoryContract $users,
        DepartmentRepositoryContract $departments,
        DepartmentRepositoryContract $parentcat

    )
    {
        $this->users = $users;
        $this->departments = $departments;
        $this->parentcat = $parentcat;
    }


    /**
     * @return mixed
     */
    public function index()
    {
        $departments = $this->departments->getAllDepartments();
        $parentcat = $this->parentcat->getAllParrentCat();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $topicview = Topic::select(DB::raw('topics.created_at,topics.slug,topics.id,topics.title, count(*) as aggregate'))
            ->join('page-views', 'topics.id', '=', 'page-views.visitable_id')
            ->groupBy('topics.title','topics.id','topics.slug','topics.created_at')
            ->orderBy('aggregate', 'desc')
            ->take(10)
            ->get();
        $usertopics = Topic::where('user_id',Auth::id())->take(10)->get();
        $allUsertopics = Topic::all();
        $sureDelete = __('Are you sure want to Delete?');
        $topics = Topic::orderBy('id', 'desc')->paginate(10);
        return view('alltopics',compact('topics','sureDelete', 'allUsertopics', 'parentcat', 'departments', 'usertopics','topicview', 'allUsersdepartments'));
    }


    public function anyData()
    {
        $topics = Topic::select(['id', 'title', 'details', 'user_id','created_at','updated_at']);

        return Datatables::of($topics)
            ->addColumn('titlelink', function ($topics) {
                return '<a href="topic/show/' . $topics->id . '" ">' . $topics->title . '</a>';
            })

            ->editColumn('created_at', function ($topics) {
                return $topics->created_at ? with(new Carbon($topics->created_at))
                    ->format('d/m/Y') : '';
            })

            ->editColumn('user_id', function ($topics) {
                return User::find($topics->user_id)->name;
            })

            ->editColumn('updated_at', function ($topics) {
                return $topics->updated_at ? with(new Carbon($topics->updated_at))
                    ->format('d/m/Y') : '';
            })

            ->addColumn('edit', function ($topics) {
                return '<a href="' . route("topic.edit", $topics->id    ) . '" class="btn btn-success"> Edit</a>';
            })
            ->add_column('delete', function ($topics) {
                return '<button type="button" class="btn btn-danger delete_topic" data-topic_id="' . $topics->id . '" onClick="openModal(' . $topics->id. ')" id="myBtn">Delete</button>';
            })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topicview = Topic::select(DB::raw('topics.created_at,topics.slug,topics.id,topics.title, count(*) as aggregate'))
            ->join('page-views', 'topics.id', '=', 'page-views.visitable_id')
            ->groupBy('topics.title','topics.id','topics.slug','topics.created_at')
            ->orderBy('aggregate', 'desc')
            ->take(10)
            ->get();
        $usertopics = Topic::where('user_id',Auth::id())->take(10)->get();

        return view('topics.create',compact('topic','sureDelete','topicsCount','comments','liked_user','usertopics','topicview','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
            'title' => 'required|unique:topics|min:10',
            'details' => 'required|min:20',
            ],
            ['title.unique' => 'This Topic Is Already Posted.']);

        $topic = Topic::create($request->all());

        if($request->hasfile('uploaded_files'))
        {
            foreach($request->file('uploaded_files') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/topic_documents/'.$topic->id."/", $name);
            }
        }

        return redirect(route('topic.index'))->withMessage(__('Email Issue Has Been Created!'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storecat(Request $request)
    {
        $getInsertedId = ParentCat::create($request->all());
        Session()->flash('flash_message', 'New Announcement is created');
        return redirect()->route('topic.index', $getInsertedId);
    }

    public function show($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $parentcat = $this->parentcat->getAllParrentCat();
        $allUsertopics = Topic::all();
        $topicview = Topic::select(DB::raw('topics.created_at,topics.slug,topics.id,topics.title, count(*) as aggregate'))
            ->join('page-views', 'topics.id', '=', 'page-views.visitable_id')
            ->groupBy('topics.title','topics.id','topics.slug','topics.created_at')
            ->orderBy('aggregate', 'desc')
            ->take(10)
            ->get();
        $usertopics = Topic::where('user_id',Auth::id())->take(10)->get();
        $sureDelete = __('Are you sure want to Delete?');
        $sureSolved = __('Are you sure that has been Solved?');

        $topic = Topic::find($id);
        $comments = Comment::where('commentable_id',$topic->id)->orderBy('id', 'asc')->paginate(10);
        $topicsCount = Topic::where('user_id', $topic->user_id)->get();
        return view('topics.show',compact('topic','parentcat', 'allUsertopics','sureDelete','sureSolved','topicsCount','comments','departments','usertopics','topicview','allUsersdepartments'));
    }

    public function edit($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $departments = $this->departments->getAllDepartments();
        $topicview = Topic::select(DB::raw('topics.created_at,topics.slug,topics.id,topics.title, count(*) as aggregate'))
            ->join('page-views', 'topics.id', '=', 'page-views.visitable_id')
            ->groupBy('topics.title','topics.id','topics.slug','topics.created_at')
            ->orderBy('aggregate', 'desc')
            ->take(10)
            ->get();

        $usertopics = Topic::where('user_id',Auth::id())->take(10)->get();
        $topic = Topic::find($id);
        return view('topics.edit',compact('topic','usertopics','topicview', 'allUsersdepartments', 'departments'));
    }

    public function update(Request $request, Topic $topic)
    {
        $this->validate($request,
            [
                'title' => 'required|min:10|unique:topics,title,'.$topic->id,
                'details' => 'required|min:20'
            ],
            ['title.unique' => 'This Topic Is Already Posted.']
        );
        $topic->update($request->all());
        return redirect(route('topic.show',$topic->id))->withMessage(__('Email issue Has Been Updated!'));
    }

    public function destroy(Topic $topic)
    {
        $comments = Comment::where('commentable_id',$topic->id)->get();
        foreach($comments as $comment){
            Comment::where('commentable_id',$comment->id)->delete();
        }
        Comment::where('commentable_id',$topic->id)->delete();
        $topic->delete();
        return redirect('/topic')->withMessage(__('Email issue Has Been Deleted!'));
    }

    public function makeAsRead()
    {
        Auth::user()->unreadNotifications->markAsRead();
    }


}
