<?php

namespace App\Http\Controllers;

use App\Models\Projectuploads;
use DB;
use Carbon;
use Illuminate\Support\Facades\Session;
use Datatables;
use App\Models\User;
use App\Models\Task;
use App\Models\Department;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Lead;
use App\Models\Taskcategory;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Client\ClientRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Taskcategory\TaskcategoryRepositoryContract;

class ItController extends Controller
{
    protected $users;
    protected $clients;
    protected $settings;
    protected $tasks;
    protected $leads;
    protected $roles;
    protected $departments;
    protected $taskcategories;

    public function __construct(
        UserRepositoryContract $users,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        ClientRepositoryContract $clients,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads,
        TaskcategoryRepositoryContract $taskcategories
    )
    {
        $this->users = $users;
        $this->clients = $clients;
        $this->settings = $settings;
        $this->tasks = $tasks;
        $this->leads = $leads;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->taskcategories = $taskcategories;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function itdashboard()
    {
        /**
         * Other Statistics
         *
         */
        $fileuploads = Projectuploads::where('taskcategory_id', "=",0)->get();
        $companyname = $this->settings->getCompanyName();
        $users = $this->users->getAllUsers();
        $totalClients = $this->clients->getAllClientsCount();
        $totalUsers = $this->users->getAllUsersCount();
        $totalTimeSpent = $this->tasks->totalTimeSpent();

        $alluserManagers = $this->users->getAllUsersManagers();
        $allITLeads = $this->leads->getAllItLeads();
        $allitCompletedLeads = $this->leads->allCompletedITLeads();
        $allPendingITLeads = $this->leads->allPendingITLead();
        /**
         * Statistics for all-time tasks.
         *
         */


        $allCompletedTasks = $this->tasks->allCompletedTasks();


        /**
         * Statistics for today tasks.
         *
         */


        /**
         * Statistics for tasks this month.
         *
         */

        /**
         * Statistics for tasks each month(For Charts).
         *
         */
        $createdTasksMonthly = $this->tasks->createdTasksMothly();
        $completedTasksMonthly = $this->tasks->completedTasksMothly();

        /**
         * Statistics for all-time Leads.
         *
         */

        $totalallITLeads = $this->leads->itleads();
        $allCompletedLeads = $this->leads->allCompletedITLeads();
        $totalPercentageLeads = $this->leads->percantageITCompleted();
        /**
         * Statistics for today leads.
         *
         */
        $completedLeadsToday = $this->leads->completedLeadsItToday();
        $createdLeadsToday = $this->leads->createdLeadsITToday();
        $allLeads = $this->leads->getAllItLeads();

        /**
         * Statistics for leads this month.
         *
         */
        $leadCompletedThisMonth = $this->leads->completedITLeadsThisMonth();

        /**
         * Statistics for leads each month(For Charts).
         *
         */
        $completedLeadsMonthly = $this->leads->createdLeadsMonthly();
        $createdLeadsMonthly = $this->leads->completedLeadsMonthly();

        $departments = $this->departments->getAllDepartments();
        $taskcategories = $this->taskcategories->listAllTaskcategory();
        $roles = $this->roles->listAllRoles();
        $alluserEmployees = $this->users->getAllUsersEmployees();

        $getAllITLeads = $this->leads->getAllItLeads();

        $getAllITLeadArr = array();
        $i = 0;
        foreach ($getAllITLeads as $getAllITLead) {
            $getAllITLeadArr[] .= $getAllITLeads[$i]['id'];
            $i++;
        }

        $totalPercentageTasks = $this->tasks->percantageITCompleted($getAllITLeadArr);

        $alltasks = $this->tasks->allITTasks($getAllITLeadArr);

        $taskCompletedThisMonth = $this->tasks->completedITTasksThisMonth($getAllITLeadArr);
        $taskPendingThisMonth = $this->tasks->pendingITTasksThisMonth($getAllITLeadArr);
        $completedTasksToday = $this->tasks->completedITTasksToday($getAllITLeadArr);
        $createdTasksToday = $this->tasks->createdITTasksToday($getAllITLeadArr);
        $allCompletedITTasks = $this->tasks->allCompletedITTasks($getAllITLeadArr);
        $allPendingITTasks = $this->tasks->allPendingITTasks($getAllITLeadArr);
        $allPendingITLeads = $this->leads->allPendingITLead();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();

        return view('it.dashboard', compact(
            'completedTasksToday',
            'completedLeadsToday',
            'taskPendingThisMonth',
            'createdTasksToday',
            'createdLeadsToday',
            'createdTasksMonthly',
            'completedTasksMonthly',
            'completedLeadsMonthly',
            'createdLeadsMonthly',
            'taskCompletedThisMonth',
            'leadCompletedThisMonth',
            'totalTimeSpent',
            'totalClients',
            'totalUsers',
            'users',
            'allLeads',
            'companyname',
            'alltasks',
            'allCompletedTasks',
            'totalPercentageTasks',
            'totalallITLeads',
            'allCompletedLeads',
            'allPendingITLeads',
            'totalPercentageLeads',
            'departments',
            'alluserManagers',
            'allITLeads',
            'taskcategories',
            'roles',
            'fileuploads',
            'alluserEmployees',

            'alliteleadscount',
            'allCompletedITTasks',
            'allPendingITTasks',
            'allPendingITLeads',
            'allitCompletedLeads',
            'allUsersdepartments'
        ));
    }

}
