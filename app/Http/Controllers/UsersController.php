<?php
namespace App\Http\Controllers;

use Gate;
use Illuminate\Support\Facades\DB;
use Carbon;
use Yajra\Datatables\Datatables;
use App\Models\User;
use App\Models\Task;
use App\Models\Department;
use App\Models\LeaveRequest;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Lead;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Repositories\User\UserRepositoryContract;
use App\Repositories\Role\RoleRepositoryContract;
use App\Repositories\Department\DepartmentRepositoryContract;
use App\Repositories\Setting\SettingRepositoryContract;
use App\Repositories\Task\TaskRepositoryContract;
use App\Repositories\Lead\LeadRepositoryContract;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    protected $users;
    protected $roles;
    protected $departments;
    protected $settings;
    protected $leads;
    protected $tasks;

    public function __construct(
        UserRepositoryContract $users,
        RoleRepositoryContract $roles,
        DepartmentRepositoryContract $departments,
        SettingRepositoryContract $settings,
        TaskRepositoryContract $tasks,
        LeadRepositoryContract $leads
    )
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->departments = $departments;
        $this->settings = $settings;
        $this->tasks = $tasks;
        $this->leads = $leads;
        $this->middleware('user.create', ['only' => ['create']]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $users = User::all();
        $departments = $this->departments->getAllDepartments();
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        return view('users.index',compact('users','departments','allUsersdepartments'));
    }

    public function users()
    {
        return User::all();
    }

    public function anyData()
    {
        $users = $this->users->getAllUsers();
        return Datatables::of($users)
            ->editColumn('namelink', function ($users) {
                $userdetails = User::findOrFail($users->id);
                $companyname = $this->settings->getCompanyName();
                if ($userdetails->image_path != "") {
                    $imgpath = '<img class="small-profile-picture" src="images/' . $companyname . '/' . $userdetails->image_path . '" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture" src="images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="users/' . $users->id . '" ">' .$imgpath.$users->name . '</a>';
            })
            ->editColumn('edit', function ($user) {
                return '<a href="' . route("users.edit", $user->id) . '" class="btn btn-success btn-sm"> Edit</a>';
            })
            ->editColumn('delete', function ($user) {
                return '<form action="users/destroy/' . $user->id . '" method="DELETE">
            <input type="submit" name="submit" value="Delete" class="btn btn-danger btn-sm" onClick="return confirm(\'Are you sure want to delete?\')"">
            ' . csrf_field() .method_field('DELETE'). '
            </form>'; })->make(true);
    }

    /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function taskData($id)
    {
        $depts =DB::table('department_user')->where('user_id', $id)->get();
        if(count($depts)>0) {
            foreach ($depts as $dept) {
                $department_id = $dept->department_id;
            }
        } else{
            $department_id = 0;
        }
        $leads = Lead::where('dept_id', $department_id)->get();
        $leadArr = array();
        foreach ($leads as $lead){
            $leadArr[] = $lead->id;
        }
        $leadArr = array_unique($leadArr);
        if(count($leadArr)>0){
            $user = User::findOrFail($id);
            if ($user->hasRole('manager')) {
                $tasks = Task::whereIn('lead_id', $leadArr)->get();
            } elseif ($user->hasRole('employee')) {
                $tasks = Task::whereIn('lead_id', $leadArr)->where('user_assigned_id', $id)->get();
            }

        } else {
            $tasks = Task::select(
                ['id', 'title', 'user_assigned_id', 'lead_id', 'created_at', 'deadline', 'status', 'priority']
            )->where('user_assigned_id', $id)->get();
        }

        return Datatables::of($tasks)
            ->addColumn('titlelink', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' . $tasks->title . '</a>';
            })
            ->editColumn('created_at', function ($tasks) {
                return $tasks->created_at ? with(new Carbon($tasks->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('deadline', function ($tasks) {
                return $tasks->deadline ? with(new Carbon($tasks->deadline))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('lead_id', function ($tasks) {
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$tasks->lead->title. '</a>';
            })
            ->editColumn('user_assigned_id', function ($tasks) {
                $user = User::findOrFail($tasks->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($user->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="/images/'.$companyname.'/'.$user->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="/images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('tasks.show', $tasks->id) . '">' .$imgpath.$user->name. '</a>';
            })
            ->editColumn('priority', function ($tasks) {
                if($tasks->priority==1){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($tasks->priority==2){
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($tasks->priority==3) {
                    return '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })
            ->editColumn('status', function ($tasks) {
                return $tasks->status == 1 ? '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('tasks.show', $tasks->id) . '">' .'<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })->make(true);
    }

        /**
     * Json for Data tables
     * @param $id
     * @return mixed
     */
    public function leadData($id)
    {
        $tasks = Task::where('user_assigned_id', $id)->get();
        $leadArr = array();
        foreach ($tasks as $task){
            $leadArr[] = $task->lead_id;
        }
        $leadArr = array_unique($leadArr);
        if(count($leadArr)>0){
            $leads = Lead::whereIn('id', $leadArr)->get();
        } else{
            $leads = Lead::select(
                ['id', 'title', 'user_assigned_id', 'dept_id', 'created_at', 'contact_date', 'status','priority']
            )->where('user_assigned_id', $id);
        }

        return Datatables::of($leads)
            ->addColumn('titlelink', function ($leads) {
                return '<a href="' . route('leads.show', $leads->id) . '">' . $leads->title . '</a>';
            })
            ->editColumn('created_at', function ($leads) {
                return $leads->created_at ? with(new Carbon($leads->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('contact_date', function ($leads) {
                return $leads->contact_date ? with(new Carbon($leads->contact_date))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('user_assigned_id', function ($leads) {
                $userdetails = User::findOrFail($leads->user_assigned_id);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="/images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="/images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('leads.show', $leads->id) . '">' . $imgpath.$userdetails->name. '</a>';
            })

            ->editColumn('dept_id', function ($leads) {
                $deptdata = Department::findOrFail($leads->dept_id);
                return '<a href="' . route('leads.show', $leads->id) . '">' . $deptdata->name. '</a>';
            })

            ->editColumn('status', function ($leads) {
                return $leads->status == 1 ? '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-success btn-sm btn-block">Open</button>'. '</a>' : '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-danger btn-sm btn-block">Closed</button>'. '</a>';
            })

            ->editColumn('priority', function ($leads) {
                if($leads->priority==1){
                    return '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-primary btn-sm btn-block">Low</button>'. '</a>';
                } elseif($leads->priority==2){
                    return '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-success btn-sm btn-block">Medium</button>'. '</a>';
                } elseif($leads->priority==3) {
                    return '<a href="' . route('leads.show', $leads->id) . '">' . '<button type="button" class="btn btn-danger btn-sm btn-block">High</button>'. '</a>';
                }
            })
            ->make(true);
    }


    /**
     * @return mixed
     */
    public function create()
    {
        return view('users.create')
            ->withRoles($this->roles->listAllRoles())
            ->withDepartments($this->departments->getAllDepartments());
    }

    /**
     * @param StoreUserRequest $userRequest
     * @return mixed
     */
    public function store(StoreUserRequest $userRequest)
    {
        $getInsertedId = $this->users->create($userRequest);
        Session()->flash('flash_message', 'User is created');
        return redirect()->route('dashboard', $getInsertedId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $user = $this->users->find($id);
        $departments = $this->departments->getAllDepartments();
        $companyname = $this->settings->getCompanyName();
        $taskstatistics = $this->tasks->totalOpenAndClosedTasks($id);
        $leadStatistics = $this->leads->totalOpenAndClosedLeads($id);
        return view('users.show', compact('allUsersdepartments','user','departments','companyname','taskstatistics','leadStatistics'));

    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $allUsersdepartments = $this->users->getAllUsersWithDepartmentsData();
        $user = $this->users->find($id);
        $departments = $this->departments->getAllDepartments();
        $roles = $this->roles->listAllRoles();
        return view('users.edit', compact('user','allUsersdepartments','departments','roles'));
    }

    /**
     * @param $id
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update($id, UpdateUserRequest $request)
    {
        $this->users->update($id, $request);
        Session()->flash('flash_message', 'User successfully updated');
        return redirect()->back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $this->users->destroy($id);
        Session()->flash('flash_message', 'User successfully deleted');
        return redirect()->route('users.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function leaveApprove($id)
    {
        $requestData = ['status' => 1];
        LeaveRequest::where('id', $id)->limit(1)->update($requestData);
        Session()->flash('flash_message', 'Leave successfully approved');
        return redirect()->route('users.users',$id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function leaveData($id)
    {
        $leaveRequests = LeaveRequest::where('applicantid', $id)->get();
        $countLeaveRequest = count($leaveRequests);
        if($countLeaveRequest==0){
            $leaveRequests = LeaveRequest::where('applicantid', $id);
        }
        return Datatables::of($leaveRequests)
            ->addColumn('namelink', function ($leaveRequests) {
                $userdetails = User::findOrFail($leaveRequests->applicantid);
                $companyname = $this->settings->getCompanyName();
                if($userdetails->image_path != ""){
                    $imgpath = '<img class="small-profile-picture"  src="/images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                } else {
                    $imgpath = '<img class="small-profile-picture"  src="/images/default_avatar.jpg" width="30" height="30"> ';
                }
                return '<a href="' . route('users.users', $userdetails->id) . '">' . $imgpath.$userdetails->name. '</a>';
            })
            ->editColumn('type', function ($leaveRequests) {
                return $leaveRequests->type;
            })
            ->editColumn('shortdescription', function ($leaveRequests) {
                return $leaveRequests->shortdescription;
            })
            ->editColumn('duration', function ($leaveRequests) {
                return $leaveRequests->duration;
            })
            ->editColumn('created_at', function ($leaveRequests) {
                return $leaveRequests->created_at ? with(new Carbon($leaveRequests->created_at))
                    ->format('d/m/Y') : '';
            })
            ->editColumn('status', function ($leaveRequests) {
                return $leaveRequests->status == 1 ? '<button type="button" class="btn btn-success btn-sm btn-block">Approved</button>': '<button type="button" class="btn btn-danger btn-sm btn-block">Waiting</button>';
            })->make(true);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function allleavedata()
    {
        $leaveRequests = LeaveRequest::all();
        return Datatables::of($leaveRequests)
                ->addColumn('namelink', function ($leaveRequests) {
                    $userdetails = User::findOrFail($leaveRequests->applicantid);
                    $companyname = $this->settings->getCompanyName();
                    if($userdetails->image_path != ""){
                        $imgpath = '<img class="small-profile-picture"  src="/images/'.$companyname.'/'.$userdetails->image_path.'" width="30" height="30"> ';
                    } else {
                        $imgpath = '<img class="small-profile-picture"  src="/images/default_avatar.jpg" width="30" height="30"> ';
                    }
                    return '<a href="' . route('users.users', $userdetails->id) . '">' . $imgpath.$userdetails->name. '</a>';
                })
                ->editColumn('type', function ($leaveRequests) {
                    return $leaveRequests->type;
                })
                ->editColumn('shortdescription', function ($leaveRequests) {
                    return $leaveRequests->shortdescription;
                })
                ->editColumn('duration', function ($leaveRequests) {
                    return $leaveRequests->duration;
                })
                ->editColumn('created_at', function ($leaveRequests) {
                    return $leaveRequests->created_at ? with(new Carbon($leaveRequests->created_at))
                        ->format('d/m/Y') : '';
                })
                ->addColumn('approve', function ($leaveRequests) {
                    $userdetails = User::findOrFail($leaveRequests->applicantid);
                    if($leaveRequests->status==0){
                        return '<form action="/users/approve/' . $leaveRequests->id . '" method="POST">
                                <input type="submit" name="submit" value="Approve" class="btn btn-danger btn-sm" onClick="return confirm(\'Are you sure want to Approve?\')">
                                ' . csrf_field(). '
                                </form>';
                    } else if($leaveRequests->status==1){
                        return '<button type="button" class="btn btn-success btn-sm btn-block">Approved</button>';
                    }
                })->make(true);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function storeleavrequest(Request $request)
    {
        $created_at = date("Y-m-d h:i:s");
        $InsertedData = LeaveRequest::create([
            'applicantid' => $request->applicantid,
            'type' => $request->type,
            'shortdescription' => $request->shortdescription,
            'duration' => $request->duration,
            'created_at' => $created_at,
            'updated_at' => $created_at
        ]);
        $getInsertedId = $InsertedData->applicantid;
        if ($request->hasfile('uploaded_files')) {
            foreach ($request->file('uploaded_files') as $files) {
                $name = $files->getClientOriginalName();
                $files->move(public_path() . '/leave_request_documents/' .$getInsertedId. "/", $name);
            }
        }
        Session()->flash('flash_message', 'Leave Applied Successfully');
        return redirect()->route('users.users', $getInsertedId);
    }
}
