<?php

namespace App\Http\Controllers;

use Gate;
use Carbon;
use Datatables;
use Session;
use App\Models\Taskcategory;
use Illuminate\Http\Request;
use App\Repositories\Taskcategory\TaskcategoryRepositoryContract;


class TaskcategoryController extends Controller
{
    protected $request;
    protected $tasks;
    protected $clients;
    protected $settings;
    protected $users;
    protected $invoices;
    protected $leads;
    protected $departments;
    protected $taskcategories;


    /**
     * DepartmentsController constructor.
     * @param TaskcategoryRepositoryContract $taskcategories
     */
    public function __construct(TaskcategoryRepositoryContract $taskcategories)
    {
        $this->taskcategories = $taskcategories;
        $this->middleware('user.is.admin', ['only' => ['taskcategory.create', 'taskcategory.destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('taskcategory.index')
            ->withTaskcategory($this->taskcategories->getAllTaskcategory());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return mixed
     */
    public function create()
    {
        return view('taskcategory.create');
    }

    /**
     * @param StoreTaskRequest $request
     * @return mixed
     */
    public function store(Request $request) // uses __contrust request
    {
        $getInsertedId = $this->taskcategories->create($request);
        Session()->flash('flash_message', 'Task category is created');
        return redirect()->route("dashboard", $getInsertedId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $this->taskcategories->destroy($id);
        return redirect()->route('taskcategory.index');
    }


}
