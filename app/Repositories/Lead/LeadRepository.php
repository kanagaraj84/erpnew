<?php
namespace App\Repositories\Lead;

use App\Models\Lead;
use Notifynder;
use Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class LeadRepository
 * @package App\Repositories\Lead
 */
class LeadRepository implements LeadRepositoryContract
{
    /**
     *
     */
    const CREATED = 'created';
    /**
     *
     */
    const UPDATED_STATUS = 'updated_status';
    /**
     *
     */
    const UPDATED_DEADLINE = 'updated_deadline';
    /**
     *
     */
    const UPDATED_ASSIGN = 'updated_assign';


    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllLeads()
    {
        return Lead::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Lead::findOrFail($id);
    }

    /**
     * @param $requestData
     * @return mixed
     */
    public function create($requestData)
    {
        $dept_id = $requestData->get('dept_id');
        $input = $requestData = array_merge(
            $requestData->  all(),
            ['user_created_id' => \Auth::id(),
                'contact_date' => $requestData->contact_date . " " . $requestData->contact_time . ":00",
                'priority' => $requestData->priority]
        );

        $lead = Lead::create($input);
        $insertedId = $lead->id;
        Session()->flash('flash_message', 'Lead successfully added!');

        event(new \App\Events\LeadAction($lead, self::CREATED));

        return $insertedId;
    }

    /**
     * @param $id
     * @param $requestData
     */
    public function updateStatus($id, $requestData)
    {
        $lead = Lead::findOrFail($id);

        $input = $requestData->get('status');
        $input = array_replace($requestData->all(), ['status' => 2]);
        $lead->fill($input)->save();
        event(new \App\Events\LeadAction($lead, self::UPDATED_STATUS));
    }

    /**
     * @param $id
     * @param $requestData
     */
    public function updateFollowup($id, $requestData)
    {
        $lead = Lead::findOrFail($id);
        $input = $requestData->all();
        $input = $requestData =
            ['contact_date' => $requestData->contact_date . " " . $requestData->contact_time . ":00",
                'priority' => $requestData->priority];
        $lead->fill($input)->save();
        event(new \App\Events\LeadAction($lead, self::UPDATED_DEADLINE));
    }

    /**
     * @param $id
     * @param $requestData
     */
    public function updateAssign($id, $requestData)
    {
        $lead = Lead::findOrFail($id);
        $input = $requestData->get('user_assigned_id');
        $input = array_replace($requestData->all());
        $lead->fill($input)->save();
        $insertedName = $lead->user->name;

        event(new \App\Events\LeadAction($lead, self::UPDATED_ASSIGN));
    }

    /**
     * @return int
     */
    public function leads()
    {
        return Lead::all()->count();
    }

    /**
     * @return mixed
     */
    public function itleads()
    {
        return Lead::where('dept_id', "=",3)->count();
    }

    /**
     * @return mixed
     */
    public function getAllItLeads()
    {
        return Lead::where('dept_id', "=",3)->get();
    }

    /**
     * @return mixed
     */
    public function getAllItLeadscount()
    {
        return Lead::where('dept_id', "=",3)->count();
    }

    /**
     * @return mixed
     */
    public function allCompletedITLeads()
    {
        return Lead::where('status', 2)
            ->where('dept_id', "=",3)
            ->count();
    }

    /**
     * @return mixed
     */
    public function allPendingITLead()
    {
        return Lead::where('status', 1)
            ->where('dept_id', "=",3)
            ->count();
    }

    /**
     * @return mixed
     */
    public function percantageITCompleted()
    {
        if (!$this->itleads() || !$this->allCompletedITLeads()) {
            $totalPercentageITLeads = 0;
        } else {
            $totalPercentageITLeads = $this->allCompletedITLeads() / $this->getAllItLeadscount() * 100;
        }

        return $totalPercentageITLeads;
    }

    /**
     * @return mixed
     */
    public function completedLeadsItToday()
    {
        return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('dept_id', "=",3)
            ->where('status', 2)->count();
    }

    /**
     * @return mixed
     */
    public function createdLeadsITToday()
    {
        return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('dept_id', "=",3)->count();
    }


    /**
     * @return mixed
     */
    public function totalMarketingLeads()
    {
        return Lead::where('dept_id', "=",2)->count();
    }

    /**
     * @return mixed
     */
    public function getAllMarketingLeads()
    {
        return Lead::where('dept_id', "=",2)->get();
    }


    /**
     * @return mixed
     */
    public function allCompletedMarketLeads()
    {
        return Lead::where('dept_id', "=",2)
            ->where('status', "=",2)->count();
    }

    /**
     * @return mixed
     */
    public function allPendingMarketLead()
    {
        return Lead::where('dept_id', "=",2)
            ->where('status', "=",1)->count();
    }

    /**
     * @return mixed
     */
    public function allCompletedLeads()
    {
        return Lead::where('status', "=",2)->count();
    }

    /**
     * @return mixed
     */
    public function allPendingLeads()
    {
        return Lead::where('status', "=",1)->count();
    }

    /**
     * @return float|int
     */
    public function percantageCompleted()
    {
        if (!$this->leads() || !$this->allCompletedLeads()) {
            $totalPercentageLeads = 0;
        } else {
            $totalPercentageLeads = $this->allCompletedLeads() / $this->leads() * 100;
        }

        return $totalPercentageLeads;
    }

    /**
     * @return float|int
     */
    public function percantageMarketCompleted()
    {
        if (!$this->totalMarketingLeads() || !$this->allCompletedMarketLeads()) {
            $totalPercentageLeads = 0;
        } else {
            $totalPercentageLeads = $this->allCompletedMarketLeads() / $this->totalMarketingLeads() * 100;
        }

        return $totalPercentageLeads;
    }

    /**
     * @return mixed
     */
    public function completedLeadsToday()
    {
        return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('status', 2)->count();
    }

    /**
     * @return mixed
     */
    public function completedLeadsMarketToday()
    {
        return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('dept_id', "=",2)
            ->where('status', 2)->count();
    }

    /**
     * @return mixed
     */
    public function pendingLeadsToday()
    {
        return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('status', 1)->count();
    }



    /**
     * @return mixed
     */
    public function createdLeadsToday()
    {
         return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->count();
    }


    /**
     * @return mixed
     */
    public function createdLeadsMarketToday()
    {
        return Lead::whereRaw(
            'date(updated_at) = ?',
            [Carbon::now()->format('Y-m-d')]
        )->where('dept_id', "=",2)->count();
    }

    /**
     * @return mixed
     */
    public function completedLeadsThisMonth()
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }


    /**
     * @return mixed
     */
    public function completedLeadsMarketThisMonth()
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->where('dept_id', 2)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return mixed
     */
    public function completedITLeadsThisMonth()
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->where('dept_id', 3)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }




    /**
    * @return mixed
    */
    public function pendingLeadsThisMonth()
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 1)
            ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return mixed
     */
    public function createdLeadsMonthly()
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as month, updated_at'))
            ->where('status', 1)
          ->whereBetween('updated_at', [Carbon::now()->startOfMonth(), Carbon::now()])->get();
    }

    /**
     * @return mixed
     */
    public function completedLeadsMonthly()
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as month, created_at'))
            ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
            ->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function totalOpenAndClosedLeads($id)
    {
        $open_leads = Lead::where('status', 1)
        ->where('user_assigned_id', $id)
        ->count();

        $closed_leads = Lead::where('status', 2)
        ->where('user_assigned_id', $id)->count();

        return collect([$closed_leads, $open_leads]);
    }

    /**
     * @return mixed
     */
    public function leadCompletedBetweenDates($sDate, $eDate)
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 2)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();
    }

    /**
     * @return mixed
     */
    public function leadPendingBetweenDates($sDate, $eDate)
    {
        return DB::table('leads')
            ->select(DB::raw('count(*) as total, updated_at'))
            ->where('status', 1)
            ->whereBetween('updated_at', [$sDate, $eDate])->get();
    }

}
