<?php
namespace App\Repositories\Taskcategory;

use App\Models\Taskcategory;

/**
 * Class DepartmentRepository
 * @package App\Repositories\Department
 */
class TaskcategoryRepository implements TaskcategoryRepositoryContract
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllTaskcategory()
    {
        return Taskcategory::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return Taskcategory::findOrFail($id);
    }

    /**
     * @return mixed
     */
    public function listAllTaskcategory()
    {
        return Taskcategory::pluck('name', 'id');
    }

    /**
     * @param $requestData
     */
    public function create($requestData)
    {
        $taskcategory = Taskcategory::create($requestData->all());
        $insertedId = $taskcategory->id;
        return $insertedId;
    }

    /**
     * @param $id
     */
    public function destroy($id)
    {
        Taskcategory::findorFail($id)->delete();
    }
}
