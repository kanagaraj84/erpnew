<?php
namespace App\Repositories\Taskcategory;

interface TaskcategoryRepositoryContract
{
    public function find($id);

    public function getAllTaskcategory();
    
    public function listAllTaskcategory();

    public function create($requestData);

    public function destroy($id);
}
