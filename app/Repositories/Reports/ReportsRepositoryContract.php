<?php
namespace App\Repositories\Reports;

interface ReportsRepositoryContract
{
    public function getAllReports();

    public function find($id);

    public function create($requestData);

    public function destroy($id);
}
