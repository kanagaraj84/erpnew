<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    protected $fillable =
        [
            'id',
            'companyname',
            'address',
            'country',
            'phone',
            'created_at',
            'updated_at'
        ];
}
