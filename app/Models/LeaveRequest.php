<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leaverequest';


    protected $fillable = [
        'applicantname','applicantid','duration','type','status','shortdescription','created_at',
        'updated_at'
    ];
}
