<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Officials extends Model
{
    protected $fillable =
        [
            'id',
            'title',
            'description',
            'user_id',
            'created_at',
            'updated_at'
        ];
}
