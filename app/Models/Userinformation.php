<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userinformation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'userinformation';

    protected $fillable =
        [
            'id',
            'user_id',
            'join_date',
            'passport_no',
            'passport_expiry_date',
            'visa_expiry_date',
            'nationality',
            'dob',
            'created_at',
            'updated_at'
        ];
}
