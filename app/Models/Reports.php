<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $fillable =
        [
            'title',
            'description',
            'user_id',
            'created_at',
            'updated_at'
        ];
}
