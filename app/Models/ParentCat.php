<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParentCat extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parentcat';


    protected $fillable = [
        'id','title','description','created_at','updated_at'
    ];
}
