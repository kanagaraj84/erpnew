<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{
    protected $fillable =
        [
            'id',
            'title',
            'description',
            'created_at',
            'updated_at'
        ];
}
