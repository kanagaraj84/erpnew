<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{

    protected $fillable = [
        'title','details','user_id', 'parent_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get all of the Topic's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }


    /**
     * Get all of the Like's.
     */
    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'likeable');
    }

}
