<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accountexpenses extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accountexpenses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title','category_id','description','branch_id','amount',
        'report_duration', 'created_at','updated_at'];
}
