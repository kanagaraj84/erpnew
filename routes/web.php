<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::auth();
Route::get('/logout', 'Auth\LoginController@logout');
Route::group(['middleware' => ['auth']], function () {
    /**
     * Main
     */

        Route::get('/alltopics','TopicController@index')->name('topic.index');
        Route::resource('/topic','TopicController',['except' => ['show']]);
        Route::resource('/comment','CommentController',['except' => ['show']]);
        Route::post('/topic/create','TopicController@create')->name('topic.create');
        Route::post('/topic/{topic}/edit','TopicController@edit')->name('topic.edit');
        Route::post('/topic/{topic}/edit','TopicController@update')->name('topic.update');
        Route::DELETE('/topic/destroy/{topic}','TopicController@destroy')->name('topic.destroy');
        Route::PATCH('/topic/comment/solve/{comment}','CommentController@solveComment')->name('comment.solve');
        Route::get('/topicdata', 'TopicController@anyData')->name('topic.data');

        Route::post('/topic/comment/create/{topic}','CommentController@storeComment')->name('topic.comment.create');
        Route::get('/tag/{tag}/topics','TopicController@sortByTags')->name('tag.topics');
        Route::post('/topic/comment/reply/create/{comment}','CommentController@storeReply')->name('topic.reply.create');
        Route::PATCH('/comment/reply/update/{comment}','CommentController@replyUpdate')->name('reply.update');
        Route::DELETE('/comment/reply/delete/{comment}','CommentController@replyDestroy')->name('reply.delete');
        Route::post('/topic/comment/bestanswer/{topic}','TopicController@bestAnswer')->name('bestAnswer');
        Route::post('/notification/makeAsRead/','TopicController@makeAsRead')->name('makeAsRead');
        Route::post('/topic/likeTopic','LikeController@likeTopic')->name('likeTopic');
        Route::get('/user/{user}','UserProfileController@show')->name('user.show');

        Route::get('/topic/show/{topic}','TopicController@show')->name('topic.show');
        Route::post('/topic/storecreate','TopicController@storecat')->name('topic.storecat');
        Route::get('login/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.login');
        Route::get('login/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.login.callback');

        Route::get('/', 'PagesController@dashboard'); 
        Route::get('dashboard', 'PagesController@dashboard')->name('dashboard');
        Route::POST('dashboard', 'PagesController@dashboard')->name('dashboard');
        Route::get('/pagetaskdata', 'PagesController@taskData')->name('pages.taskdata');
        Route::get('/pageleaddata', 'PagesController@leadData')->name('pages.leaddata');
        Route::get('/pageuserdata', 'PagesController@userData')->name('pages.userdata');
        Route::get('/pagetaskcategorydata', 'PagesController@pagetaskcategorydata')->name('pages.taskcategorydata');
        Route::get('/pagesnstaskdata', 'PagesController@snstaskData')->name('pages.snstaskdata');
        Route::get('/pagearticletaskdata', 'PagesController@articletaskData')->name('pages.articletaskdata');
        Route::get('/pageemailtaskdata', 'PagesController@emailtaskData')->name('pages.emailtaskdata');

        Route::get('marketing', 'MarketingController@dashboard')->name('marketing');
        Route::get('/pagemarketleaddata', 'PagesController@marketleadData')->name('pages.marketingleaddata');
        Route::get('/pagemarkettaskdata', 'PagesController@markettaskData')->name('pages.marketingtaskdata');
        Route::get('/repoMarketdata', 'PagesController@repoMarketdata')->name('pages.repoMarketdata');
        Route::get('/repoitdata', 'PagesController@repoITdata')->name('pages.repoITdata');


        Route::get('itdashboard', 'ItController@itdashboard')->name('itdashboard');
        Route::get('/pageitleaddata', 'PagesController@itleadData')->name('pages.itleaddata');
        Route::get('/pageittaskdata', 'PagesController@ittaskData')->name('pages.ittaskData');
        Route::post('/officialdata', 'PagesController@officialdata')->name('pages.officialdata');
        Route::get('/officialdata', 'PagesController@officialdata')->name('pages.officialdata');

    Route::group(['prefix' => 'leads'], function () {
        Route::get('/data', 'LeadsController@anyData')->name('leads.data');
        Route::patch('/updateassign/{id}', 'LeadsController@updateAssign');
        Route::patch('/updatestatus/{id}', 'LeadsController@updateStatus');
        Route::patch('/updatefollowup/{id}', 'LeadsController@updateFollowup')->name('leads.followup');
    });

    /**
     * Users
     */
    Route::group(['prefix' => 'users'], function () {
        Route::get('/data', 'UsersController@anyData')->name('users.data');
        Route::get('/taskdata/{id}', 'UsersController@taskData')->name('users.taskdata');
        Route::get('/leaddata/{id}', 'UsersController@leadData')->name('users.leaddata');
        Route::get('/destroy/{id}','UsersController@destroy')->name('users.destroy');
        Route::get('/leavedata/{id}', 'UsersController@leaveData')->name('users.leavedata');
        Route::get('/allleavedata', 'UsersController@allleavedata')->name('users.allleavedata');
        Route::post('/applyleave', 'UsersController@storeleavrequest')->name('leave.store');
        Route::post('/approve/{id}','UsersController@leaveApprove')->name('users.leaveapprove');
    });
        Route::resource('users', 'UsersController');
        Route::get('users/{id}', 'UsersController@users')->name('users.users');

	 /**
     * Roles
     */
        Route::resource('roles', 'RolesController');
    /**
     * Tasks
     */
    Route::group(['prefix' => 'tasks'], function () {
        Route::get('/data', 'TasksController@anyData')->name('tasks.data');
        Route::get('/membertasksdata/{id}', 'TasksController@memberTasksData')->name('membertasks.data');
        Route::patch('/updatestatus/{id}', 'TasksController@updateStatus');
        Route::patch('/updateassign/{id}', 'TasksController@updateAssign');
        Route::post('/updatetime/{id}', 'TasksController@updateTime');
        Route::patch('/updatefollowup/{id}', 'TasksController@updateFollowup')->name('tasks.followup');
    });
        Route::resource('tasks', 'TasksController');

    Route::post('/tasks/{tasks}/edit','TasksController@edit')->name('tasks.edit');
    Route::post('/tasks/{tasks}/edit','TasksController@update')->name('tasks.update');
    /**
     * Task category
     */
    Route::resource('taskcategory', 'TaskcategoryController');
    /**
     * Task category
     */
    Route::group(['prefix' => 'taskcategory'], function () {
        Route::get('/', 'TaskcategoryController@index');
        Route::match(['get', 'post'], 'create', 'TaskcategoryController@create');
    });

    Route::post('/projects/fupload', 'LeadsController@storeleadfile')->name('project.fupload');
    Route::post('/projects/fmarketupload', 'LeadsController@storeMarketleadfile')->name('project.fmarketupload');
    Route::post('/projects/fitupload', 'LeadsController@storeITleadfile')->name('project.fitupload');
    Route::get('/projects/uploadform', 'LeadsController@uploadform')->name('project.uploadform');


    /**
     * Leads
     */
    Route::group(['prefix' => 'leads'], function () {
        Route::get('/data', 'LeadsController@anyData')->name('leads.data');
        Route::patch('/updateassign/{id}', 'LeadsController@updateAssign');
        Route::patch('/updatestatus/{id}', 'LeadsController@updateStatus');
        Route::patch('/updatefollowup/{id}', 'LeadsController@updateFollowup')->name('leads.followup');
    });
        Route::resource('leads', 'LeadsController');
        Route::post('/comments/{type}/{id}', 'CommentController@store');
    /**
     * Settings
     */
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'SettingsController@index')->name('settings.index');
        Route::patch('/permissionsUpdate', 'SettingsController@permissionsUpdate');
        Route::patch('/overall', 'SettingsController@updateOverall');
    });

    /**
     * Departments
     */
        Route::resource('departments', 'DepartmentsController');

    /**
     * Integrations
     */
    Route::group(['prefix' => 'integrations'], function () {
        Route::get('Integration/slack', 'IntegrationsController@slack');
    });
        Route::resource('integrations', 'IntegrationsController');

    /**
     * Notifications
     */
    Route::group(['prefix' => 'notifications'], function () {
        Route::post('/markread', 'NotificationsController@markRead')->name('notification.read');
        Route::get('/markall', 'NotificationsController@markAll');
        Route::get('/{id}', 'NotificationsController@markRead');
    });

    /**
     * Announcements
     */
    Route::post('/announcement/upload', 'LeadsController@storeAnnouncement')->name('announcement.upload');
    Route::get('/announcement/{announcement}/edit','LeadsController@editAnnouncement')->name('announcement.edit');
    Route::post('/announcement/{announcement}/edit','LeadsController@editAnnouncement')->name('announcement.edit');
    Route::patch('/announcement/{announcement}/update','LeadsController@updateAnnouncement')->name('announcement.update');
    Route::post('/announcement/{announcement}/update','LeadsController@updateAnnouncement')->name('announcement.update');
    Route::DELETE('/announcement/destroy/{announcement}','LeadsController@destroyAnnouncement')->name('announcement.destroy');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/**
 * Accounts
 */
Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'AccountController@index')->name('account.index');
    Route::post('/storedata', 'AccountController@storedata')->name('account.store');
    Route::get('/accountdata', 'AccountController@accountdata')->name('account.data');
    Route::get('/accountdetails/{date}', 'AccountController@accountDetails')->name('account.details');
    Route::post('/accountdetails/{date}', 'AccountController@accountDetails')->name('account.details');
    Route::get('/{id}/edit','AccountController@editData')->name('account.edit');
    Route::get('/update/{id}','AccountController@updateData');
    Route::post('/update/{id}','AccountController@updateData');
    Route::get('/destroy/{id}','AccountController@destroy')->name('account.destroy');
    Route::DELETE('/destroy/{id}','AccountController@destroy')->name('account.destroy');
});

Route::resource('account', 'AccountController');
Route::get('account', 'AccountController@index')->name('account');
Route::POST('account', 'AccountController@index')->name('account.index');

Route::get('/totaldata', 'AccountController@totalData')->name('expense.totaldata');
Route::get('/catetotaldata', 'AccountController@cateTotalData')->name('expense.catetotaldata');
Route::get('/branchtotaldata', 'AccountController@branchTotalData')->name('expense.branchtotaldata');

/**
 * Reports
 */
/*Route::group(['prefix' => 'reports'], function () {
    Route::get('/storedata', 'ReportsController@storedata')->name('reports.store');
    Route::post('/storedata', 'ReportsController@storedata')->name('reports.store');
    Route::get('/reportsdata', 'ReportsController@reportsData')->name('reports.data');
});*/

/**
 * Expense Category
 */
Route::group(['prefix' => 'expensecat'], function () {
    Route::get('/storedata', 'ExpenseCategoryController@storedata')->name('expensecat.store');
    Route::post('/storedata', 'ExpenseCategoryController@storedata')->name('expensecat.store');
    Route::get('/expensecatdata', 'ExpenseCategoryController@expenseData')->name('expensecat.data');
    Route::get('/{id}/edit','ExpenseCategoryController@editData')->name('expensecat.edit');
    Route::get('/destroy/{id}','ExpenseCategoryController@destroy')->name('expensecat.destroy');
    Route::DELETE('/destroy/{id}','ExpenseCategoryController@destroy')->name('expensecat.destroy');
    Route::post('/update/{id}','ExpenseCategoryController@updateData')->name('expensecat.update');
});

/**
 * HR
 */
Route::group(['prefix' => 'hr'], function () {
    Route::get('/', 'HrController@index')->name('hr.index');
    Route::get('/hrusersdata', 'HrController@hrUsersData')->name('hrusers.data');
    Route::get('/{id}/edit','HrController@editData')->name('hrusers.edit');
    Route::get('/update/{id}','HrController@updateData')->name('hrusers.update');
    Route::post('/update/{id}','HrController@updateData')->name('hrusers.update');
    Route::post('/companystoredata', 'HrController@companyStoredata')->name('company.store');
    Route::get('/companydata', 'HrController@companyData')->name('company.data');
    Route::get('/destroy/{id}','HrController@companydestroy')->name('company.destroy');
    Route::DELETE('/destroy/{id}','HrController@companydestroy')->name('company.destroy');
    Route::get('/{id}/companyedit','HrController@companyEditData')->name('company.edit');
    Route::get('/companyupdate/{id}','HrController@updateCompanyData')->name('hrusers.update');
    Route::post('/companyupdate/{id}','HrController@updateCompanyData')->name('hrusers.update');
});