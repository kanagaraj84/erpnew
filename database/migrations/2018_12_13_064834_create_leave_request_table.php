<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaverequest', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('applicantid')->nullable();
            $table->string('type')->nullable();
            $table->string('duration')->nullable();
            $table->integer('status')->default(0);
            $table->string('shortdescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaverequest');
    }
}
