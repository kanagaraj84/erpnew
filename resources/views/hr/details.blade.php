<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="/assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="">

<div class="wrapper">
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')
        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    <?php
    $i=0;
    foreach($allAccountDetails as $allAccountDetail){
        if($i==0){
            $title = $reports->title;
            $branch_id = $allAccountDetail->branch_id;
            $report_id = $allAccountDetail->report_id;
            $created_at = $allAccountDetail->created_at;
            $report_duration = $allAccountDetail->report_duration;
            $openbalance = $allAccountDetail->openbalance;
            $total_indirect_expense = $allAccountDetail->total_indirect_expense;
        }
        $i++;
    } ?>

        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <h3>ACCOUNT DETAILS</h3>
                <div class="col-12">
                    <div class="card card-chart">
                        <div class="card-header ">
                            <h3 class="card-title" align="center">{{$title}}</h3>
                        </div>
                    </div>
                </div>
            </div>
<?php $i=0; ?>
  <table class="table">
   @if($branch_id=='2')
    <tr>
        <td colspan="8" align="center">ABBC Foundation Belarus</td>
    </tr>
    <tr>
        <td colspan="8" align="center">Republic of Belarus, minsk</td>
    </tr>
    <tr>
        <td colspan="8" align="center">Prospekt Pobediteley 108</td>
    </tr>
    <tr>
        <td colspan="8" align="center">Reviera plaza business centre office 205</td>
    </tr>
    <tr>
      <td>&nbsp;</td><td colspan="3">ABBC Foundation Belarus</td>
      <td>&nbsp;</td><td colspan="3">ABBC Foundation Belarus</td>
    </tr>
    @else
    <tr>
        <td colspan="8" align="center">ABBC Block Chain IT Solutions LLC</td>
    </tr>
    <tr>
        <td colspan="8" align="center">Al Manara Tower, AL Abraj Street</td>
    </tr>
    <tr>
        <td colspan="8" align="center">Office 2408,Bussiness Bay</td>
    </tr>
    <tr>
        <td colspan="8" align="center">Emirate : Dubai</td>
    </tr>

    <tr>
        <td colspan="8"  align="center">{{$report_duration}}</td>
    </tr>

      @if($report_id==1)
        <tr>
          <td>&nbsp;</td><td colspan="3">ABBC Block Chain IT Solutions LLC</td>
          <td>&nbsp;</td><td colspan="3">ABBC Block Chain IT Solutions LLC</td>
        </tr>
          @endif
    @endif

    @foreach($allAccountDetails as $allAccountDetail)
      @if($allAccountDetail->report_id==1)
            @if($i==0)
                <tr>
                    <td>Opening Balance</td><td>&nbsp;</td><td>&nbsp;</td>
                    <td align="right">{{$openbalance}}</td><td> Indirect Expenses</td>
                    <td colspan="2">&nbsp;</td><td>{{$allAccountDetail->total_indirect_expense}}</td>
                </tr>
            @elseif($i==1)
                    <tr>
                        <td>Cash-in-Hand</td><td colspan="3">&nbsp;</td>
                        <td>{{$allAccountDetail->payments}}</td><td>&nbsp;</td>
                        <td>{{$allAccountDetail->amount}}</td>
                        <td>&nbsp;</td>
                    </tr>
            @elseif($i>1 && $i!=36 && $i!=38)
                <tr>
                    <td colspan="4">&nbsp;</td>
                    <td>{{$allAccountDetail->payments}}</td><td>&nbsp;</td>
                    <td>{{$allAccountDetail->amount}}</td>
                </tr>

           @elseif($i>0 && $i==36)
                <tr>
                    <td colspan="4">&nbsp;</td>
                    <td>{{$allAccountDetail->payments}}</td><td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>{{$allAccountDetail->total_indirect_expense}}</td>
                </tr>

            @elseif($i==38)
                <tr>
                    <td>Total</td><td>&nbsp;</td><td>&nbsp;</td>
                    <td align="right">{{$openbalance}}</td><td>Total</td>
                    <td colspan="2">&nbsp;</td>
                    <td>{{$openbalance}}</td>
                </tr>
            @endif

       @elseif($allAccountDetail->report_id==2)

               @if($i==0)
                   <tr>
                       <td>Date</td><td>Particulars</td><td>Vch Type</td>
                       <td>Vch No</td><td>Debit</td>
                       <td colspan="3">&nbsp;</td>
                   </tr>
               @endif
               <tr>

                   <td>@if($allAccountDetail->dateofexpense!='' && $i!=30 && $i!=31)
                           {{$allAccountDetail->dateofexpense}}
                       @endif</td>
                   <td>{{$allAccountDetail->particulars}}</td>
                   <td>{{$allAccountDetail->vch_type}}</td>
                   <td>
                       @if($allAccountDetail->vch_no!='' && $i!=30 && $i!=31)
                           {{$allAccountDetail->vch_no}}
                           @elseif($i!=31)
                           Current Total :
                            @elseif($i!=30)
                           Closing Balance:
                       @endif

                       {{$allAccountDetail->vch_no}}</td>
                   <td>{{$allAccountDetail->debit}}</td>
                   <td colspan="3">&nbsp;</td>
               </tr>

          @endif


    <?php $i++; ?>
    @endforeach
        </table>
      </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="/assets/js/core/jquery.min.js"></script>
<script src="/assets/js/core/popper.min.js"></script>
<script src="/assets/js/core/bootstrap.min.js"></script>
<script src="/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="/assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });


    $('#pagination a').on('click', function (e) {
        e.preventDefault();
        var url = $('#search').attr('action') + '?page=' + page;
        $.post(url, $('#search').serialize(), function (data) {
            $('#posts').html(data);
        });
    });


    $(function () {
        var table = $('#account-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '<?php echo route('account.data'); ?>',
            columns: [
                {data: 'report_id', name: 'report_id'},
                {data: 'branch_id', name: 'branch_id'},
                {data: 'report_duration', name: 'report_duration'},
                {data: 'created_at', name: 'created_at'},
                {data: 'view', name: 'view', orderable: false, searchable: false},
            ]
        });
    });

</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
</body>
</html>
