<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body class="">
<div class="wrapper">
@extends('layouts.mastersidebar')
<div class="main-panel">
    <!-- Navbar -->
    @include('layouts.navbar')
    <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalFollowUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ __('Change deadline and Priority') }}</h4>
        </div>

        <div class="modal-body">

            {!! Form::model($lead, [
              'method' => 'PATCH',
              'route' => ['leads.followup', $lead->id],
              ]) !!}

              <div class="form-group">
                {!! Form::label('contact_date', __('Next Deadline'), ['class' => 'control-label']) !!}
                {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('priority', __('Priority'), ['class' => 'control-label']) !!}<br />
                <select name="priority" id="priority">
                    <option value="1" <?php if($lead->priority==1){ ?> selected="selected" <?php } ?>>Low</option>
                    <option value="2" <?php if($lead->priority==2){ ?> selected="selected" <?php } ?>>Medium</option>
                    <option value="3" <?php if($lead->priority==3){ ?> selected="selected" <?php } ?>>High</option>
                </select>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default col-lg-6"
                data-dismiss="modal">{{ __('Close') }}</button>
                <div class="col-lg-6">
                    {!! Form::submit( __('Update Deadline'), ['class' => 'btn btn-success form-control closebtn']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="ModalCommandEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ __('Change deadline and Priority') }}</h4>
            </div>

            <div class="modal-body">

                {!! Form::model($lead, [
                  'method' => 'PATCH',
                  'route' => ['leads.followup', $lead->id],
                  ]) !!}
                  <div class="form-group">
                    {!! Form::label('contact_date', __('Next Deadline'), ['class' => 'control-label']) !!}
                    {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('priority', __('Priority'), ['class' => 'control-label']) !!}<br />
                    <select name="priority" id="priority">
                        <option value="1" <?php if($lead->priority==1){ ?> selected="selected" <?php } ?>>Low</option>
                        <option value="2" <?php if($lead->priority==2){ ?> selected="selected" <?php } ?>>Medium</option>
                        <option value="3" <?php if($lead->priority==3){ ?> selected="selected" <?php } ?>>High</option>
                    </select>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default col-lg-6"
                    data-dismiss="modal">{{ __('Close') }}</button>
                    <div class="col-lg-6">
                        {!! Form::submit( __('Update Deadline'), ['class' => 'btn btn-success form-control closebtn']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</div>

<!-- End Navbar -->
<div class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="card card-chart">
            <div class="card-body">
                <div class="row">
                    @include('partials.userheader')
                    <div class="col-md-3">
                        <div class="sidebarheader">
                            <h3>{{ __('Project Information') }}</h3>
                        </div>
                        <div class="sidebarbox">
                            <p>Project Title: {{$lead->title}} </p>
                            <p>{{ __('Assigned to') }}:
                                <a href="">
                                    {{$lead->user->name}}</a></p>
                                    <p class="smalltext">{{ __('Created') }}:
                                        {{ date('d F, Y, H:i:s', strtotime($lead->created_at))}}
                                        @if($lead->updated_at != $lead->created_at)
                                        <br/>{{ __('Modified') }}: {{date('d F, Y, H:i:s', strtotime($lead->updated_at))}}
                                    @endif</p>
                                    @if($lead->days_until_contact < 2)
                                    <p>{{ __('Deadline') }}: <span style="color:red;">{{date('d, F Y, H:i', strTotime($lead->contact_date))}}

                                        @if($lead->status == 1 && $lead->user_assigned_id==Auth::user()->id) ({!! $lead->days_until_contact !!}) @endif</span> <i
                                        class="glyphicon glyphicon-calendar" data-toggle="modal"
                                        data-target="#ModalFollowUp"></i></p> <!--Remove days left if lead is completed-->

                                        @else
                                        <p>{{ __('Deadline') }}: <span style="color:green;">{{date('d, F Y, H:i', strTotime($lead->contact_date))}}

                                            @if($lead->status == 1 && $lead->user_assigned_id==Auth::user()->id) ({!! $lead->days_until_contact !!})<i
                                            class="glyphicon glyphicon-calendar" data-toggle="modal"
                                            data-target="#ModalFollowUp"></i>@endif</span></p>
                                            <!--Remove days left if lead is completed-->
                                            @endif
                                            @if($lead->status == 1)
                                            <span class="label text-info">     {{ ('Status') }}
                                            : {{ ('In Progresss') }}</span>
                                            @elseif($lead->status == 2)
                                            <span class="label text-success">    {{ ('Status') }} : Completed</span>
                                            @elseif($lead->status == 3)
                                            <span class="label text-danger"> {{ ('Status') }}
                                            : {{ ('Not interested') }}</span>
                                            @endif

                                            @if($lead->priority==1)
                                            <p>{{ __('Priority') }}: <span class="btn btn-primary btn-sm">{{ __('Low') }}</span></p>
                                            @elseif($lead->priority==2)
                                            <p>{{ __('Priority') }}: <span class="btn btn-success btn-sm">{{ __('Medium') }}</span></p>
                                            @elseif($lead->priority==3)
                                            <p>{{ __('Priority') }}: <span class="btn btn-danger btn-sm">{{ __('High') }}</span></p>
                                            @endif

                                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalCommandEdit">Edit deadline and Priority</button>

                                        </div>

                                        @if($settings->lead_complete_allowed == 1)
                                        @if($lead->status == 1 && ($lead->user_assigned_id==Auth::user()->id || Entrust::hasRole('administrator')) )
                                        {!! Form::model($lead, [
                                         'method' => 'PATCH',
                                         'url' => ['leads/updatestatus', $lead->id],
                                         ]) !!}
                                         @if($lead->status == 1)
                                         {!! Form::submit(__('Complete Lead'), ['class' => 'btn text-success form-control closebtn movedown']) !!}
                                         {!! Form::close() !!}
                                         @endif
                                         @endif
                                         @else
                                         {!! Form::model($lead, [
                                          'method' => 'PATCH',
                                          'url' => ['leads/updatestatus', $lead->id],
                                          ]) !!}
                                          @if($lead->status == 1)
                                          {!! Form::submit(__('Complete Lead'), ['class' => 'btn text-success form-control closebtn movedown']) !!}
                                          {!! Form::close() !!}
                                          @endif
                                          @endif
                                      </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-5">
                                        @include('partials.comments', ['subject' => $lead])
                                    </div>
                                    <div class="col-md-7">
                                        <div class="sidebarheader">
                                            <p>&nbsp;</p>
                                            <h4>{{ __('Team Member') }}</h4>
                                        </div>
                                        <div class="sidebarbox">
                                            <div class="card card-chart" style="background-color:#1d1e2c; border-radius: 25px; padding: 5px;">
                                                <div class="card-body">
                                                    <table class="table table-hover" id="memberTasks-table" >
                                                        <thead>
                                                            <tr>
                                                                <th>{{ __('Name') }}</th>
                                                                <th>{{ __('Task') }}</th>
                                                                <th>{{ __('Deadline') }}</th>
                                                                <th>  <select name="priority" id="priority-memberTasks">
                                                                    <option value="" disabled selected>{{ __('Priority') }}</option>
                                                                    <option value="normal">Low</option>
                                                                    <option value="standard">Medium</option>
                                                                    <option value="urgent">High</option>
                                                                    <option value="all">All</option>
                                                                </select>
                                                            </th>

                                                            <th>
                                                                <select name="status" id="status-memberTasks">
                                                                    <option value="" disabled selected>{{ __('Status') }}</option>
                                                                    <option value="open">Open</option>
                                                                    <option value="closed">Closed</option>
                                                                    <option value="all">All</option>
                                                                </select>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

    // we simulate the window Resize so the charts will get updated in realtime.
    var simulateWindowResize = setInterval(function () {
    window.dispatchEvent(new Event('resize'));
    }, 180);

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(function () {
    clearInterval(simulateWindowResize);
    }, 1000);
    });

        $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
            var $btn = $(this);

            if (white_color == true) {

                $('body').addClass('change-background');
                setTimeout(function () {
                    $('body').removeClass('change-background');
                    $('body').removeClass('white-content');
                }, 900);
                white_color = false;
            } else {

                $('body').addClass('change-background');
                setTimeout(function () {
                    $('body').removeClass('change-background');
                    $('body').addClass('white-content');
                }, 900);

                white_color = true;
            }

        });

        $('.light-badge').click(function () {
            $('body').addClass('white-content');
        });

        $('.dark-badge').click(function () {
            $('body').removeClass('white-content');
        });
    });
});
</script>
<script>
    $(function () {

        var table = $('#memberTasks-table').DataTable({
            processing: true,
            serverSide: true,
            bInfo: false,
            bPaginate: false,
            ajax: '{!! route('membertasks.data', $lead->id) !!}',
            columns: [
            {data: 'user_assigned_id', name: 'user_assigned_id',},
            {data: 'titlelink', name: 'title'},
            {data: 'deadline', name: 'deadline'},
            {data: 'priority', name: 'priority', orderable: false},
            {data: 'status', name: 'status', orderable: false},
            ]
        });

        $('#priority-memberTasks').change(function() {
            selected = $("#priority-memberTasks option:selected").val();
            if(selected == 'normal') {
                table.columns(3).search(1).draw();
            } else if(selected == 'standard') {
                table.columns(3).search(2).draw();
            } else if(selected == 'urgent') {
                table.columns(3).search(3).draw();
            } else {
                table.columns(3).search( '' ).draw();
            }
        });

        $('#status-memberTasks').change(function () {
            selected = $("#status-memberTasks option:selected").val();
            if (selected == 'open') {
                table.columns(4).search(1).draw();
            } else if (selected == 'closed') {
                table.columns(4).search(2).draw();
            } else {
                table.columns(4).search('').draw();
            }
        });
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
</body>
</html>