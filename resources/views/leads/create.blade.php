@extends('layouts.master')
@section('heading')
    <h1>{{ __('Create Project') }}</h1>
@stop

@section('content')

    {!! Form::open([
            'route' => 'leads.store', 'enctype' => 'multipart/form-data'
            ]) !!}

    <div class="form-group">
        {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-inline">
        <div class="form-group col-lg-3 removeleft">
            {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
            {!! Form::select('status', array(
            '1' => 'Open', '2' => 'Completed'), null, ['class' => 'form-control'] )
         !!}
        </div>
        <div class="form-group col-lg-4 removeleft">
            {!! Form::label('contact_date', __('Deadline'), ['class' => 'control-label']) !!}
            {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-lg-5 removeleft removeright">
            {!! Form::label('contact_time', __('Time'), ['class' => 'control-label']) !!}
            {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
        <input type="file" name="uploaded_files[]" class="form-control" multiple>
    </div>

    <?php
        $users = $users->toArray();
        $userarr = array();
        $i =0;
        foreach($users as $user=>$val){
            $userid = $val['id'];
            $userarr[$userid] = $val['name'];
            $i++;
        }
        $departments = $departments->toArray();
        $departmentsarr = array();
        $i =0;
        foreach($departments as $key=>$value){
            $deptid = $value['id'];
            $departmentsarr[$deptid] = $value['name'];
            $i++;
        }
    ?>

    <div class="form-group">
        {!! Form::label('user_assigned_id', __('Assign Manager'), ['class' => 'control-label']) !!}
        {!! Form::select('user_assigned_id', $userarr, null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('dept_id', __('Assign department'), ['class' => 'control-label']) !!}
        {!! Form::select('dept_id', $departmentsarr, null, ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit(__('Create new Project'), ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop