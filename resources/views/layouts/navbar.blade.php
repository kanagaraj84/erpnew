<nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle d-inline">
                <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            {{--<a class="navbar-brand" href="javascript:void(0)">Dashboard</a>--}}
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navigation">
        <ul class="navbar-nav ml-auto">
            {{--<li class="search-bar input-group">
                <button class="btn btn-link" id="search-button" data-toggle="modal" data-target="#searchModal"><i
                    class="tim-icons icon-zoom-split"></i>
                    <span class="d-lg-none d-md-block">Search</span>
                </button>
            </li>--}}
            <?php $notifications = auth()->user()->unreadNotifications; ?>
            <li class="dropdown nav-item">
                <a href="javascript:void(0)" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <div class="notification d-none d-lg-block d-xl-block"></div>
                    <p class="dot">
                        {{ $notifications->count() }}
                    </p>
                </a>
                <ul class="dropdown-menu dropdown-menu-right dropdown-navbar">
                    @if($notifications->count() >0)
                        @foreach($notifications as $notification)
                        <a href="{{ route('notification.read', ['id' => $notification->id])  }}"
                           class="nav-item dropdown-item" onClick="postRead({{ $notification->id }})">
                           <div class="alert alert-info">
                            <li class="nav-link">
                                <img src="/{{ auth()->user()->avatar }}" class="notification-profile-image"
                                width="25px">
                                <span>{{ $notification->data['message']}}</span></li>

                            </div>
                        </a>
                        @endforeach
                    @else
                        No Notifciations
                    @endif

                </ul>
            </li>
            <li class="dropdown nav-item">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                    <div class="photo">
                        <img src="/{{ auth()->user()->avatar }}" alt="Profile Photo">
                    </div>
                    <b class="caret d-none d-lg-block d-xl-block"></b>
                    <p class="d-lg-none">
                        Log out
                    </p>
                </a>
                <ul class="dropdown-menu dropdown-navbar">
                        <li class="nav-link">
                        <a href="{{route('users.show', auth()->user()->id)}}"
                           class="nav-item dropdown-item">Profile</a>
                       </li>
                       @if(Entrust::hasRole('administrator'))
                           <li class="nav-link">
                            <a href="{{route('settings.index')}}" class="nav-item dropdown-item">Settings</a>
                           </li>
                       @endif
                    <li class="dropdown-divider"></li>
                    <li class="nav-link">
                        <a href="{{route('logout')}}" class="nav-item dropdown-item">Log out</a>
                    </li>
                </ul>
            </li>
            <li class="separator d-lg-none"></li>
        </ul>
    </div>
</div>
</nav>