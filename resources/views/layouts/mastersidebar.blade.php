<div class="sidebar">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
  -->
  <div class="sidebar-wrapper">
    <div class="logo">
        <a href="javascript:void(0)" class="simple-text logo-mini">
            <img src="{{ asset('images/abbc_logo.png') }}" width="100px"
      alt="">
        </a>
        <a href="/dashboard" class="simple-text logo-normal">
            ABBC ERP SYSTEM
        </a>
    </div>
    <ul class="nav">
            <li >
                <a href="{{route('dashboard', \Auth::id())}}">
                    <i class="tim-icons icon-bank"></i>
                    <p style="font-size: 13px; font-weight: 500;">Dashboard</p>
                </a>
            </li>

            @foreach($allUsersdepartments as $allUsersdepartment)
            @if($allUsersdepartment->user_id == \Auth::id())
            <?php $currentDeptId = $allUsersdepartment->department_id; ?>
            @endif
            @endforeach

            @if(isset($departments))
            @foreach($departments as $key=>$val)
            @if($val['name']=='Marketing' && ($currentDeptId==2 || Entrust::hasRole('administrator')))
            <li>
                <a href="{{route('marketing')}}">
                    <i class="tim-icons icon-globe-2"></i>
                    <p style="font-size: 13px; font-weight: 500;">{{$val['name']}}</p>
                </a>
            </li>
            @endif

            @if($val['name']=='IT' && ($currentDeptId==3 || Entrust::hasRole('administrator')))
            <li>
                <a href="{{route('itdashboard')}}">
                    <i class="tim-icons icon-laptop"></i>
                    <p style="font-size: 13px; font-weight: 500;">{{$val['name']}}</p>
                </a></li>
            @endif
            @endforeach
            @endif

            @if($currentDeptId==2 || Entrust::hasRole('administrator'))
            <li>
            <a href="{{ route('topic.index')}}"> <i class="tim-icons icon-email-85"></i>
                <p style="font-size: 13px; font-weight: 500;">{{ __('Email Support') }}</p></a>
            </li>
            @endif

            @if($currentDeptId==4 ||Entrust::hasRole('administrator'))
            <li>
                <a href="{{ route('account.index')}}"> <i class="tim-icons icon-bank"></i>
                    <p style="font-size: 13px; font-weight: 500;">{{ __('Accounts') }}</p></a>
            </li>
            @endif

            @if($currentDeptId==5 ||Entrust::hasRole('administrator'))
                <li>
                    <a href="{{ route('hr.index')}}"> <i class="tim-icons icon-bank"></i>
                        <p style="font-size: 13px; font-weight: 500;">{{ __('HR') }}</p></a>
                </li>
            @endif

            </ul>
        </div>
    </div>
