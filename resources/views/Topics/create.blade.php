@extends('layouts.master')
@section('tab-title')
    {{__('- Create Email issue')}}
@stop

@section('content')
    <div class="col-md-8 ">
        <div class="lf_wrap_content">
            <div class="lf_page_title">
                <h2>{{__('Create New Email issue')}}</h2>
            </div>
            @if (count($errors)>0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            @endif
            <form action="{{route('topic.store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="title">{{__('Topic Title')}}</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="{{__('Enter Title For The Topic')}}" value="{{old('title')}}">
                </div>
                <div class="form-group">
                    <label for="details">{{__('Details')}}</label>
                    <textarea class="form-control" id="details" rows="20" name="details" data-provide="markdown" data-iconlibrary="fa" data-hidden-buttons="cmdPreview">{{old('details')}}</textarea>
                    <input type="hidden" class="form-control" id="tags" value="" name="tags">
                    <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{Auth::user()->id}}">

                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="{{__('Submit Email issue')}}">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop
