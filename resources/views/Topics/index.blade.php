@extends('layouts.master')
@section('heading')
    <h1>{{ __('All Email issues') }}</h1>
@stop

@section('content')
    <table class="table table-hover" id="topics-table">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Details') }}</th>
            <th>{{ __('Created By') }}</th>
            <th>{{ __('Created at') }}</th>
            <th>{{ __('Updated at') }}</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
    </table>

@stop

@push('scripts')
<script>
    $(function () {
        $('#topics-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('topic.data') !!}',
            columns: [
                {data: 'titlelink', name: 'title'},
                {data: 'details', name: 'details'},
                {data: 'user_id', name: 'user_id'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false},
            ]
        });
    });


</script>
@endpush
