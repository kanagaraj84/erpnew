<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $("#expensecategory").hide();
            $("#emptab").click(function () {
                $("#employees").show();
                $("#companies").hide();
            });
            $("#companytab").click(function () {
                $("#employees").hide();
                $("#companies").show();
            });
        });
    </script>
</head>

<body class="">
<div class="modal fade" id="addEmpInfo" tabindex="-1" role="dialog" data-bac`kdrop="static" style="margin-top: -200px;" >
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Add Employee Information</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                    'route' => 'account.store' , 'enctype' => 'multipart/form-data'
                    ]) !!}
                <div class="form-group">
                    {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('branch', __('Branch'), ['class' => 'control-label']) !!}
                    {!! Form::select('branch_id', array(
                    '' => 'Select', '1' => 'ABBC Dubai', '2' => 'Belarus', '3' => 'Manara', '4' => 'Consolidated'), null,
                    ['class' => 'form-control','id' => 'branch_id','required' => 'required'] )
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('report_duration', __('Duraion'), ['class' => 'control-label']) !!}
                    {!! Form::text('report_duration', null, ['class' => 'form-control','autocomplete' => 'off', 'required' => 'required']) !!}
                </div>
                <div>
                    {!! Form::label('uploaded_files', __('Upload file'), ['class' => 'control-label']) !!}
                    <input type="file" name="import_file[]" id="import_file" class="form-control" multiple required><br />
                </div>
                <div class="form-group">
                    {!! Form::label('description', __('Purpose/Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('amount', __('Amount'), ['class' => 'control-label']) !!}
                    {!! Form::number('amount', null, ['class' => 'form-control','id' => 'amount', 'required' => 'required']) !!}
                </div>

                <input type="hidden" id="userid" name="userid">

                {!! Form::submit(__('Add Info'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close</button>
                <span class="pull-right"></span>
            </div>
        </div>
    </div>
</div>
<div class="wrapper">
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')

        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h3>HR</h3>
                </div>
            </div>


    <div class="row">
    <div class="col-lg-12">
    <div class="card card-chart">
    <div class="card-header ">
        <div class="row">
            <div class="col-sm-6 text-left">
                <h5 class="card-category"></h5>
                @if(Session::has('flash_message_warning'))
                    <message message="{{ Session::get('flash_message_warning') }}"
                             type="warning"></message>
                @endif
                @if(Session::has('flash_message'))
                    <message message="{{ Session::get('flash_message') }}" type="success"></message>
                @endif
            </div>
            <div class="col-sm-6 text-right">
                <div id="dynamicData"></div>
            </div>
        </div>


        <ul class="nav nav-tabs">
            <li id="emptab" class="btn btn-sm btn-primary btn-simple active tex"><a
                        class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                        data-toggle="tab"
                        href="#employees">Employees</a></li>

            <li id="companytab" class="btn btn-sm btn-primary btn-simple"><a
                        class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                        data-toggle="tab"
                        href="#companies">Companies</a></li>
        </ul>

    </div>
    <div class="card-body">
        <div id="employees" class="tab-pane fade in active in show">
            <table class="table table-hover" id="hrusers-table">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Mail') }}</th>
            <th>{{ __('Work number') }}</th>
            <th>{{ __('Personal number') }}</th>
            <th></th>
        </tr>
        </thead>
    </table>
        </div>
        <div id="companies" class="tab-pane fade">
            <table class="table table-hover" id="companies-table">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Description') }}</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
    </table>
        </div>
    </div>
    </div>
    </div>

    </div>

        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    <a href="javascript:void(0)" target="_blank">ABBC ERP</a>
                </div>
            </div>
        </footer>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');
            $full_page = $('.full-page');
            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;
            window_width = $(window).width();
            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            $('.fixed-plugin a').click(function(event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function() {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function() {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function() {
                $('body').removeClass('white-content');
            });
        });
    });

    $(function () {
        var table = $('#hrusers-table').DataTable({
                    processing: true,
                    serverSide: true,
                    lengthChange: false,
                    bInfo: false,
                    bPaginate: false,
                    ajax: '{!! route('hrusers.data') !!}',
                    columns: [
                        {data: 'namelink', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'work_number', name: 'work_number'},
                        {data: 'personal_number', name: 'work_number'},
                        {data: 'add_details', name: 'add_details', orderable: false, searchable: false},
                    ]
                });
    });


</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
</body>
</html>