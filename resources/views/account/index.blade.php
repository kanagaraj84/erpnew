<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="../assets/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script type="text/javascript" src="../assets/daterangepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#expensecategory").hide();
            $("#exptab").click(function () {
                $("#expenses").show();
                $("#expensecategory").hide();
                $("#dynamicData").show();
            });
            $("#expensecategorytab").click(function () {
                $("#expenses").hide();
                $("#expensecategory").show();
                $("#dynamicData").hide();
            });
        });
    </script>
    <style>
        .drp-buttons{
            display:none!important;
        }
    </style>
</head>
<body class="">

<div class="modal fade" id="AccountForm" tabindex="-1" role="dialog" data-bac`kdrop="static" style="margin-top: -220px;" >
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Upload Expense</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                    'route' => 'account.store' , 'enctype' => 'multipart/form-data'
                    ]) !!}
                <div class="form-group">
                    {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('category', __('Category'), ['class' => 'control-label']) !!}
                    {!! Form::select('category_id', $expenseCategories, null,
                    ['class' => 'form-control','id' => 'category_id','required' => 'required'] )
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('branch', __('Branch'), ['class' => 'control-label']) !!}
                    {!! Form::select('branch_id', array(
                    '' => 'Select', '1' => 'ABBC Dubai', '2' => 'Belarus', '3' => 'Manara', '4' => 'Consolidated'), null,
                    ['class' => 'form-control','id' => 'branch_id','required' => 'required'] )
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('report_duration', __('Duraion'), ['class' => 'control-label']) !!}
                    {!! Form::text('report_duration', null, ['class' => 'form-control','autocomplete' => 'off', 'required' => 'required']) !!}
                </div>
                <div>
                    {!! Form::label('uploaded_files', __('Upload file'), ['class' => 'control-label']) !!}
                    <input type="file" name="import_file[]" id="import_file" class="form-control" multiple required><br />
                </div>
                <div class="form-group">
                    {!! Form::label('description', __('Purpose/Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('amount', __('Amount'), ['class' => 'control-label']) !!}
                    {!! Form::number('amount', null, ['class' => 'form-control','id' => 'amount', 'required' => 'required']) !!}
                </div>

                {!! Form::submit(__('Upload'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}

            </div>
            <script>
                $('input[name="report_duration"]').daterangepicker();
            </script>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close</button>
                <span class="pull-right"></span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="expensecat" tabindex="-1" role="dialog" data-bac`kdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Add Category</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                    'route' => 'expensecat.store' , 'enctype' => 'multipart/form-data'
                    ]) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
                {!! Form::submit(__('Create Category'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>

            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
                <span class="pull-right"></span>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')
        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

<!-- End Navbar -->
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <h3>ACCOUNTING</h3>
        </div>
    </div>

<div class="row">
<div class="col-12">
<div class="card card-chart">
<div class="card-header ">
    <div class="row">
        <div class="col-sm-6 text-left">
            <h5 class="card-category"></h5>
            @if(Session::has('flash_message_warning'))
                <message message="{{ Session::get('flash_message_warning') }}"
                         type="warning"></message>
            @endif
            @if(Session::has('flash_message'))
                <message message="{{ Session::get('flash_message') }}" type="success"></message>
            @endif
        </div>
        <div class="col-sm-6 text-right">
        <div id="dynamicData"></div>
        </div>
    </div>


    <ul class="nav nav-tabs">
        <li id="exptab" class="btn btn-sm btn-primary btn-simple active tex"><a
                    class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                    data-toggle="tab"
                    href="#expenses">Expenses</a></li>

        <li id="expensecategorytab" class="btn btn-sm btn-primary btn-simple"><a
                    class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                    data-toggle="tab"
                    href="#expensecategory">Total</a></li>
    </ul>

</div>

    <?php
    $startval = isset($startDateVal)? $startDateVal.' - ' : '';
    $endval = isset($endDateVal)? $endDateVal : '';
    $showdateVal = $startval.$endval;

    ?>
<div class="container" class="tab-content">
<div class="card-body" style="height:auto;">
    <div id="expenses" class="tab-pane fade in active in show">
        <p><a href="#AccountForm" data-toggle="modal" data-href="{{url('accounts/create')}}">
                Upload Expenses <i class="fa fa-plus-circle" style="font-size:24px;"></i></a></p>

        <div class="col-sm-6" style="margin-left: 750px; margin-top: 10px;">
            {!! Form::open([ 'route' => 'account.index']) !!}
            <div class="form-group">
                <input id="datesearch" class="dates" name="datesearch" type="text" autocomplete="off" >
                {!! Form::submit(__('Search'), ['class' => 'btn-primary', 'id' => 'submitSelectDates']) !!}
            </div>
            @if($showdateVal!=' - ')
                <div class="form-group" id="showDateVal" style="color: #fff"><?php echo "Result Between ".$showdateVal; ?></div>
            @endif

            {!! Form::close() !!}
        </div>
        <script>
            $('input[name="datesearch"]').daterangepicker();
            $('#datesearch').val('');
        </script>
        <h3>{{ __('List of Expenses') }}</h3>
        <table class="table table-hover" id="account-table">
            <thead>
            <tr>
                <th>{{ __('Title') }}</th>
                <th><select name="branch_id" id="branch_id-account">
                        <option value="" disabled selected>{{ __('Branch') }}</option>
                        <option value="1">ABBC Dubai</option>
                        <option value="2">Belarus</option>
                        <option value="3">Manara</option>
                        <option value="4">Others</option>
                        <option value="all">All</option>
                    </select>
                </th>
                <th>
                    <select name="category_id" id="category_id-account">
                        <option value="" disabled selected>{{ __('Category') }}</option>
                        <option value="1">Labour contracts</option>
                        <option value="2">Staff Salary</option>
                        <option value="3">Rental</option>
                        <option value="4">Marketing</option>
                        <option value="5">IT</option>
                        <option value="6">Others</option>
                        <option value="all">All</option>
                    </select>
                </th>
                <th>{{ __('Duration') }}</th>
                <th>{{ __('Created at') }}</th>
                <th>{{ __('Amount') }}</th>
                <th>{{ __('Uploaded Files') }}</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <div id="expensecategory" class="tab-pane fade">
        {{--<p><a href="#expensecat" data-toggle="modal"
              data-href="{{url('expensecat/create')}}">
                Add New Category <i class="fa fa-plus-circle"
                                    style="font-size:24px;"></i></a></p>
        <h3>{{ __('List of Categories') }}</h3>--}}
        <table class="table table-hover" id="expensecat-table">
            <thead>
            <tr>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('Uploaded at') }}</th>
                <th>{{ __('Total Expenses') }}</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {

        $.ajax({
            url: '{!! route('expense.totaldata'); !!}',
            data : 'sval=<?php echo $startDateVal; ?>&eval=<?php echo $endval; ?>',
            cache: false,
            success: function(data) {
                $('#dynamicData').html(data);
            }
        });


        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });

    $('#pagination a').on('click', function (e) {
        e.preventDefault();
        var url = $('#search').attr('action') + '?page=' + page;
        $.post(url, $('#search').serialize(), function (data) {
            $('#posts').html(data);
        });
    });

    $(function () {
        var table = $('#account-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '<?php echo route('account.data').'?sval='.$startDateVal.'&eval='.$endval; ?>',
            columns: [
                {data: 'titlelink', name: 'title'},
                {data: 'branch_id', name: 'branch_id', orderable: false},
                {data: 'category_id', name: 'category_id', orderable: false},
                {data: 'report_duration', name: 'report_duration'},
                {data: 'created_at', name: 'created_at'},
                {data: 'amount', name: 'amount'},
                {data: 'uploaded_files', name: 'uploaded_files'},
                {data: 'edit', name: 'edit'},
                {data: 'delete', name: 'delete'}
            ]
        });

        $('#branch_id-account').change(function () {
            var selected = $("#branch_id-account option:selected").val();
            $.ajax({
                url: '<?php echo route('expense.branchtotaldata')."?branchid="; ?>'+selected,
                cache: false,
                success: function(data) {
                    $('#dynamicData').html(data);
                }
            });

            if (selected =='1') {
                table.columns(1).search(1).draw();
            } else if (selected =='2') {
                table.columns(1).search(2).draw();
            } else if (selected =='3') {
                table.columns(1).search(3).draw();
            } else if (selected =='4') {
                table.columns(1).search(4).draw();
            } else if (selected =='all') {
                table.columns(1).search('').draw();
            }
        });

        $('#category_id-account').change(function () {
            var selected = $("#category_id-account option:selected").val();
            $.ajax({
                url: '<?php echo route('expense.catetotaldata')."?cateid="; ?>'+selected,
                cache: false,
                success: function(data) {
                    $('#dynamicData').html(data);
                }
            });
            if (selected =='1') {
                table.columns(2).search(1).draw();
            } else if (selected =='2') {
                table.columns(2).search(2).draw();
            } else if (selected =='3') {
                table.columns(2).search(3).draw();
            } else if (selected =='4') {
                table.columns(2).search(4).draw();
            } else if (selected =='5') {
                table.columns(2).search(5).draw();
            } else if (selected =='6') {
                table.columns(2).search(6).draw();
            } else if (selected =='all') {
                table.columns(2).search('').draw();
            }
        });
    });

    $(function () {
        var table = $('#expensecat-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '<?php echo route('expensecat.data'); ?>',
            columns: [
                {data: 'title', name: 'title'},
                {data: 'description', name: 'description'},
                {data: 'created_at', name: 'created_at'},
                {data: 'total_expense', name: 'total_expense'},
                {data: 'edit', name: 'edit'},
                {data: 'delete', name: 'delete'}
            ]
        });
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>
</body>
</html>
