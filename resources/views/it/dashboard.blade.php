<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#tasks").hide();
            $("#sns").hide();
            $("#article").hide();
            $("#repos").hide();
            $("#projtab").click(function () {
                $("#tasks").hide();
                $("#projects").show();
                $("#article").hide();
                $("#sns").hide();
                $("#repos").hide();
            });
            $("#tasktab").click(function () {
                $("#tasks").show();
                $("#projects").hide();
                $("#article").hide();
                $("#sns").hide();
                $("#repos").hide();
            });

            $("#snstab").click(function () {
                $("#tasks").hide();
                $("#projects").hide();
                $("#article").hide();
                $("#sns").show();
                $("#repos").hide();
            });

            $("#articletab").click(function () {
                $("#article").show();
                $("#sns").hide();
                $("#tasks").hide();
                $("#projects").hide();
                $("#repos").hide();
            });

            $("#repotab").click(function () {
                $("#article").hide();
                $("#sns").hide();
                $("#tasks").hide();
                $("#projects").hide();
                $("#repos").show();
            });
        });
    </script>
</head>
<body class="">
<?php
$allLeads = $allLeads->toArray();
$leadarr = array();
$i =0;
foreach($allLeads as $key=>$value){
    $leadid = $value['id'];
    $leadarr[$leadid] = $value['title'];
    $i++;
} ?>

<div class="modal fade" id="ProjectForm" tabindex="-1" role="dialog" data-bac`kdrop="static"  style="margin-top: -200px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Create Project</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                        'route' => 'leads.store' , 'enctype' => 'multipart/form-data'
                        ]) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {{ Form::hidden('status', '1', array('id' => 'status')) }}
                </div>

                <div class="form-group">
                    {!! Form::label('priority', __('Priority'), ['class' => 'control-label']) !!}
                    {!! Form::select('priority', array(
                    '1' => 'Low', '2' => 'Medium', '3' => 'High'), null, ['class' => 'form-control'] )
                 !!}
                </div>

                <div class="form-group">
                    {!! Form::label('contact_date', __('Deadline'), ['class' => 'control-label']) !!}
                    {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('contact_time', __('Time'), ['class' => 'control-label']) !!}
                    {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
                </div>

                <div>
                    {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
                    <input type="file" name="uploaded_files[]" class="form-control" multiple>
                </div>

                <?php
                $alluserManagers = $alluserManagers->toArray();
                $userarr = array();
                $i =0;
                foreach($alluserManagers as $user=>$val){
                    $userid = $val['id'];
                    $userarr[$userid] = $val['name'];
                    $i++;
                }

                $alluserEmployees = $alluserEmployees->toArray();
                $userEmparr = array();
                $i =0;
                foreach($alluserEmployees as $user=>$val){
                    $userid = $val['id'];
                    $userEmparr[$userid] = $val['name'];
                    $i++;
                }

                $departments = $departments->toArray();
                $departmentsarr = array();
                $i =0;
                foreach($departments as $key=>$value){
                    $deptid = $value['id'];
                    $departmentsarr[$deptid] = $value['name'];
                    $i++;
                } ?>

                <div class="form-group">
                    {!! Form::label('user_assigned_id', __('Assign Manager'), ['class' => 'control-label']) !!}
                    {!! Form::select('user_assigned_id', $userarr, null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {{ Form::hidden('dept_id', '3', array('id' => 'dept_id')) }}
                </div>

                {!! Form::submit(__('Create new Project'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close</button>
                <span class="pull-right">
        </span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="TaskForm" tabindex="-1" role="dialog" data-backdrop="static"  style="margin-top: -230px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Create Task</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                        'route' => 'tasks.store' , 'enctype' => 'multipart/form-data'
                        ]) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title') , ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {{ Form::hidden('status', '1', array('id' => 'status')) }}
                </div>

                <div class="form-group">
                    {!! Form::label('deadline', __('Deadline'), ['class' => 'control-label']) !!}
                    {!! Form::date('deadline', \Carbon\Carbon::now()->addDays(3), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {{ Form::hidden('taskcategory_id', '0', array('id' => 'taskcategory_id')) }}
                </div>

                <div class="form-group">
                    {!! Form::label('priority', __('Priority'), ['class' => 'control-label']) !!}
                    {!! Form::select('priority', array(
                    '1' => 'Low', '2' => 'Medium', '3' => 'High'), null, ['class' => 'form-control'] )
                 !!}
                </div>

                <div class="form-group">
                    {!! Form::label('user_assigned_id', __('Assign user'), ['class' => 'control-label']) !!}
                    {!! Form::select('user_assigned_id', $userEmparr, null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('lead_id', __('Assign project'), ['class' => 'control-label']) !!}
                    {!! Form::select('lead_id', $leadarr, null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div>
                    {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
                    <input type="file" name="uploaded_files[]" class="form-control" multiple>
                </div>

                {!! Form::submit(__('Create task'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close</button>
                <span class="pull-right">
        </span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="repoform" tabindex="-1" role="dialog" data-backdrop="static"  style="margin-top: -120px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Upload Repo</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'project.fitupload', 'enctype' => 'multipart/form-data']) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title') , ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                </div>
                <div class="form-group">
                    {{ Form::hidden('status', '2', array('id' => 'status')) }}
                    {{ Form::hidden('taskcategory_id', '0', array('id' => 'taskcategory_id')) }}
                </div>
                <div class="form-group">
                    {!! Form::label('lead_id', __('Related Project'), ['class' => 'control-label']) !!}
                    {!! Form::select('lead_id', $leadarr, null, ['class' => 'form-control','required' => 'required']) !!}
                </div>
                <div>
                    {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
                    <input type="file" name="uploaded_files[]" class="form-control" multiple required>
                </div>
                {!! Form::submit(__('Upload Repo'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
                <span class="pull-right">
        </span>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')
        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h3>INFORMATION TECHNOLOGY</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-chart">
                        <div class="card-header" style="margin-top: 30px;">
                            <a href="{{route('tasks.index')."?st=it"}}"><h5 class="card-category"></h5></a>
                            <h3 class="card-title" align="center"><i class="tim-icons icon-bell-55 text-info"></i> {{$alltasks}} IT Tasks
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="info-box-content">
                                <span class="info-box-text"> {{ __('All Tasks') }} </span>
                                <span class="info-box-number">{{$allCompletedITTasks}} / {{$alltasks}}</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: {{$totalPercentageTasks}}%"></div>
                                </div>
                                <span class="progress-description">
                    {{number_format($totalPercentageTasks, 0)}}% {{ __('Completed') }}
                  </span>
                            </div>
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h4>
                             <!--            <p> @foreach($taskCompletedThisMonth as $thisMonth)
                                                {{$thisMonth->total}}
                                            @endforeach{{ __('Tasks completed this month') }}</p> -->
                                    </h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-chart">
                        <div class="card-header" style="margin-top: 30px;">
                            <a href="{{route('leads.index')."?st=it"}}"><h5 class="card-category"></h5></a>
                            <h3 class="card-title" align="center"><i class="tim-icons icon-calendar-60 text-info"></i> {{$totalallITLeads}} IT Projects
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="info-box-content">
                                <span class="info-box-text">{{ __('All Projects') }}</span>
                                <span class="info-box-number">{{$allitCompletedLeads}} / {{$totalallITLeads}}</span>

                                <div class="progress">
                                    <div class="progress-bar" style="width: {{$totalPercentageLeads}}%"></div>
                                </div>
                                <span class="progress-description">
                    {{number_format($totalPercentageLeads, 0)}}% {{ __('Completed') }}
                  </span>
                            </div>

                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h4>
                                    <!--     <p>  @foreach($leadCompletedThisMonth as $thisMonth)
                                                {{$thisMonth->total}}
                                            @endforeach
                                            {{ __('Leads completed this month') }}</p> -->
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-chart">
                        <div class="card-header">
                            <h5 class="card-title"><?php
                                $todayDate = date("d");
                                echo $todayDate . " " . date('F', mktime(0, 0, 0, date('m'), 1)). " ".date("Y");
                                ?></h5>

                            <h5 class="card-title">
                                <i class="tim-icons icon-calendar-60 text-success"></i>
                                Completed Projects <span class="text-success"><b>{{$completedLeadsToday}}</b></span></h5>
                        </div>
                        <div class="card-header">
                            <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-danger"></i>
                                Created Projects <span class="text-danger"><b>{{$createdLeadsToday}}</b></span></h5>
                        </div>

                        <div class="card-header">
                            <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-success"></i>
                                Completed Tasks <span class="text-success"><b>{{$completedTasksToday}}</b></span></h5>
                        </div>
                        <div class="card-header">
                            <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-danger"></i>
                                Created Tasks <span class="text-danger"><b>{{$createdTasksToday}}</b></span></h5>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-chart">
                        <div class="card-header ">
                            <div class="row">
                                <div class="col-sm-6 text-left">
                                    <h5 class="card-category"></h5>
                                    {{--<h2 class="card-title">ABBC Foundation</h2>--}}
                                    @if(Session::has('flash_message_warning'))
                                        <message message="{{ Session::get('flash_message_warning') }}" type="warning"></message>
                                    @endif
                                    @if(Session::has('flash_message'))
                                        <message message="{{ Session::get('flash_message') }}" type="success"></message>
                                    @endif
                                </div>
                            </div>
                            <ul class="nav nav-tabs">
                                <li id="projtab" class="btn btn-sm btn-primary btn-simple active tex"><a
                                            class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                            data-toggle="tab"
                                            href="#projects">Projects</a></li>
                                <li id="tasktab" class="btn btn-sm btn-primary btn-simple"><a
                                            class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                            data-toggle="tab"
                                            href="#tasks">Tasks</a></li>

                                <li id="repotab" class="btn btn-sm btn-primary btn-simple"><a
                                            class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                            data-toggle="tab"
                                            href="#repos">Repositories</a></li>
                            </ul>
                        </div>

                        <div class="container" class="tab-content">
                            <div class="card-body" style="height:auto;">
                                <div id="projects" class="tab-pane fade in active in show">
                                    @if(Entrust::can('lead-create'))
                                    <a href="#ProjectForm" data-toggle="modal" data-href="{{url('leads/create')}}">
                                        Add New Project <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
                                    @endif
                                    <p>&nbsp;</p>
                                    <table class="table table-hover" id="leads-table">
                                        <thead>
                                        <tr>
                                            <th>{{ __('Title') }}</th>
                                            <th>{{ __('Assigned To') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Deadline') }}</th>
                                            <th>
                                                <select name="status" id="status-lead">
                                                    <option value="" disabled selected>{{ __('Status') }}</option>
                                                    <option value="open">Open</option>
                                                    <option value="closed">Closed</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </th>
                                            <th>  <select name="priority" id="priority-lead">
                                                    <option value="" disabled selected>{{ __('Priority') }}</option>
                                                    <option value="normal">Low</option>
                                                    <option value="standard">Medium</option>
                                                    <option value="urgent">High</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>

                                <div id="tasks" class="tab-pane fade">
                                    @if(Entrust::can('task-create'))
                                    <a href="#TaskForm" data-toggle="modal" data-href="{{url('tasks/create')}}">
                                        Add New Task <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
                                    @endif
                                    <p>&nbsp;</p>
                                    <table class="table table-hover" id="tasks-table">
                                        <thead>
                                        <tr>
                                            <th>{{ __('Title') }}</th>
                                            <th>{{ __('Project') }}</th>
                                            <th>{{ __('Assigned To') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Deadline') }}</th>
                                            <th>
                                                <select name="status" id="status-tasks">
                                                    <option value="" disabled selected>{{ __('Status') }}</option>
                                                    <option value="open">Open</option>
                                                    <option value="closed">Closed</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </th>

                                            <th>  <select name="priority" id="priority-tasks">
                                                    <option value="" disabled selected>{{ __('Priority') }}</option>
                                                    <option value="normal">Low</option>
                                                    <option value="standard">Medium</option>
                                                    <option value="urgent">High</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>

                                <div id="repos" class="tab-pane fade">

                                    <a href="#repoform" data-toggle="modal" data-href="">
                                        Add New Repository <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
                                    <br>
                                    <br>
                                    <table class="table table-hover" id="repos-table">
                                        <thead>
                                        <th>{{ __('Title') }}</th>
                                        <th>{{ __('Project') }}</th>
                                        <th>{{ __('Uploaded By') }}</th>
                                        <th>{{ __('Uploaded At') }}</th>
                                        <th>{{ __('Uploaded Files') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    <a href="javascript:void(0)" target="_blank">ABBC ERP
                </div>
            </div>
        </footer>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });

</script>
</body>

</html>

<script>
    $(function () {
        var table = $('#tasks-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '{!! route('pages.ittaskData') !!}',
            columns: [
                {data: 'titlelink', name: 'title'},
                {data: 'lead_id', name: 'lead_id'},
                {data: 'user_assigned_id', name: 'user_assigned_id'},
                {data: 'created_at', name: 'created_at'},
                {data: 'deadline', name: 'deadline'},
                {data: 'status', name: 'status', orderable: false},
                {data: 'priority', name: 'priority', orderable: false},
                {data: 'edit', name: 'edit', orderable: false}
            ]
        });

        $('#status-tasks').change(function () {
            selected = $("#status-tasks option:selected").val();
            if (selected == 'open') {
                table.columns(5).search(1).draw();
            } else if (selected == 'closed') {
                table.columns(5).search(2).draw();
            } else {
                table.columns(5).search('').draw();
            }
        });

        $('#priority-tasks').change(function() {
            selected = $("#priority-tasks option:selected").val();
            if(selected == 'normal') {
                table.columns(6).search(1).draw();
            } else if(selected == 'standard') {
                table.columns(6).search(2).draw();
            } else if(selected == 'urgent') {
                table.columns(6).search(3).draw();
            } else {
                table.columns(6).search( '' ).draw();
            }
        });
    });


    $(function () {
        var table = $('#repos-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '{!! route('pages.repoITdata') !!}',
            columns: [
                {data: 'titlelink', name: 'project_name'},
                {data: 'project_id', name: 'project_id'},
                {data: 'user_id', name: 'user_id'},
                {data: 'created_at', name: 'created_at'},
                {data: 'uploaded_files', name: 'uploaded_files'}
            ]
        });
    });

    $(function () {
        var table = $('#leads-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '{!! route('pages.itleaddata') !!}',
            columns: [
                {data: 'titlelink', name: 'title'},
                {data: 'user_assigned_id', name: 'user_assigned_id'},
                {data: 'created_at', name: 'created_at'},
                {data: 'contact_date', name: 'contact_date'},
                {data: 'status', name: 'status', orderable: false},
                {data: 'priority', name: 'priority', orderable: false},
            ]
        });

        $('#status-lead').change(function () {
            selected = $("#status-lead option:selected").val();
            if (selected == 'open') {
                table.columns(4).search(1).draw();
            } else if (selected == 'closed') {
                table.columns(4).search(2).draw();
            } else {
                table.columns(4).search('').draw();
            }
        });

        $('#priority-lead').change(function() {
            selected = $("#priority-lead option:selected").val();
            if(selected == 'normal') {
                table.columns(5).search(1).draw();
            } else if(selected == 'standard') {
                table.columns(5).search(2).draw();
            } else if(selected == 'urgent') {
                table.columns(5).search(3).draw();
            } else {
                table.columns(5).search( '' ).draw();
            }
        });
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>

