<div class="modal fade" id="ProjectForm" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Create Project</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                        'route' => 'leads.store'
                        ]) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title'), ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-inline">
                    <div class="form-group col-lg-3 removeleft">
                        {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
                        {!! Form::select('status', array(
                        '1' => 'Open', '2' => 'Completed'), null, ['class' => 'form-control'] )
                     !!}
                    </div>
                    <div class="form-group col-lg-4 removeleft">
                        {!! Form::label('contact_date', __('Deadline'), ['class' => 'control-label']) !!}
                        {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-lg-5 removeleft removeright">
                        {!! Form::label('contact_time', __('Time'), ['class' => 'control-label']) !!}
                        {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
                    </div>

                </div>

                <?php
                $alluserManagers = $alluserManagers->toArray();
                $userarr = array();
                $i = 0;
                foreach ($alluserManagers as $user => $val) {
                    $userid = $val['id'];
                    $userarr[$userid] = $val['name'];
                    $i++;
                }

                $alluserEmployees = $alluserEmployees->toArray();
                $userEmparr = array();
                $i = 0;
                foreach ($alluserEmployees as $user => $val) {
                    $userid = $val['id'];
                    $userEmparr[$userid] = $val['name'];
                    $i++;
                }

                $departments = $departments->toArray();
                $departmentsarr = array();
                $i = 0;
                foreach ($departments as $key => $value) {
                    $deptid = $value['id'];
                    $departmentsarr[$deptid] = $value['name'];
                    $i++;
                } ?>

                <div class="form-group">
                    {!! Form::label('user_assigned_id', __('Assign Manager'), ['class' => 'control-label']) !!}
                    {!! Form::select('user_assigned_id', $userarr, null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {{ Form::hidden('dept_id', '3', array('id' => 'dept_id')) }}
                </div>

                {!! Form::submit(__('Create new Project'), ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
                <span class="pull-right">
        </span>
            </div>
        </div>
    </div>
</div>
<?php

$allITLeads = $allITLeads->toArray();
$leadarr = array();
$i = 0;
foreach ($allITLeads as $key => $value) {
    $leadid = $value['id'];
    $leadarr[$leadid] = $value['title'];
    $i++;
} ?>

<div class="modal fade" id="TaskForm" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Create Task</h4>
            </div>
            <div class="modal-body">
                {!! Form::open([
                        'route' => 'tasks.store' , 'id' => 'taskform'
                        ]) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title') , ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
                </div>

                <div class="form-inline">
                    <div class="form-group col-sm-6 removeleft ">
                        {!! Form::label('deadline', __('Deadline'), ['class' => 'control-label']) !!}
                        {!! Form::date('deadline', \Carbon\Carbon::now()->addDays(3), ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group col-sm-6 removeleft removeright">
                        {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
                        {!! Form::select('status', array(
                        '1' => 'Open', '2' => 'Completed'), null, ['class' => 'form-control'] )
                     !!}
                    </div>
                </div>

                <div class="form-group form-inline">
                    {!! Form::label('taskcategory_id', __('Select Task category'), ['class' => 'control-label']) !!}
                    {!! Form::select('taskcategory_id', $taskcategories, null, ['class' => 'form-control']) !!}

                    {!! Form::label('user_assigned_id', __('Assign user'), ['class' => 'control-label']) !!}
                    {!! Form::select('user_assigned_id', $userEmparr, null, ['class' => 'form-control']) !!}

                    {!! Form::label('lead_id', __('Assign project'), ['class' => 'control-label']) !!}
                    {!! Form::select('lead_id', $leadarr, null, ['class' => 'form-control']) !!}
                </div>

                {!! Form::submit(__('Create task'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
                <span class="pull-right">
        </span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="repo" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="favoritesModalLabel">Upload Repo</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(['route' => 'project.fupload', 'enctype' => 'multipart/form-data']) !!}

                <div class="form-group">
                    {!! Form::label('title', __('Title') , ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                </div>


                <div class="form-inline">
                    <div class="form-group col-sm-6 removeleft removeright">
                        {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
                        {!! Form::select('status', array(
                           '2' => 'Completed'), null, ['class' => 'form-control'] )
                     !!}
                    </div>
                </div>

                <div class="form-group col-sm-6 removeright">
                    {!! Form::label('lead_id', __('Related Project'), ['class' => 'control-label']) !!}
                    {!! Form::select('lead_id', $leadarr, null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
                    <input type="file" name="uploaded_files[]" class="form-control" multiple>
                </div>

                {!! Form::submit(__('Upload Repo'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}

            </div>
            <div class="modal-footer">
                <button type="button"
                        class="btn btn-default"
                        data-dismiss="modal">Close
                </button>
                <span class="pull-right">
        </span>
            </div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <el-tabs active-name="leads" style="width:100%">

        <el-tab-pane label="All Projects" name="leads">
            <a href="#ProjectForm" data-toggle="modal" data-href="{{url('leads/create')}}">
                Add New Project <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
            <table class="table table-hover" id="leads-table">

                <thead>
                <tr>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Assigned') }}</th>
                    <th>{{ __('Created at') }}</th>
                    <th>{{ __('Deadline') }}</th>
                    <th>
                        <select name="status" id="status-lead">
                            <option value="" disabled selected>{{ __('Status') }}</option>
                            <option value="open">Open</option>
                            <option value="closed">Closed</option>
                            <option value="all">All</option>
                        </select>
                    </th>
                </tr>
                </thead>
            </table>
        </el-tab-pane>

        <el-tab-pane label="All Tasks" name="tasks">
            <a href="#TaskForm" data-toggle="modal" data-href="{{url('tasks/create')}}">
                Add New Task <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
            <table class="table table-hover" id="tasks-table">
                <thead>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Project') }}</th>
                <th>{{ __('Assigned To') }}</th>
                <th>{{ __('Created at') }}</th>
                <th>{{ __('Deadline') }}</th>
                <th>
                    <select name="status" id="status-task">
                        <option value="" disabled selected>{{ __('Status') }}</option>
                        <option value="open">Open</option>
                        <option value="closed">Closed</option>
                        <option value="all">All</option>
                    </select>
                </th>
                </tr>
                </thead>
            </table>
        </el-tab-pane>


        <el-tab-pane label="All Repositories" name="repos">
            <a href="#repo" data-toggle="modal">
                Add New Repository <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
            <table class="table table-hover" id="tasks-table">
                <thead>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Project') }}</th>
                <th>{{ __('Uploaded By') }}</th>
                <th>{{ __('Uploaded At') }}</th>

                <th>
                    {{ __('Status') }}
                </th>
                <th>{{ __('Action') }}</th>
                </tr>
                </thead>
                @if ( !$fileuploads->isEmpty() )
                    <tbody>
                    @foreach ( $fileuploads  as $fileupload)
                        <tr>
                            <td>
                                {{ $fileupload->project_name }}

                            </td>
                            <td>

                                <?php //echo "<pre>";print_r($allITLeads); die; ?>

                                @if ($allITLeads )
                                    @foreach ( $allITLeads  as $leads)
                                        @if ( $leads['id'] == $fileupload->project_id)
                                            {{ $leads['title'] }}
                                        @endif
                                    @endforeach
                                @endif
                            </td>


                            <td>
                                @if ( !$users->isEmpty() )
                                    @foreach ( $users  as $user)
                                        @if ( $user->id == $fileupload->user_id)
                                            {{ $user->name }}
                                        @endif
                                    @endforeach
                                @endif

                            </td>
                            <td>
                                {{ $fileupload->created_at }}

                            </td>
                            <td>
                                @if ( $fileupload->status="2")Completed
                                @endif
                            </td>


                            <td>

                                          <span>

                                              <?php
                                              $fpath = url('/completed_projects/'.$fileupload->project_id."/");
                                              $dir_path = public_path() . '/completed_projects/'.$fileupload->project_id.'/';
                                              $dir = new DirectoryIterator($dir_path);
                                              $i=1;
                                              foreach ($dir as $fileinfo) {
                                                  if (!$fileinfo->isDot()) {
                                                      echo "<a href='".$fpath."/".$fileinfo."'>File ".$i."</a>";
                                                      echo "<br />";
                                                      $i++;
                                                  }
                                                  else {

                                                  }
                                              }
                                              ?>
                                </span>
                            </td>

                        </tr>

                    @endforeach
                    </tbody>
                @else
                    <p><em>No Project Uploaded Yet</em></p>
                @endif
            </table>
        </el-tab-pane>


    </el-tabs>
</div>
<div style="margin-top: 20px;" class="col-sm-6">

    <div class="box box-primary">
        <div class="box-header with-border">
            <h4 class="box-title"
            >
                {{ __('Tasks each month') }}
            </h4>
            <div class="box-tools pull-right">
                <button type="button" id="collapse1" class="btn btn-box-tool" data-toggle="collapse"
                        data-target="#collapseOne"><i id="toggler1" class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="collapse" id="collapseOne" class="panel-collapse">
            <div class="box-body">

                <div>
                    <graphline class="chart" :labels="{{json_encode($createdTaskEachMonths)}}"
                               :values="{{json_encode($taskCreated)}}"
                               :valuesextra="{{json_encode($taskCompleted)}}"></graphline>
                </div>

            </div>
        </div>
    </div>

</div>
<div class="col-sm-6" style="margin-top: 20px;">


    <div class="box box-primary">
        <div class="box-header with-border">
            <h4 class="box-title"
            >
                {{ __('Projects each month') }}
            </h4>
            <div class="box-tools pull-right">
                <button type="button" id="collapse2" aria-expanded="false" class="btn btn-box-tool"
                        data-toggle="collapse"
                        data-target="#collapseTwo"><i id="toggler2" class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="collapse" id="collapseTwo" class="panel-collapse">
            <div class="box-body">

                <div>
                    <graphline class="chart" :labels="{{json_encode($createdLeadEachMonths)}}"
                               :values="{{json_encode($leadCreated)}}"
                               :valuesextra="{{json_encode($leadsCompleted)}}"></graphline>

                </div>
            </div>
        </div>
    </div>


</div>

<div class="col-sm-12">

    <div class="col-sm-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title"
                >
                    {{ __('Users') }}
                </h4>
                <div class="pull-right">
                    @if(Entrust::can('user-create'))
                        <a href="#newuserForm" data-toggle="modal" data-href="{{url('users/create')}}">Add New User <i
                                    class="fa fa-plus-circle" style="font-size:24px;"></i></a>
                    @endif
                </div>
            </div>
            <div id="collapseOne" class="panel-collapse">

                <ul class="list-group" style="background:#fff;    height: 110px; overflow: auto;">
                    @foreach($users as $user)

                        <li class="list-group-item" style="background:#fff;"><a
                                    href="{{route('users.show', $user->id)}}">
                                <img class="small-profile-picture"
                                     @if($user->image_path != "")
                                     src="images/{{$companyname}}/{{$user->image_path}}"
                                     @else
                                     src="images/default_avatar.jpg"
                                        @endif />
                            </a> &nbsp; &nbsp;
                            {{$user->name}}  </li>



                    @endforeach
                </ul>

            </div>
        </div>


    </div>

    <div class="col-sm-4">

        <div class="col-sm-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title"
                    >
                        {{ __('IT Related Activities') }}
                    </h4>
                    <div class="box-tools pull-right">

                    </div>
                </div>

            </div>
        </div>


    </div>

    <div class="col-sm-4">


        <div class="col-sm-12">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title"
                    >
                        {{ __('Marketing Related Activities') }}
                    </h4>

                </div>

            </div>
        </div>


    </div>
</div>
</div>


@push('scripts')
    <script>
        $('#pagination a').on('click', function (e) {
            e.preventDefault();
            var url = $('#search').attr('action') + '?page=' + page;
            $.post(url, $('#search').serialize(), function (data) {
                $('#posts').html(data);
            });
        });

        $(function () {
            var table = $('#tasks-table').DataTable({
                processing: true,
                serverSide: true,
                lengthChange: false,
                bInfo: false,
                bPaginate: false,
                ajax: '{!! route('pages.ittaskData') !!}',
                columns: [

                    {data: 'titlelink', name: 'title'},
                    {data: 'lead_id', name: 'lead_id'},
                    {data: 'user_assigned_id', name: 'user_assigned_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'deadline', name: 'deadline'},
                    {data: 'status', name: 'status', orderable: false},
                ]
            });
            $('#status-task').change(function () {
                selected = $("#status-task option:selected").val();
                if (selected == 'open') {
                    table.columns(5).search(1).draw();
                } else if (selected == 'closed') {
                    table.columns(5).search(2).draw();
                } else {
                    table.columns(5).search('').draw();
                }
            });
        });

        $(function () {
            var table = $('#leads-table').DataTable({
                processing: true,
                serverSide: true,
                lengthChange: false,
                bInfo: false,
                bPaginate: false,
                ajax: '{!! route('pages.itleaddata') !!}',
                columns: [
                    {data: 'titlelink', name: 'title'},
                    {data: 'user_assigned_id', name: 'user_assigned_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'deadline', name: 'deadline'},
                    {data: 'status', name: 'status', orderable: false},
                ]
            });
            $('#status-lead').change(function () {
                selected = $("#status-lead option:selected").val();
                if (selected == 'open') {
                    table.columns(5).search(1).draw();
                } else if (selected == 'closed') {
                    table.columns(5).search(2).draw();
                } else {
                    table.columns(5).search('').draw();
                }
            });
        });


    </script>
@endpush