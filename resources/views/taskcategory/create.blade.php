@extends('layouts.master')

@section('content')
    {!! Form::open([
            'route' => 'taskcategory.store',
            ]) !!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        {!! Form::label( __('Name'), 'Task category name:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null,['class' => 'form-control','required' => 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::label( __('Description'), 'Task category description:', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control','required' => 'required']) !!}
    </div>
    {!! Form::submit("Create Task category", ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

@endsection