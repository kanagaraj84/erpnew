<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body class="">

<div class="wrapper">
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')
        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <h2 class="col-lg-12">
                    @if(isset($status) && $status=='art')
                        <h2>All Articles</h2>
                    @elseif(isset($status) && $status=='sns')
                        <h2>All SNS</h2>
                    @else
                        <h2>All Tasks</h2>
                    @endif
                    <div class="card card-chart">
                        <div class="card-body">
                            <table class="table table-hover" id="tasks-table">
                                <thead>
                                <tr>
                                    <th>{{ __('Title') }}</th>
                                    <th>{{ __('Created at') }}</th>
                                    <th>{{ __('Deadline') }}</th>
                                    <th>{{ __('Project') }}</th>
                                    <th>{{ __('Assigned') }}</th>
                                    <th>  <select name="priority" id="priority-tasks">
                                            <option value="" disabled selected>{{ __('Priority') }}</option>
                                            <option value="normal">Low</option>
                                            <option value="standard">Medium</option>
                                            <option value="urgent">High</option>
                                            <option value="all">All</option>
                                        </select>
                                    </th>

                                    <th>
                                        <select name="status" id="status-tasks">
                                            <option value="" disabled selected>{{ __('Status') }}</option>
                                            <option value="open">Open</option>
                                            <option value="closed">Closed</option>
                                            <option value="all">All</option>
                                        </select>
                                    </th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });

    $('#pagination a').on('click', function (e) {
        e.preventDefault();
        var url = $('#search').attr('action') + '?page=' + page;
        $.post(url, $('#search').serialize(), function (data) {
            $('#posts').html(data);
        });
    });

    $(function () {
        var table = $('#tasks-table').DataTable({
            processing: true,
            bInfo: false,
            bPaginate: false,
            serverSide: true,
            ajax: '<?php echo route('tasks.data')."?st=".$status; ?>',
            columns: [

                {data: 'titlelink', name: 'title'},
                {data: 'created_at', name: 'created_at'},
                {data: 'deadline', name: 'deadline'},
                {data: 'lead_id', name: 'lead_id'},
                {data: 'user_assigned_id', name: 'user_assigned_id'},
                {data: 'priority', name: 'priority', orderable: false},
                {data: 'status', name: 'status', orderable: false},
            ]
        });

        $('#priority-tasks').change(function() {
            selected = $("#priority-tasks option:selected").val();
            if(selected == 'normal') {
                table.columns(5).search(1).draw();
            } else if(selected == 'standard') {
                table.columns(5).search(2).draw();
            } else if(selected == 'urgent') {
                table.columns(5).search(3).draw();
            } else {
                table.columns(5).search( '' ).draw();
            }
        });

        $('#status-tasks').change(function () {
            selected = $("#status-tasks option:selected").val();
            if (selected == 'open') {
                table.columns(6).search(1).draw();
            } else if (selected == 'closed') {
                table.columns(6).search(2).draw();
            } else {
                table.columns(6).search('').draw();
            }
        });

    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
</body>
</html>