@extends('layouts.master')
@section('heading')
    <h1>Create task</h1>
@stop

@section('content')

    {!! Form::open([
            'route' => 'tasks.store', 'enctype' => 'multipart/form-data'
            ]) !!}

    <div class="form-group">
        {!! Form::label('title', __('Title') , ['class' => 'control-label']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', __('Description'), ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-inline">
        <div class="form-group col-sm-6 removeleft ">
            {!! Form::label('deadline', __('Deadline'), ['class' => 'control-label']) !!}
            {!! Form::date('deadline', \Carbon\Carbon::now()->addDays(3), ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6 removeleft removeright">
            {!! Form::label('status', __('Status'), ['class' => 'control-label']) !!}
            {!! Form::select('status', array(
            '1' => 'Open', '2' => 'Completed'), null, ['class' => 'form-control'] )
         !!}
        </div>
    </div>


    <?php
    $users = $users->toArray();
    $userarr = array();
    $i =0;
    foreach($users as $user=>$val){
        $userid = $val['id'];
        $userarr[$userid] = $val['name'];
        $i++;
    }

    $leads = $leads->toArray();
    $leadarr = array();
    $i =0;
    foreach($leads as $key=>$value){
        $leadid = $value['id'];
        $leadarr[$leadid] = $value['title'];
        $i++;
    } ?>

    <div class="form-group form-inline">
        {!! Form::label('taskcategory_id', __('Select Task category'), ['class' => 'control-label']) !!}
        {!! Form::select('taskcategory_id', $taskcategories, null, ['class' => 'form-control']) !!}

        {!! Form::label('user_assigned_id', __('Assign user'), ['class' => 'control-label']) !!}
        {!! Form::select('user_assigned_id', $userarr, null, ['class' => 'form-control']) !!}

        {!! Form::label('lead_id', __('Assign project'), ['class' => 'control-label']) !!}
        {!! Form::select('lead_id', $leadarr, null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group">
        {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
        <input type="file" name="uploaded_files[]" class="form-control" multiple>
    </div>

    {!! Form::submit(__('Create task'), ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

@stop