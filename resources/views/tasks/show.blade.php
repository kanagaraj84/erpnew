<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body class="">

<div class="wrapper">
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')
        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ModalFollowUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('Change deadline and Priority') }}</h4>
                    </div>

                    <div class="modal-body">

                        {!! Form::model($tasks, [
                          'method' => 'PATCH',
                          'route' => ['tasks.followup', $tasks->id],
                          ]) !!}

                        <div class="form-group">
                            {!! Form::label('contact_date', __('Next follow up'), ['class' => 'control-label']) !!}
                            {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('priority', __('Priority'), ['class' => 'control-label']) !!}<br />
                            <select name="priority" id="priority">
                                <option value="1" <?php if($tasks->priority==1){ ?> selected="selected" <?php } ?>>Low</option>
                                <option value="2" <?php if($tasks->priority==2){ ?> selected="selected" <?php } ?>>Medium</option>
                                <option value="3" <?php if($tasks->priority==3){ ?> selected="selected" <?php } ?>>High</option>
                            </select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default col-lg-6"
                                    data-dismiss="modal">{{ __('Close') }}</button>
                            <div class="col-lg-6">
                                {!! Form::submit( __('Update follow up'), ['class' => 'btn btn-success form-control closebtn']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ModalCommandEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('Change deadline and Priority') }}</h4>
                    </div>

                    <div class="modal-body">

                        {!! Form::model($tasks, [
                          'method' => 'PATCH',
                          'route' => ['tasks.followup', $tasks->id],
                          ]) !!}
                        <div class="form-group">
                            {!! Form::label('contact_date', __('Next follow up'), ['class' => 'control-label']) !!}
                            {!! Form::date('contact_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::time('contact_time', '11:00', ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('priority', __('Priority'), ['class' => 'control-label']) !!}<br />
                            <select name="priority" id="priority">
                                <option value="1" <?php if($tasks->priority==1){ ?> selected="selected" <?php } ?>>Low</option>
                                <option value="2" <?php if($tasks->priority==2){ ?> selected="selected" <?php } ?>>Medium</option>
                                <option value="3" <?php if($tasks->priority==3){ ?> selected="selected" <?php } ?>>High</option>
                            </select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default col-lg-6"
                                    data-dismiss="modal">{{ __('Close') }}</button>
                            <div class="col-lg-6">
                                {!! Form::submit( __('Update follow up'), ['class' => 'btn btn-success form-control closebtn']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-chart">
                        <div class="card-body">

                            <div class="row">
                                @include('partials.userheader')
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    @include('partials.comments', ['subject' => $tasks])
                                </div>
                                <div class="col-md-3">
                                    <div class="sidebarheader">
                                        <h2>{{ __('Task information') }}</h2>
                                    </div>
                                    <div class="sidebarbox">
                                        <p>{{ __('Assigned') }}:
                                            <a href="{{route('users.show', $tasks->user->id)}}">
                                                {{$tasks->user->name}}</a></p>

                                        <p>{{ __('Created at') }}: {{ date('d F, Y, H:i', strtotime($tasks->created_at))}} </p>

                                        @if($tasks->days_until_deadline)
                                            <p>{{ __('Deadline') }}: <span style="color:red;">{{date('d, F Y', strTotime($tasks->deadline))}}

                                                    @if($tasks->status == 1)({!! $tasks->days_until_deadline !!})@endif</span></p>
                                            <!--Remove days left if tasks is completed-->

                                        @else
                                            <p>{{ __('Deadline') }}: <span style="color:green;">{{date('d, F Y', strTotime($tasks->deadline))}}

                                                    @if($tasks->status == 1)({!! $tasks->days_until_deadline !!})@endif</span></p>
                                            <!--Remove days left if tasks is completed-->
                                        @endif

                                        @if($tasks->status == 1)
                                            <p class="label text-info">     {{ __('Status') }}: {{ __('In Progresss') }}</p>
                                        @else
                                            <p class="label text-success">  {{ __('Status') }}: {{ __('Completed') }}</p>
                                        @endif

                                        @if($tasks->priority==1)
                                            <p>{{ __('Priority') }}: <span class="btn-primary">{{ __('Normal') }}</span></p>
                                        @elseif($tasks->priority==2)
                                            <p>{{ __('Priority') }}: <span class="btn-success">{{ __('Standard') }}</span></p>
                                        @elseif($tasks->priority==3)
                                            <p>{{ __('Priority') }}: <span class="btn-danger">{{ __('Urgent') }}</span></p>
                                        @endif
                                    </div>

                                    @if($settings->task_complete_allowed == 1)
                                        @if($tasks->status == 1 && ($tasks->user_assigned_id==Auth::user()->id || Entrust::hasRole('administrator')) )
                                            {!! Form::model($tasks, [
                                              'method' => 'PATCH',
                                              'url' => ['tasks/updatestatus', $tasks->id],
                                              ]) !!}
                                            @if($tasks->status == 1)
                                                {!! Form::submit(__('Close task'), ['class' => 'btn btn-success form-control closebtn']) !!}
                                                {!! Form::close() !!}
                                            @endif
                                        @endif
                                    @else
                                        {!! Form::model($tasks, [
                                              'method' => 'PATCH',
                                              'url' => ['tasks/updatestatus', $tasks->id],
                                              ]) !!}
                                        @if($tasks->status == 1)
                                            {!! Form::submit(__('Close task'), ['class' => 'btn btn-success form-control closebtn']) !!}
                                            {!! Form::close() !!}
                                        @endif
                                    @endif



                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="../assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
    $('#pagination a').on('click', function (e) {
        e.preventDefault();
        var url = $('#search').attr('action') + '?page=' + page;
        $.post(url, $('#search').serialize(), function (data) {
            $('#posts').html(data);
        });
    });

    $(function () {
        $('#tasks-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('tasks.data') !!}',
            columns: [

                {data: 'titlelink', name: 'title'},
                {data: 'created_at', name: 'created_at'},
                {data: 'deadline', name: 'deadline'},
                {data: 'lead_id', name: 'lead_id'},
                {data: 'user_assigned_id', name: 'user_assigned_id'},
                {data: 'status', name: 'status'},
                {data: 'priority', name: 'priority'}
            ]
        });
    });
</script>
</body>
</html>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>