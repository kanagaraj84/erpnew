<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="">
    <div class="wrapper">
        @extends('layouts.mastersidebar')
        <div class="main-panel">
            <!-- Navbar -->
            @include('layouts.navbar')
            <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="EmailIssueForm" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="modal_content">
                    <div class="modal-header">
                        <button type="button" class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"
                        id="favoritesModalLabel">Create Email Issue</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open([
                            'route' => 'topic.store' , 'enctype' => 'multipart/form-data'
                            ]) !!}

                            <div class="form-group">
                                <label for="title">{{__('Title')}}</label>
                                <input type="text" class="form-control" id="title" name="title"
                                placeholder="{{__('Enter Title For The Issue')}}" required>
                            </div>

                        <div class="form-group">
                            {!! Form::label('category', __('Category'), ['class' => 'control-label']) !!}
                            {!! Form::select('parent_id', $parentcat, null,
                            ['class' => 'form-control','id' => 'parent_id','required' => 'required'] )
                            !!}
                        </div>
                            <div class="form-group">
                                <label for="details">{{__('Issue Details')}}</label>
                                <textarea class="form-control" id="details" rows="10" name="details" data-provide="markdown"
                                data-iconlibrary="fa" data-hidden-buttons="cmdPreview" required></textarea>
                                <input type="hidden" class="form-control" id="tags" name="tags">
                                <input type="hidden" class="form-control" id="user_id" name="user_id"
                                value="{{Auth::user()->id}}">

                            </div>

                            <div>
                                {!! Form::label('uploaded_files', __('Select the files to upload'), ['class' => 'control-label']) !!}
                                <input type="file" name="uploaded_files[]" class="form-control" multiple>
                            </div>

                            {!! Form::submit(__('Create new Email issue'), ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Close
                        </button>
                        <span class="pull-right">
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="parentcat" tabindex="-1" role="dialog" data-backdrop="static">
            <div class="modal-dialog" role="document">
                <div class="modal-content" id="modal_content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"
                            id="favoritesModalLabel">Create Main Category</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open([
                            'route' => 'topic.storecat' , 'enctype' => 'multipart/form-data'
                            ]) !!}

                        <div class="form-group">
                            <label for="title">{{('Title')}}</label>
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="{{('')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="description">{{('Description')}}</label>
                            <textarea class="form-control" id="description" rows="10" name="description" data-provide="markdown"
                                      data-iconlibrary="fa" data-hidden-buttons="cmdPreview" required></textarea>
                        </div>
                        {!! Form::submit(('Create Main Category'), ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">Close
                        </button>
                        <span class="pull-right">
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h3>ALL ISSUES</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="card card-chart">
                        <div class="card-header">
                            <ul class="nav nav-tabs">
                                <a href="#EmailIssueForm" class="btn btn-primary btn-sm" data-toggle="modal" data-href="{{url('topic/create')}}"
                                style="float: right;">
                                <i class="fa fa-plus-circle" style="font-size:12px;"></i> Create Email Support</a>

                                <a href="#parentcat" class="btn btn-primary btn-sm" data-toggle="modal"
                                   data-href="{{url('topic/create')}}"
                                   style="float: right;">
                                    <i class="fa fa-plus-circle" style="font-size:12px;"></i> Create Main Category</a>
                            </ul>
                        </div>

                        @if (Session::has('danger'))
                        <div class="alert alert-danger">{{ Session::get('danger') }}</div>
                        @endif
                        @if (Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                        @endif
                        @forelse($topics as $topic)
                        @include('partials.topicloop')
                        @empty
                        <div class="alert alert-danger">
                            {{__('No Email Issue is  Available')}}
                        </div>
                        @endforelse
                        {{ $topics->links() }}
                    </div>

                </div>
                <?php use App\Models\ParentCat; ?>
                <div class="col-4">
                    <div class="card card-chart">
                        <div class="card-header">
                            <h4 class="card-title"><i class="tim-icons icon-atom text-danger"></i>
                            {{__('Active ISSUES')}}</h4>
                        </div>
                        <div class="card-header">
                        @forelse($parentcat  as $id=>$title)
                            <a href="#{{str_replace(' ','-',$title)}}" data-toggle="collapse" data-parent="#{{str_replace(' ','-',$title)}}"  aria-expanded="false">
                                <i class="glyphicon sidebar-icon glyphicon-cog"></i><span id="menu-txt">
                                    <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-danger"></i>
                                        {{$title}}
                                    </h5></span>
                                <i class="ion-chevron-up sidebar-arrow arrow-up"></i></a>
                                <div class="card-header">
                                    @forelse($allUsertopics as $usertopic)
                                        <?php $gwtcate = ParentCat::findOrFail($usertopic->parent_id); ?>

                                        @if($title==$gwtcate->title)
                                        <div id="{{str_replace(' ','-',$gwtcate->title)}}" class="collapse" aria-expanded="false">
                                            <h5 class="card-title">&nbsp;&nbsp;  <i class="tim-icons icon-calendar-60 text-danger"></i>
                                                <a href="{{route('topic.show',$usertopic->id)}}">{{$usertopic->title}}</a>
                                            </h5>
                                        </div>
                                            @endif
                                    @empty
                                        <a href="#" data-toggle="collapse" data-parent="#"  aria-expanded="false">
                                            <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-danger"></i>
                                                {{__('You have no active topics')}}
                                            </h5></a>
                                    @endforelse
                                </div>
                        @empty
                            <a href="#" data-toggle="collapse" data-parent="#"  aria-expanded="false">
                                <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-danger"></i>
                                {{__('You have no active topics')}}
                                </h5></a>
                            @endforelse
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-fluid">
            <div class="copyright">
                ©
                <script>
                    document.write(new Date().getFullYear())
                </script>
                <a href="javascript:void(0)" target="_blank">ABBC ERP
                </div>
            </div>
        </footer>
    </div>
</div>

<!--   Core JS Files   -->
<script src="/assets/js/core/jquery.min.js"></script>
<script src="/assets/js/core/popper.min.js"></script>
<script src="/assets/js/core/bootstrap.min.js"></script>
<script src="/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="/assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });
</script>

<script>
    $(function () {
        var table = $('#tasks-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '{!! route('pages.marketingtaskdata') !!}',
            columns: [
            {data: 'titlelink', name: 'title'},
            {data: 'lead_id', name: 'lead_id'},
            {data: 'user_assigned_id', name: 'user_assigned_id'},
            {data: 'created_at', name: 'created_at'},
            {data: 'deadline', name: 'deadline'},
            {data: 'status', name: 'status', orderable: false},
            ]
        });

        $('#status-task').change(function () {
            selected = $("#status-task option:selected").val();
            if (selected == 'open') {
                table.columns(5).search(1).draw();
            } else if (selected == 'closed') {
                table.columns(5).search(2).draw();
            } else {
                table.columns(5).search('').draw();
            }
        });
    });

    $(function () {
        var table = $('#leads-table').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            bInfo: false,
            bPaginate: false,
            ajax: '{!! route('pages.marketingleaddata') !!}',
            columns: [
            {data: 'titlelink', name: 'title'},
            {data: 'user_assigned_id', name: 'user_assigned_id'},
            {data: 'created_at', name: 'created_at'},
            {data: 'contact_date', name: 'contact_date'},
            {data: 'status', name: 'status', orderable: false},
            ]
        });

        $('#status-lead').change(function () {
            selected = $("#status-lead option:selected").val();
            if (selected == 'open') {
                table.columns(4).search(1).draw();
            } else if (selected == 'closed') {
                table.columns(4).search(2).draw();
            } else {
                table.columns(4).search('').draw();
            }
        });
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>

</body>
</html>