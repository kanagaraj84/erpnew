<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="../assets/css/black-dashboard.css" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet" />

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="../assets/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script type="text/javascript" src="../assets/daterangepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <style>
        .drp-buttons{
            display:none!important;
        }
    </style>
</head>

<body class="">
    <div class="wrapper">

<div class="modal fade" id="newuserForm" tabindex="-1" role="dialog" data-backdrop="static" style="margin-top: -300px;">
<div class="modal-dialog" role="document">
<div class="modal-content" id="modal_content">
<div class="modal-header">
    <button type="button" class="close"
    data-dismiss="modal"
    aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"
    id="favoritesModalLabel">Create User</h4>
</div>

<?php
$alluserManagers = $alluserManagers->toArray();
$userarr = array();
$i =0;
foreach($alluserManagers as $user=>$val){
    $userid = $val['id'];
    $userarr[$userid] = $val['name'];
    $i++;
}

$alluserEmployees = $alluserEmployees->toArray();
$userEmparr = array();
$i =0;
foreach($alluserEmployees as $user=>$val){
    $userid = $val['id'];
    $userEmparr[$userid] = $val['name'];
    $i++;
}
$departments = $departments->toArray();
$departmentsarr = array();
$i =0;
foreach($departments as $key=>$value){
    $deptid = $value['id'];
    $departmentsarr[$deptid] = $value['name'];
    $i++;
} ?>

<div class="modal-body">
    {!! Form::open([
        'route' => 'users.store',
        'files'=>true,
        'enctype' => 'multipart/form-data'

        ]) !!}
        <div class="">
            {{ Form::label('image_path', __('Image'), []) }}
            {!! Form::file('image_path',  null, ['class' => 'form-control']) !!}
        </div>


        <div class="form-group">
            {!! Form::label('name', __('Name'), ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control','required' => 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', __('Mail'), ['class' => 'control-label']) !!}
            {!! Form::email('email', null, ['class' => 'form-control','required' => 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('address', __('Address'), ['class' => 'control-label']) !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('work_number', __('Work number'), ['class' => 'control-label']) !!}
            {!! Form::text('work_number',  null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('personal_number', __('Personal number'), ['class' => 'control-label']) !!}
            {!! Form::text('personal_number',  null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', __('Password'), ['class' => 'control-label']) !!}
            {!! Form::password('password', ['class' => 'form-control','required' => 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password_confirmation', __('Confirm password'), ['class' => 'control-label']) !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control','required' => 'required']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('roles', __('Assign role'), ['class' => 'control-label']) !!}
            {!! Form::select('roles', $roles, isset($user->role->role_id) ? $user->role->role_id : null,
                ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('departments', __('Assign department'), ['class' => 'control-label']) !!}
            </div>
            <div class="form-group">
                {!! Form::select('departments', $departmentsarr, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit(__('Create user'), ['class' => 'btn btn-primary']) !!}
                {!! Form::close() !!}
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
</div>

<div class="modal fade" id="officialsform" tabindex="-1" role="dialog" data-backdrop="static"
style="margin-top: -100px;">
<div class="modal-dialog" role="document">
<div class="modal-content" id="modal_content">
    <div class="modal-header">
        <button type="button" class="close"
        data-dismiss="modal"
        aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"
        id="favoritesModalLabel">New Announcement</h4>
    </div>

    <div class="modal-body">
        {!! Form::open([
            'route' => 'announcement.upload',
            'enctype' => 'multipart/form-data'

            ]) !!}
            <div class="form-group">
                {!! Form::label('TITLE', ('TITLE'), ['class' => 'control-label']) !!}
                {!! Form::text('title', null, ['class' => 'form-control','required' => 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Description', ('Description'), ['class' => 'control-label']) !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}

            </div>

            <div class="form-group">
                <input type="hidden" class="form-control" id="user_id" name="user_id"
                       value="<?php echo e(Auth::user()->id); ?>">
                {!! Form::submit(__('POST'), ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}

                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close </button>
            </div>

        </div>

    </div>
</div>
</div>

@extends('layouts.mastersidebar')

<div class="main-panel">
<!-- Navbar -->
@include('layouts.navbar')

<div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="tim-icons icon-simple-remove"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- End Navbar -->
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <h3>DASHBOARD</h3>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-4">
            <div class="card card-chart">
                <div class="card-header" style="margin-top: 30px;">
                    <a href="{{route('tasks.index')}}"><h5 class="card-title"></h5>
                        <h2 class="card-title" align="center"><i class="tim-icons icon-bell-55 text-info"></i> {{$alltasks}} Tasks
                        </h2></a>
                    </div>
                    <div class="card-body">
                        <div class="info-box-content" >
                           <span class="info-box-text">  {{ ('All Tasks') }} </span>
                           <span class="info-box-number">{{$allCompletedTasks}} / {{$alltasks}}</span>
                           <div class="progress">
                            <div class="progress-bar" style="width: {{$totalPercentageTasks}}%"></div>
                        </div>
                        <span class="progress-description">
                            {{number_format($totalPercentageTasks, 0)}}% {{ ('Completed') }}
                        </span>
                    </div>
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h4>
                          <!--   <p> @foreach($taskCompletedThisMonth as $thisMonth)
                                {{$thisMonth->total}}
                                <?php  $totalMonthlyCompleteTask = $thisMonth->total; ?>
                            @endforeach{{ ('Tasks completed this month') }}</p> -->
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($taskPendingThisMonth as $thisMonth)
    <?php  $totalMonthlyPendingTask = $thisMonth->total; ?>
    @endforeach

    @foreach($allMonthlyCompletedEmailIssues as $thisMonth)
    <?php  $totalMonthlyCompletedEmailIssues = $thisMonth->total; ?>
    @endforeach



    @foreach($allMonthlyPendingEmailIssues as $thisMonth)
    <?php  $totalMonthlyPendingEmailIssues = $thisMonth->total; ?>
    @endforeach

    <?php $countAllMonthlyArticleMarketTasks =  count($allMonthlyArticleMarketTasks); ?>

    @if($countAllMonthlyArticleMarketTasks!=0)
    @foreach($allMonthlyArticleMarketTasks as $thisMonth)
    <?php $totalMonthlyArticleMarketTasks = $thisMonth->month; ?>
    @endforeach
    @else
    <?php $totalMonthlyArticleMarketTasks = 0; ?>
    @endif

    <?php $countAllMonthlySNSMarketTasks =  count($allMonthlySNSMarketTasks); ?>

    @if($countAllMonthlySNSMarketTasks!=0)
    @foreach($allMonthlySNSMarketTasks as $thisMonth)
    <?php $totalMonthlySNSMarketTasks = $thisMonth->month; ?>
    @endforeach
    @else
    <?php $totalMonthlySNSMarketTasks = 0; ?>
    @endif


    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header" style="margin-top: 30px;">
                <a href="{{route('leads.index')}}"><h5 class="card-title"></h5>
                    <h2 class="card-title" align="center"><i class="tim-icons icon-calendar-60 text-info"></i> {{$allleads}} Projects
                    </h2></a>
                </div>
                <div class="card-body">
                    <div class="info-box-content">
                        <span class="info-box-text">{{ ('All Projects') }}</span>
                        <span class="info-box-number">{{$allCompletedLeads}} / {{$allleads}}</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: {{$totalPercentageLeads}}%"></div>
                        </div>
                        <span class="progress-description">
                            {{number_format($totalPercentageLeads, 0)}}% {{ ('Completed') }}
                        </span>
                    </div>

                    <div class="small-box bg-green">
                        <div class="inner">
                            <h4>
                           <!--  <p>  @foreach($leadCompletedThisMonth as $thisMonth)
                                {{$thisMonth->total}}
                                <?php  $totalMonthlyCompleteProject = $thisMonth->total; ?>
                                @endforeach
                            {{ ('Projects completed this month') }}</p> -->
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @foreach($leadPendingThisMonth as $thisMonth)
    <?php  $totalMonthlyPendingProject = $thisMonth->total; ?>
    @endforeach

    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header">

                <h5 class="card-title"><?php
                $todayDate = date("d");
                echo $todayDate . " " . date('F', mktime(0, 0, 0, date('m'), 1)). " ".date("Y");
                ?></h5>

                <h5 class="card-title">
                    <i class="tim-icons icon-calendar-60 text-success"></i> Completed Projects
                    <span class="text-success"><b>{{$completedLeadsToday}}</b></span></h5>
                </div>
                <div class="card-header">
                    <h5 class="card-title">
                        <i class="tim-icons icon-calendar-60 text-danger"></i> Created Projects
                        <span class="text-danger"><b>{{$createdLeadsToday}}</b></span></h5>
                    </div>

                    <div class="card-header">
                        <h5 class="card-title">
                            <i class="tim-icons icon-calendar-60 text-success"></i> Completed Tasks
                            <span class="text-success"><b>{{$completedTasksToday}}</b></span></h5>
                        </div>
                        <div class="card-header">
                            <h5 class="card-title"><i class="tim-icons icon-calendar-60 text-danger"></i> Created
                                Tasks <span class="text-danger"><b>{{$createdTasksToday}}</b></span></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-chart">
                            <div class="card-header ">
                              <div class="row">
                                <div class="col-sm-6 text-left">
                                    <a href="#"><h5 class="card-title">OVERALL ACTIVITIES</h5></a>
                                </div>
                                <div class="col-sm-6">
                                    <div class="btn-group btn-group-toggle float-right" data-toggle="buttons">
                                        <label class="btn btn-sm btn-primary btn-simple active" id="0">
                                            <input type="radio" name="options" checked>
                                            <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">All</span>
                                            <span class="d-block d-sm-none">
                                              <i class="tim-icons icon-single-02"></i>
                                          </span>
                                      </label>
                                      <label class="btn btn-sm btn-primary btn-simple" id="1">
                                        <input type="radio" class="d-none d-sm-none" name="options">
                                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Monthly</span>
                                        <span class="d-block d-sm-none">
                                          <i class="tim-icons icon-gift-2"></i>
                                      </span>
                                  </label>
                                  <label class="btn btn-sm btn-primary btn-simple" id="2">
                                    <input type="radio" class="d-none" name="options">
                                    <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Daily</span>
                                    <span class="d-block d-sm-none">
                                      <i class="tim-icons icon-tap-02"></i>
                                  </span>
                              </label>
                          </div>
                      </div>
                  </div>
              </div>


<?php
    $startval = isset($startDateVal)? $startDateVal.' - ' : '';
    $endval = isset($endDateVal)? $endDateVal : '';
    $showdateVal = $startval.$endval;
?>

<div class="row">
    <div class="col-sm-6" style="margin-left: 10px;">
        {!! Form::open([ 'route' => 'dashboard']) !!}
        <div class="form-group">
            <input id="dates" class="dates" name="dates" type="text" autocomplete="off" >
            {!! Form::submit(__('Search'), ['class' => 'btn-primary', 'id' => 'submitSelectDates']) !!}
        </div>
        @if($showdateVal!=' - ')
            <div class="form-group" id="showDateVal" style="color: #fff"><?php echo "Result Between ".$showdateVal; ?></div>
        @endif

        {!! Form::close() !!}
    </div>
</div>

<script>
    $('input[name="dates"]').daterangepicker();
    $('#dates').val('');
</script>

<input type="hidden" id="allchartvalues" value="{{$allCompletedLeads.",".$allPendingLeads.",".$allCompletedTasks.",".$allPendingTasks.",".$allCompletedEmailIssues.",".$allPendingEmailIssues.",".$allArticleMarketTasks.",".$allSNSMarketTasks}}" />

<input type="hidden" id="allMonthlyChartvalues" value="{{$totalMonthlyCompleteProject.",".$totalMonthlyPendingProject.",".$totalMonthlyCompleteTask.",".$totalMonthlyPendingTask.",".$totalMonthlyCompletedEmailIssues.",".$totalMonthlyPendingEmailIssues.",".$totalMonthlyArticleMarketTasks.",".$totalMonthlySNSMarketTasks}}">

<input type="hidden" id="allDailyChartvalues" value="{{$completedLeadsToday.",".$pendingLeadsToday.",".$completedTasksToday.",".$pendingTasksToday.",".$allCompletedEmailIssuesToday.",".$allPendingEmailIssuesToday.",".$allArticleMarketTasksToday.",".$allSNSMarketTasksToday}}">

<input type="hidden" id="testhyperlink" value="{{route('marketing')}}">

<input type="hidden" id="dateRangeChartvalues" value="{{$dateBetweenchartVal}}">

<div class="card-body">
<div class="chart-area">
    <canvas id="chartBig1"></canvas>

<table width="100%">
<tr>
<td width="10%" style="text-align: center;">
<a href="{{route('leads.index')."?st=cmp"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a> </td>
<td width="10%" style="text-align: center;">
<a href="{{route('leads.index')."?st=pdg"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
<td width="10%" style="text-align: center;">
<a href="{{route('tasks.index')."?st=cmp"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
<td width="10%" style="text-align: center;">
<a href="{{route('tasks.index')."?st=pdg"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
<td width="10%" style="text-align: center;">
<a href="{{route('topic.index')}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
<td width="12%" style="text-align: center;">
<a href="{{route('topic.index')}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
<td width="18%" style="text-align: center;">
<a href="{{route('tasks.index')."?st=art"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
<td width="2%" style="text-align: center;">
<a href="{{route('tasks.index')."?st=sns"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
</tr>
</table>
<div class="clearfix"></div><p>&nbsp;</p>


</div><p>&nbsp;</p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-6">
<div class="card card-chart">
<div class="card-header">

<a href="#"><h5 class="card-title">IT DEPARTMENT ACTIVITIES</h5></a>

</div>
<div class="card-body">
<div class="chart-area">
<canvas id="ItChart"></canvas>
<input type="hidden" id="allitchartvalues" value="{{$allitCompletedLeads.",".$allPendingITLeads.",".$allCompletedITTasks.",".$allPendingITTasks }}"/>

<table width="100%">
<tr>
<td width="20%" style="text-align: center;">
<a href="{{route('leads.index')."?st=itcmp"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a> </td>
<td width="20%" style="text-align: center;">
    <a href="{{route('leads.index')."?st=itpdg"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
    <td width="20%" style="text-align: center;">
        <a href="{{route('tasks.index')."?st=itcmp"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
        <td width="20%" style="text-align: center;">
            <a href="{{route('tasks.index')."?st=itpdg"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
        </tr>
    </table>
    <div class="clearfix"></div> <p>&nbsp;&nbsp;</p>
</div>
<p>&nbsp;&nbsp;</p>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="card card-chart">
<div class="card-header">
<a href="#"><h5 class="card-title">MARKETING DEPARTMENT ACTIVITIES</h5></a>
<div class="card-body">
<div class="chart-area">
    <input type="hidden" id="allchartlinegreenvalues" value="{{$allCompletedMarketLeads.",".$allPendingMarketLeads.",".$allCompletedMarketTasks.",".$allPendingMarketTasks }}" />
    <canvas id="chartLineGreen"></canvas>

    <table width="100%">
        <tr>
            <td width="20%" style="text-align: center;">
                <a href="{{route('leads.index')."?st=markcmp"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a> </td>
                <td width="20%" style="text-align: center;">
                    <a href="{{route('leads.index')."?st=markpdg"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
                    <td width="20%" style="text-align: center;">
                        <a href="{{route('tasks.index')."?st=markcmp"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
                        <td width="20%" style="text-align: center;">
                            <a href="{{route('tasks.index')."?st=markpdg"}}" style="text-decoration: none;"><i class="fa fa-eye" style="font-size:16px"></i></a></td>
                        </tr>
                    </table>
                    <div class="clearfix"></div> <p>&nbsp;&nbsp;</p>
                </div>
                <p>&nbsp;&nbsp;</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
<div class="col-lg-5">
<div class="card card-tasks">
<div class="card-header">
   <a href="#"><h5 class="card-title">TEAM MEMBERS</h5></a>

   <h3 class="card-title"><i class="tim-icons icon-single-02"></i> {{$totalUsers}}  &nbsp;
    @if(Entrust::can('user-create'))
    <span style="float: right; font-size:12px; font-color:#ffff!important;">
        <a href="#newuserForm" data-toggle="modal" data-href="{{url('users/create')}}">ADD USER
            <i class="fa fa-plus-circle"></i></a></span>
            @endif
        </h3>
    </div>
    <div class="card-body">
        <div class="chart-area">
            <canvas id="chartLinePurple" style="display: none"></canvas>
            <div class="table-full-width">
                <table class="table">
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>
                                <a href="{{route('users.show', $user->id)}}">
                                    <img class="small-profile-picture" @if($user->image_path != "")
                                    src="images/{{$companyname}}/{{$user->image_path}}" width="30" height="30"
                                    @else
                                    src="images/default_avatar.jpg" width="30" height="30"
                                    @endif />
                                    &nbsp;  {{$user->name}}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-7">
<div class="card card-tasks">
        <div class="card-header " style="">
            <div class="form-group">
                @if(Entrust::hasRole('administrator'))
                      <a href="#officialsform" data-toggle="modal" data-href="{{url('users/create')}}">
                        <h5 class="card-title">ANNOUNCEMENTS
                         <i class="fa fa-plus-circle"></i></h5>
                      </a>
                @else
                  <a href="#"><h5 class="card-title">ANNOUNCEMENTS</h5></a>
                @endif
     </div>
 </div>

 <div class="modal fade" id="officialData" tabindex="-1" role="dialog" data-backdrop="static">
 <div class="modal-dialog" role="document" style="margin-top: 200px;">
    <div class="modal-content" id="modal_content">
        <div class="modal-header">
            <button type="button" class="close"
            data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"
            id="favoritesModalLabel">Announcement</h4>
        </div>

        <div class="modal-body"><div id="dynamicData"></div>
        </div>
        <div class="modal-footer">
            <button type="button"
            class="btn btn-default"
            data-dismiss="modal">Close
        </button>
        <span class="pull-right"></span>
    </div>
</div>
</div>
</div>

<div class="card-body">

    <div style="overflow-y:auto; height: 280px;">
    @if ( !$allOffcicials->isEmpty() )
        @foreach ( $allOffcicials  as $official)

                <a href="#officialData" class="open" data-id="{{$official->id}}" id="{{$official->id}}"  data-toggle="modal" data-href="{{url('users/create')}}">
                   <span class="title">{{ $official->title }} </span> &nbsp;
                   Posted By
                    @foreach($userswithAdmin as $user)
                        @if($official->user_id==$user->id)
                            <a href="{{route('users.show', $official->user_id)}}">
                                <img class="small-profile-picture" @if($user->image_path != "")
                                src="images/{{$companyname}}/{{$user->image_path}}" width="30" height="30"
                                     @else
                                     src="images/default_avatar.jpg" width="30" height="30"
                                        @endif />
                                &nbsp;  {{$user->name}}</a>

                            @if(Entrust::hasRole('administrator'))
                            <a href="{{route('announcement.edit',$official->id)}}" title="{{__('Edit')}}">
                                <button class="btn btn-primary btn-sm">Edit</button>
                            </a> <form action="{{route('announcement.destroy',$official->id)}}" method="post" style="display: inline;">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('{{$sureDelete}}')">Delete</button>
                            </form>
                            @endif

                        @endif
                    @endforeach
                    <p class="text-muted">{{ $official->description }}</p>
               </a>

            @endforeach
            @else
                <p><em>No ANNOUNCEMENT</em></p>
            @endif
    </div>
    </div>
      </div>
      </div>
      </div>
    </div>
</div>

<footer class="footer">
<div class="container-fluid">
<div class="copyright">
    ©
    <script>
        document.write(new Date().getFullYear())
    </script>
    <a href="javascript:void(0)" target="_blank">ABBC ERP</a>
</div>
</div>
</footer>
</div>
</div>
    <!--   Core JS Files   -->
    <script src="../assets/js/core/jquery.min.js"></script>
    <script src="../assets/js/core/popper.min.js"></script>
    <script src="../assets/js/core/bootstrap.min.js"></script>
    <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- Place this tag in your head or just before your close body tag. -->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Chart JS -->
    <script src="../assets/js/plugins/chartjs.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="../assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../assets/js/black-dashboard.min.js?v=1.0.0"></script>
    <!-- Black Dashboard DEMO methods, don't include it in your project! -->
    <script src="../assets/demo/demo.js"></script>
<script>
    $(document).ready(function() {
        $().ready(function() {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



            $('.fixed-plugin a').click(function(event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function() {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

// we simulate the window Resize so the charts will get updated in realtime.
var simulateWindowResize = setInterval(function() {
window.dispatchEvent(new Event('resize'));
}, 180);

// we stop the simulation of Window Resize after the animations are completed
setTimeout(function() {
clearInterval(simulateWindowResize);
}, 1000);
});

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function() {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function() {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function() {
                $('body').removeClass('white-content');
            });
        });
});


    $(".open").click(function() {
        var id = $(this).attr("id");
       // alert(id);
        var dataString = 'id='+ id;
        $.ajax({
           // type: "POST",
            url: '{!! route('pages.officialdata') !!}'+'?id='+id,
            data: {id: id},
            cache: false,
            success: function(data) {
                $('#dynamicData').html(data);
            }
        });
    });

</script>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>
</body>

</html>