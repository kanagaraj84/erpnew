<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href=".">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>
        ABBC ERP
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet"/>
    <!-- CSS Files -->
    <link href="/assets/css/black-dashboard.css?v=1.0.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/assets/demo/demo.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="/assets/daterangepicker.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script type="text/javascript" src="/assets/daterangepicker.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

    <script>
        $(document).ready(function () {
            $("#tasks").hide();
            $("#leaves").hide();
            $("#approveleave").hide();
            $("#projtab").click(function () {
                $("#projects").show();
                $("#tasks").hide();
                $("#approveleave").hide();
                $("#leaves").hide();
            });
            $("#tasktab").click(function () {
                $("#tasks").show();
                $("#projects").hide();
                $("#approveleave").hide();
                $("#leaves").hide();
            });

            $("#leavetab").click(function () {
                $("#tasks").hide();
                $("#projects").hide();
                $("#approveleave").hide();
                $("#leaves").show();
            });
            $("#approveleavetab").click(function () {
                $("#tasks").hide();
                $("#projects").hide();
                $("#leaves").hide();
                $("#approveleave").show();
            });
        });
    </script>
</head>

<body class="">

<div class="wrapper">
    <div class="modal fade" id="leaveform" tabindex="-1" role="dialog" data-bac`kdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="modal_content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="favoritesModalLabel">Apply For Leave</h4>
                </div>

                <div class="modal-body">
                    {!! Form::open(['route' => 'leave.store' , 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                        <input type="hidden" name="applicantid" id="applicantid" value="{{Auth::user()->id}}">
                    </div>
                    <div class="form-group">
                        {!! Form::label('type', __('Type'), ['class' => 'control-label']) !!}
                        {!! Form::select('type', array('' => 'Select', 'Sick Leave' => 'Sick Leave', 'Urgent Leave' => 'Urgent Leave', 'Annual Leave' => 'Annual Leave', 'Others' => 'Others'), null,
                        ['class' => 'form-control','id' => 'type','required' => 'required'] )
                        !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('duration', __('Duraion'), ['class' => 'control-label']) !!}
                        {!! Form::text('duration', null, ['class' => 'form-control','autocomplete' => 'off', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('shortdescription', __('Description'), ['class' => 'control-label']) !!}
                        {!! Form::textarea('shortdescription', null, ['class' => 'form-control','required' => 'required']) !!}
                    </div>
                    <div>
                        {!! Form::label('uploaded_files', __('Application File'), ['class' => 'control-label']) !!}
                        <input type="file" name="uploaded_files[]" class="form-control" multiple>

                    </div>
                    {!! Form::submit(__('Apply'), ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>

                <script>
                    $('input[name="duration"]').daterangepicker();
                </script>

                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Close
                    </button>
                    <span class="pull-right"></span>
                </div>
            </div>
        </div>
    </div>
    @extends('layouts.mastersidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('layouts.navbar')
        <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="tim-icons icon-simple-remove"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <!-- End Navbar -->
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-chart">

                        <div class="card-body">
                            <div class="info-box-content">
                                <div style="width: 100px;"><img class="profilepicsize" src="../{{ $contact->avatar }}"/>
                                </div>
                                <h1>{{ $contact->nameAndDepartment }} </h1>
                                <!--MAIL-->
                                <p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                    <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></p>
                                <!--Work Phone-->
                                <p><span class="glyphicon glyphicon-headphones" aria-hidden="true"></span>
                                    <a href="tel:{{ $contact->work_number }}">{{ $contact->work_number }}</a></p>

                                <!--Personal Phone-->
                                <p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                                    <a href="tel:{{ $contact->personal_number }}">{{ $contact->personal_number }}</a>
                                </p>

                                <!--Address-->
                                <p><span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                    {{ $contact->address }}  </p>

                            </div>
                            <div class="small-box bg-yellow">
                                <div class="inner">


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-chart">
                        <div class="card-header ">
                            <div class="row">
                                <div class="col-sm-6 text-left">
                                    <h5 class="card-category"></h5>
                                    {{--<h2 class="card-title">ABBC Foundation</h2>--}}

                                    @if(Session::has('flash_message_warning'))
                                        <message message="{{ Session::get('flash_message_warning') }}"
                                                 type="warning"></message>
                                    @endif
                                    @if(Session::has('flash_message'))
                                        <message message="{{ Session::get('flash_message') }}" type="success"></message>
                                    @endif

                                </div>


                            </div>
                            <ul class="nav nav-tabs">
                                <li id="projtab" class="btn btn-sm btn-primary btn-simple active tex"><a
                                            class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                            data-toggle="tab"
                                            href="#projects">Projects</a></li>
                                <li id="tasktab" class="btn btn-sm btn-primary btn-simple"><a
                                            class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                            data-toggle="tab"
                                            href="#tasks">Tasks</a></li>

                                @if(!Entrust::hasRole('administrator') && ($user->id==Auth::user()->id) )
                                <li id="leavetab" class="btn btn-sm btn-primary btn-simple"><a
                                            class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                            data-toggle="tab"
                                            href="#leaves">Leave Requests</a></li>
                                @endif

                                @if(Entrust::hasRole('administrator') && ($user->id==Auth::user()->id) )
                                    <li id="approveleavetab" class="btn btn-sm btn-primary btn-simple"><a
                                                class="d-none d-sm-block d-md-block d-lg-block d-xl-block text-white"
                                                data-toggle="tab"
                                                href="#approveleave">Leave Approvals</a></li>
                                @endif

                            </ul>
                        </div>

                        <div class="container" class="tab-content">
                            <div class="card-body" style="height:auto;">
                                <div id="projects" class="tab-pane fade in active in show">
                                    <table class="table table-hover" id="leads-table">
                                        <h3>{{ __('Projects Assigned') }}</h3>
                                        <thead>
                                        <tr>
                                            <th>{{ __('Title') }}</th>
                                            <th>{{ __('Assigned') }}</th>
                                            <th>{{ __('Department') }}</th>
                                            <th>{{ __('Created at') }}</th>
                                            <th>{{ __('Deadline') }}</th>
                                            <th>
                                                <select name="status" id="status-lead">
                                                    <option value="" disabled selected>{{ __('Status') }}</option>
                                                    <option value="open">Open</option>
                                                    <option value="closed">Closed</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </th>

                                            <th>  <select name="priority" id="priority-lead">
                                                    <option value="" disabled selected>{{ __('Priority') }}</option>
                                                    <option value="normal">Low</option>
                                                    <option value="standard">Medium</option>
                                                    <option value="urgent">High</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="tasks" class="tab-pane fade">
                                    <table class="table table-hover" id="tasks-table">
                                        <h3>{{ __('Tasks Assigned') }}</h3>
                                        <thead>
                                        <th>{{ __('Title') }}</th>
                                        <th>{{ __('Project') }}</th>
                                        <th>{{ __('Assigned') }}</th>
                                        <th>{{ __('Created at') }}</th>
                                        <th>{{ __('Deadline') }}</th>

                                        <th>
                                            <select name="status" id="status-tasks">
                                                <option value="" disabled selected>{{ __('Status') }}</option>
                                                <option value="open">Open</option>
                                                <option value="closed">Closed</option>
                                                <option value="all">All</option>
                                            </select>
                                        </th>

                                        <th>  <select name="priority" id="priority-tasks">
                                                <option value="" disabled selected>{{ __('Priority') }}</option>
                                                <option value="normal">Low</option>
                                                <option value="standard">Medium</option>
                                                <option value="urgent">High</option>
                                                <option value="all">All</option>
                                            </select>
                                        </th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="leaves" class="tab-pane fade">
                                    @if(!Entrust::hasRole('administrator') && ($user->id==Auth::user()->id) )
                                        <a href="#leaveform" data-toggle="modal" data-href="{{url('tasks/create')}}">
                                            Apply For Leave<i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
                                    @endif
                                    <table class="table table-hover" id="leaves-table">
                                        <h3>{{ __('Leave Requests') }}</h3>
                                        <thead>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Reason') }}</th>
                                        <th>{{ __('Duration') }}</th>
                                        <th>{{ __('Created at') }}</th>
                                        <th>
                                            <select name="status" id="status-leave">
                                                <option value="" disabled selected>{{ __('Status') }}</option>
                                                <option value="0">waiting</option>
                                                <option value="1">Approved</option>
                                                <option value="all">All</option>
                                            </select>
                                        </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                @if(Entrust::hasRole('administrator') && ($user->id==Auth::user()->id) )
                                <div id="approveleave" class="tab-pane fade">
                                    <table class="table table-hover" id="approveleave-table">
                                        <h3>{{ __('Leave Approvals') }}</h3>
                                        <thead>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Reason') }}</th>
                                        <th>{{ __('Duration') }}</th>
                                        <th>{{ __('Created at') }}</th>
                                        <th></th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="/assets/js/core/jquery.min.js"></script>
<script src="/assets/js/core/popper.min.js"></script>
<script src="/assets/js/core/bootstrap.min.js"></script>
<script src="/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="/assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
<script src="/assets/js/black-dashboard.min.js?v=1.0.0"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="/assets/demo/demo.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');
            $navbar = $('.navbar');
            $main_panel = $('.main-panel');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');
            sidebar_mini_active = true;
            white_color = false;

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();


            $('.fixed-plugin a').click(function (event) {
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .background-color span').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data', new_color);
                }

                if ($main_panel.length != 0) {
                    $main_panel.attr('data', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data', new_color);
                }
            });

            $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    sidebar_mini_active = false;
                    blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
                } else {
                    $('body').addClass('sidebar-mini');
                    sidebar_mini_active = true;
                    blackDashboard.showSidebarMessage('Sidebar mini activated...');
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);
            });

            $('.switch-change-color input').on("switchChange.bootstrapSwitch", function () {
                var $btn = $(this);

                if (white_color == true) {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').removeClass('white-content');
                    }, 900);
                    white_color = false;
                } else {

                    $('body').addClass('change-background');
                    setTimeout(function () {
                        $('body').removeClass('change-background');
                        $('body').addClass('white-content');
                    }, 900);

                    white_color = true;
                }


            });

            $('.light-badge').click(function () {
                $('body').addClass('white-content');
            });

            $('.dark-badge').click(function () {
                $('body').removeClass('white-content');
            });
        });
    });


    $('#pagination a').on('click', function (e) {
        e.preventDefault();
        var url = $('#search').attr('action') + '?page=' + page;
        $.post(url, $('#search').serialize(), function (data) {
            $('#posts').html(data);
        });
    });


    $(function () {
        @if(!Entrust::hasRole('administrator') && ($user->id==Auth::user()->id) )
        var table = $('#leaves-table').DataTable({
            processing: true,
            serverSide: true,
            bPaginate: false,
            ajax: '{!! route('users.leavedata', ['id' => $user->id]) !!}',
            columns: [
                {data: 'namelink', name: 'namelink'},
                {data: 'type', name: 'type'},
                {data: 'shortdescription', name: 'shortdescription'},
                {data: 'duration', name: 'duration'},
                {data: 'created_at', name: 'created_at'},
                {data: 'status', name: 'status', orderable: false},
            ]
        });

        $('#status-leave').change(function () {
            selected = $("#status-leave option:selected").val();
            if (selected == '0') {
                table.columns(5).search(0).draw();
            } else if (selected == '1') {
                table.columns(5).search(1).draw();
            } else {
                table.columns(5).search('').draw();
            }
        });
        @endif
    });


    $(function () {
        @if(Entrust::hasRole('administrator') && ($user->id==Auth::user()->id) )
        var table = $('#approveleave-table').DataTable({
            processing: true,
            serverSide: true,
            bPaginate: false,
            ajax: '{!! route('users.allleavedata') !!}',
            columns: [
                {data: 'namelink', name: 'namelink'},
                {data: 'type', name: 'type'},
                {data: 'shortdescription', name: 'shortdescription'},
                {data: 'duration', name: 'duration'},
                {data: 'created_at', name: 'created_at'},
                {data: 'approve', name: 'approve', orderable: false},
            ]
        });
        @endif
    });

    $(function () {
        var table = $('#tasks-table').DataTable({
            processing: true,
            serverSide: true,
            bPaginate: false,
            ajax: '{!! route('users.taskdata', ['id' => $user->id]) !!}',
            columns: [

                {data: 'titlelink', name: 'title'},
                {data: 'lead_id', name: 'lead_id'},
                {data: 'user_assigned_id', name: 'user_assigned_id'},
                {data: 'created_at', name: 'created_at'},
                {data: 'deadline', name: 'deadline'},
                {data: 'status', name: 'status', orderable: false},
                {data: 'priority', name: 'priority', orderable: false}
            ]
        });

        $('#status-tasks').change(function() {
            selected = $("#status-tasks option:selected").val();
            if(selected == 'open') {
                table.columns(5).search(1).draw();
            } else if(selected == 'closed') {
                table.columns(5).search(2).draw();
            } else {
                table.columns(5).search( '' ).draw();
            }
        });

        $('#priority-tasks').change(function() {
            selected = $("#priority-tasks option:selected").val();
            if(selected == 'normal') {
                table.columns(6).search(1).draw();
            } else if(selected == 'standard') {
                table.columns(6).search(2).draw();
            } else if(selected == 'urgent') {
                table.columns(6).search(3).draw();
            } else {
                table.columns(6).search( '' ).draw();
            }
        });

    });

    $(function () {
        var table = $('#leads-table').DataTable({
            processing: true,
            serverSide: true,
            bPaginate: false,
            ajax: '{!! route('users.leaddata', ['id' => $user->id]) !!}',
            columns: [

                {data: 'titlelink', name: 'title'},
                {data: 'user_assigned_id', name: 'user_assigned_id'},
                {data: 'dept_id', name: 'dept_id'},
                {data: 'created_at', name: 'created_at'},
                {data: 'contact_date', name: 'contact_date'},
                {data: 'status', name: 'status', orderable: false},
                {data: 'priority', name: 'priority', orderable: false}
            ]
        });

        $('#status-lead').change(function() {
            selected = $("#status-lead option:selected").val();
            if(selected == 'open') {
                table.columns(5).search(1).draw();
            } else if(selected == 'closed') {
                table.columns(5).search(2).draw();
            } else {
                table.columns(5).search( '' ).draw();
            }
        });

        $('#priority-lead').change(function() {
            selected = $("#priority-lead option:selected").val();
            if(selected == 'normal') {
                table.columns(6).search(1).draw();
            } else if(selected == 'standard') {
                table.columns(6).search(2).draw();
            } else if(selected == 'urgent') {
                table.columns(6).search(3).draw();
            } else {
                table.columns(6).search( '' ).draw();
            }
        });
    });
</script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
</body>
</html>
