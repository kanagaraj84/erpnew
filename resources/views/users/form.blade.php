<div>
    {{ Form::label('image_path', __('Image'), ['class' => 'control-label','style' => 'color:#fff;']) }}
    {!! Form::file('image_path',  null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>


<div class="form-group">
    {!! Form::label('name', __('Name'), ['class' => 'control-label','style' => 'color:#fff;']) !!}
    {!! Form::text('name', null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', __('Mail'), ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<div class="form-group">
    {!! Form::label('address', __('Address'), ['class' => 'control-label']) !!}
    {!! Form::text('address', null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<div class="form-group">
    {!! Form::label('work_number', __('Work number'), ['class' => 'control-label']) !!}
    {!! Form::text('work_number',  null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<div class="form-group">
    {!! Form::label('personal_number', __('Personal number'), ['class' => 'control-label']) !!}
    {!! Form::text('personal_number',  null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<div class="form-group">
    {!! Form::label('password', __('Password'), ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>
<div class="form-group">
    {!! Form::label('password_confirmation', __('Confirm password'), ['class' => 'control-label']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<?php
$departments = $departments->toArray();
$departmentsarr = array();
$i =0;
foreach($departments as $key=>$value){
$deptid = $value['id'];
$departmentsarr[$deptid] = $value['name'];
$i++;
} ?>
<div class="form-group">
    {!! Form::label('roles', __('Assign role'), ['class' => 'control-label','style' => 'color:#fff;']) !!}
    {!! Form::select('roles', $roles, isset($user->role->role_id) ? $user->role->role_id : null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

<div class="form-group">
    {!! Form::label('departments', __('Assign department'), ['class' => 'control-label','style' => 'color:#fff;']) !!}
    {!! Form::select('departments', $departmentsarr, null, ['class' => 'form-control','style' => 'color:#fff;']) !!}
</div>

{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
