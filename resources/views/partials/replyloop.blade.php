<div class="post lf_comment_list lf_reply_list" id="replyno{{$reply->id}}" >
    <?php $imgpath = URL::asset("/images/Media/" .$reply->user->image_path); ?>
    <p>&nbsp;</p>
    <div class="topwrap">
        <span class="pull-right" style="margin-right: 5px;">
            <form action="{{route('reply.delete',$reply->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <input type="hidden" name="comment_id" value="{{$comment->id}}">
                <input type="hidden" name="topic_id" value="{{$topic->id}}">
                <button type="submit"  class="btn btn-danger btn-sm"  onclick="return confirm('{{$sureDelete}}')">Delete Reply</button>
            </form>
            @include('partials.replyedit')
        </span>

        <div class="">
            @if($reply->user->image_path)
            <div class="">
                <a href="{{route('users.users',$reply->user->id)}}">&nbsp;&nbsp; <img src="{{$imgpath}}" width="80" height="80" alt="{{$reply->user->name}}"> &nbsp;&nbsp; <strong class="lf_commenter_name">&nbsp;&nbsp;{{$reply->user->name .' '}} <span>{{ __('said')}}</span></strong></a>
            </div>
            @else
            <div class="">
                <a href="{{route('users.users',$reply->user->id)}}">&nbsp;&nbsp; <img src="{{ Gravatar::fallback(url
                    ('uploads/avater.png'))->get($reply->user->email) }}" alt="{{$reply->user->name}}">&nbsp;&nbsp; <strong class="lf_commenter_name">{{$reply->user->name .' '}} <span>{{ __('said')}}</span></strong></a>
                </div>
                @endif
                <p>&nbsp;{!! Michelf\Markdown::defaultTransform(strip_tags($reply->body))  !!}

                    <span class=" pull-left"><i class="fa fa-clock-o"></i>  {{ $reply->created_at->diffForHumans()}}</span>
                </p>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>