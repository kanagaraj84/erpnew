
<div class="post" style="margin-top:15px; margin-left: 10px; margin-right: 10px; padding:15px; background-color:#1d1e2c; border-radius: 25px;">
  <div class="wrap-ut pull-left">
    <div class="userinfo pull-left">
        <?php $imgpath = URL::asset("/images/Media/" .$topic->user->image_path); ?>
      <div class="">
        <a href="{{route('users.users',$topic->user->id)}}" style="text-decoration: none;">
          <img src="{{$imgpath}}" width="80" height="80" alt="{{$topic->user->name}}"></a>
      </div>


      </div>
      <div class="lf_icons pull-right">
        <div class="lf_edit pull-right">

          <a href="{{route('topic.edit',$topic->id)}}" title="{{__('Edit')}}">
            <button class="btn btn-primary btn-sm">Edit</button>
          </a>
        </div>

        <div class="lf_del">
          <form action="{{route('topic.destroy',$topic->id)}}" method="post">
            {{csrf_field()}}
            {{method_field('DELETE')}}
            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('{{$sureDelete}}')">Delete</button>
          </form>
        </div>
      </div>
      <div class="posttext pull-right">
        <h2 class="lf_topic_title" style="margin-top: 10px;"><a href="{{route('topic.show',$topic->id)}}">{{$topic->title}}</a></h2>
        <p style="color:#bac2c1;width: 700px;">{!! str_limit(strip_tags(Michelf\Markdown::defaultTransform($topic->details)) ,200) !!}</p>
      </div>
      <div class="clearfix"></div>
    </div>
  

    <div class="postinfo pull-left">
      <hr> 
      <div class="comments">
        <div class="commentbg">

          <a href="{{route('topic.show',$topic->id).'#lf_comments_wrap'}}"> Issues Posted &nbsp; {{$topic->comments->count()}}</a>
        </div>
      </div>

      <?php
      use App\Models\Comment;
      $commentsPendCount = Comment::where('commentable_id', $topic->id)
      ->where('status', "=", 0)->count();

      $commentsCompCount = Comment::where('commentable_id', $topic->id)
      ->where('status', "=", 1)->count(); ?>

      <p class="time">Total Issues: {{$topic->comments->count()}}</p>
      <p class="time">Incomplete : {{$commentsPendCount}}</p>
      <p class="time">Fixed: {{$commentsCompCount}}</p>
    </div>

  </div>
  <div class="card-header">
    <div class=""><i class="fa fa-clock-o"></i> {{ $topic->created_at->diffForHumans()}}</div>
    <ul class="nav nav-tabs">

    </ul>
  </div>