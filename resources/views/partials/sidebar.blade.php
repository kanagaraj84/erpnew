<div class="col-lg-4">

    <a href="#EmailIssueForm" data-toggle="modal" data-href="{{url('topic/create')}}">
        Create Email Issue <i class="fa fa-plus-circle" style="font-size:24px;"></i></a>
     <div class="sidebarblock card card-chart" id="lf_user_active_topics" style="padding: 9px;">
        <h3>{{__('My Active Threads')}}</h3>
        @forelse($usertopics as $usertopic)
            <div class="blocktxt">
                <a href="{{route('topic.show',$usertopic->id)}}">{{$usertopic->title}}</a>
            </div><br />
            <div class="divline"></div>
        @empty
            <div class="blocktxt">
                <p>{{__('You have no active topics')}}</p>
            </div>
            <div class="divline"></div>
        @endforelse
    </div>

</div>