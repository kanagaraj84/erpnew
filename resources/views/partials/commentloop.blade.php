<div class="col-12">
    <div class="card card-chart" style="margin-top:15px; padding:15px; background-color:#1d1e2c; border-radius: 25px;">
        <div class="" id="commentno{{$comment->id}}">
          <?php $imgpath = URL::asset("/images/Media/" .$comment->user->image_path); ?>
          <p>&nbsp;</p>
          <div class="dropdown">
            <button type="button" class="btn btn-link dropdown-toggle btn-icon" data-toggle="dropdown">
                <i class="tim-icons icon-settings-gear-63"></i>
            </button>
              <p>&nbsp;</p>
              <?php
              $fpath = url('/comment_documents/'.$comment->id."/");
              $dir_path = public_path() . '/comment_documents/'.$comment->id.'/';
              $dir_path_all = public_path() . "/comment_documents/".$comment->id."/*";
              $fileList = glob($dir_path_all);
              $total = count($fileList);
              if($total>0){
                  echo '<p>&nbsp;</p> <p> <i class="fa fa-file-o"></i> Attachments </p>';
                  $dir = new DirectoryIterator($dir_path);
                  $i=1;
                  foreach ($dir as $fileinfo) {
                      if (!$fileinfo->isDot()) {
                          echo " <a href='".$fpath."/".$fileinfo."' class='btn btn-primary btn-sm' target='_blank' align='center'>File ".$i."</a> &nbsp; &nbsp;";
                          $i++;
                      }
                  }
              } else {
                  echo '<p><i class="fa fa-file-o"></i> No Attachments</p>';
              } ?>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                <p> &nbsp;&nbsp; <button class="btn btn-primary btn-sm" type="button"title="{{__('Edit')}}" data-toggle="modal" data-target="#lf_comment_edit_modal{{$comment->id}}">
                Edit</button></p>
                <form action="{{route('comment.destroy',$comment->id)}}" method="post">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <input type="hidden" name="topic_id" value="{{$topic->id}}">
                    &nbsp;&nbsp; <button class="btn btn-primary btn-sm" type="submit" onclick="return confirm('{{$sureDelete}}')">Delete</button>
                </form>
            </div>
        </div>
        @include('partials.commentedit')
        <div class="">
            @if($comment->user->image_path)
            <div class="">
               &nbsp; <a href="{{route('users.users',$comment->user->id)}}" style="text-decoration: none;"><img src="{{$imgpath}}" width="80" height="80" alt="{{$comment->user->name}}"> &nbsp;&nbsp; <strong class="lf_commenter_name">&nbsp;&nbsp;{{$comment->user->name .' '}} <span>{{ __('said')}}</span></strong></a>
           </div>
           @else
           <div class="">
            <a href="{{route('users.users',$comment->user->id)}}">&nbsp;&nbsp; <img src="{{ Gravatar::fallback(url
                ('uploads/avater.png'))->get($comment->user->email) }}" alt="{{$comment->user->name}}">&nbsp;&nbsp; <strong class="lf_commenter_name">{{$comment->user->name .' '}} <span>{{ __('said')}}</span></strong></a>
            </div>
            @endif
            <p>&nbsp;&nbsp;  {!! Michelf\Markdown::defaultTransform(strip_tags($comment->body))  !!}
                <span class=" pull-left"><i class="fa fa-clock-o"></i> {{ $comment->created_at->diffForHumans()}} &nbsp;
                    @if(Auth::check())
                    @include('partials.replyform')
                    @endif
                </span>
            </p>
            @if($comment->comments()->count() > 0)
            <span> &nbsp; | </span>
            <a href="#lf_reply{{$comment->id}}" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="lf_reply"> {{__('View Replies '.$comment->comments()->count()) }}</a>
            @endif
            <span class="pull-right ">
                @if($comment->status==1)
                <button type="submit" class="btn btn-primary btn-sm">Fixed</button>
                @else        <form    action="{{route('comment.solve',$comment->id)}}" method="post">
                    {{csrf_field()}}
                    {{method_field('PATCH')}}
                    <input type="hidden" name="comment_id" value="{{$comment->id}}">
                    <input type="hidden" name="topic_id" value="{{$topic->id}}">
                    <input type="hidden" name="status" value="1">
                    <button type="submit"  class="btn btn-primary btn-sm" onclick="return confirm('{{$sureSolved}}')">Mark as Solved</button>
                </form>
                @endif
            </span>>
        </div>
        <div class="collapse" id="lf_reply{{$comment->id}}">
            @foreach($comment->comments as $reply)
            @include('partials.replyloop')
            @endforeach
        </div>
    </div>
</div>
</div>
