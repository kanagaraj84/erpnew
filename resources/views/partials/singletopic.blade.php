<div class="post beforepagination" style="padding: 20px;">
    <div class="topwrap">
        <div class="userinfo pull-right">
            <div class="lf_icons">
                <div class="lf_edit">
                    {{--<a href="{{route('topic.edit',$topic->id)}}" class='btn btn-primary btn-sm' title="{{__('Edit')}}">Edit</a>--}}
                    </div>
                </div>
            </div>
            <div class="posttext pull-left" style="margin-left: 10px;">
                <h2 class="lf_topic_title">{{$topic->title}}</h2>
                <p class="posttext">{!! Michelf\Markdown::defaultTransform(strip_tags($topic->details))  !!}</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="postinfobot">

            <div class="likeblock pull-left">
                <a href="#lf_comments_wrap" class="up"><i class="fa fa-comment"></i> &nbsp;{{$topic->comments()->count()}}</a>
            </div>

            <div class="next pull-right">
                <i class="fa fa-clock-o"></i>  {{ $topic->created_at->diffForHumans()}}
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="pull-right">

            <?php
            $fpath = url('/topic_documents/'.$topic->id."/");
            $dir_path = public_path() . '/topic_documents/'.$topic->id.'/';
            $dir_path_all = public_path() . "/topic_documents/".$topic->id."/*";
            $fileList = glob($dir_path_all);
            $total = count($fileList);
            if($total>0){
                echo '<p><i class="fa fa-file-o"></i> Attachments </p>';
                $dir = new DirectoryIterator($dir_path);
                $i=1;
                foreach ($dir as $fileinfo) {
                    if (!$fileinfo->isDot()) {
                        echo " <a href='".$fpath."/".$fileinfo."' class='btn btn-primary btn-sm' target='_blank'>File ".$i."</a>  &nbsp; &nbsp;";
                        $i++;
                    }
                }
            } else {
                echo '<p><i class="fa fa-file-o"></i> No Attachments </p>';
            } ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <br>
    <br>
    <br>