<?php $subject instanceof \App\Models\Task ? $instance = 'task' : $instance = 'lead' ?>

<div class="panel panel-primary shadow">
    <div class="panel-heading" style="margin-bottom: 20px;">

        @if($instance == 'task')
                <?php
                $fpath = url('/task_documents/'.$subject->id."/");
                $dir_path = public_path() . '/task_documents/'.$subject->id.'/';
                $dir_path_all = public_path() . "/task_documents/".$subject->id."/*";
                $fileList = glob($dir_path_all);
                $total = count($fileList);
                if($total>0){
                    echo '<p>&nbsp;</p> <p> <i class="fa fa-file-o"></i> Attachments </p>';
                    $dir = new DirectoryIterator($dir_path);
                    $i=1;
                    foreach ($dir as $fileinfo) {
                        if (!$fileinfo->isDot()) {
                            echo " <a href='".$fpath."/".$fileinfo."' class='btn btn-primary btn-sm' target='_blank' align='center'>File ".$i."</a> &nbsp; &nbsp;";
                            $i++;
                        }
                    }
                } else {
                    echo '<p><i class="fa fa-file-o"></i> No Attachments</p>';
                } ?>
        @else
                <?php
                $fpath = url('/project_documents/'.$lead->id."/");
                $dir_path = public_path() . '/project_documents/'.$lead->id.'/';
                $dir_path_all = public_path() . "/project_documents/".$lead->id."/*";
                $fileList = glob($dir_path_all);
                $total = count($fileList);
                if($total>0){
                    echo '<p>&nbsp;</p> <p> <i class="fa fa-file-o"></i> Attachments </p>';
                    $dir = new DirectoryIterator($dir_path);
                    $i=1;
                    foreach ($dir as $fileinfo) {
                        if (!$fileinfo->isDot()) {
                            echo " <a href='".$fpath."/".$fileinfo."' class='btn btn-primary btn-sm' target='_blank' align='center'>File ".$i."</a> &nbsp; &nbsp;";
                            $i++;
                        }
                    }
                } else {
                    echo '<p><i class="fa fa-file-o"></i> No Attachments</p>';
                } ?>
        @endif

    </div>

    <div class="panel-body" style="padding-left: 10px;">
        <h4>Comment Section</h4>
    </div>
</div>

<?php $count = 0;?>
<?php $i = 1 ?>
@foreach($subject->comments as $comment)
<div class="panel panel-primary" style="margin-top:15px; padding:15px; background-color:#1d1e2c; border-radius: 25px;">
    <div class="panel-body">
        <p class="smalltext">#{{$i++}}</p>
        <p>  {{ $comment->description }}</p>
        <p class="smalltext">{{ __('Comment by') }}: <a
            href="{{route('users.show', $comment->user->id)}}"> {{$comment->user->name}} </a>
        </p>
        <p class="smalltext">{{ __('Created at') }}:
            {{ date('d F, Y, H:i:s', strtotime($comment->created_at))}}
            @if($comment->updated_at != $comment->created_at)
            <br/>{{ __('Modified') }} : {{date('d F, Y, H:i:s', strtotime($comment->updated_at))}}
        @endif</p>
    </div>
</div>
@endforeach
<br/>

@if($instance == 'task')
<div class="panel panel-primary shadow pull-right">
</div>
{!! Form::open(array('url' => array('/comments/task',$subject->id, ))) !!}
<div class="form-group" style = "color: #fff;">
    {!! Form::textarea('description', null, ['id' => 'comment-field', 'class' => 'form-control', 'style' => 'color:#fff;', 'cols' => '100', 'rows' => '5', 'required' => 'required']) !!}

    {!! Form::submit( __('Add Comment') , ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!}
@else
<div class="panel panel-primary shadow pull-right">
</div>
{!! Form::open(array('url' => array('/comments/lead',$lead->id, ))) !!}
<div class="form-group" style = "color: #fff;" >
    {!! Form::textarea('description', null, ['id' => 'comment-field', 'class' => 'form-control', 'style' => 'color:#fff;', 'cols' => '100', 'rows' => '5', 'required' => 'required']) !!}
    {!! Form::submit( __('Add Comment') , ['class' => 'btn btn-primary']) !!}
</div>
{!! Form::close() !!}
@endif

@push('scripts')
<script>
    $('#comment-field').atwho({
        at: "@",
        limit: 5,
        delay: 400,
        callbacks: {
            remoteFilter: function (t, e) {
                t.length <= 2 || $.getJSON("/users/users", {q: t}, function (t) {
                    e(t)
                })
            }
        }
    })
</script>
@endpush