<div class="post" id="lf_comment_create_form">
    @if (Session::has('commentcreateerror'))
        <div class="alert alert-danger">{{ Session::get('commentcreateerror') }}</div>
    @endif

    <form action="{{route('topic.comment.create',$topic->id)}}" class="form" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('POST')}}
        <div class="postinfobot">
            <textarea id="body" rows="5" cols="108" name="body"></textarea>
            <div class="clearfix"></div>
                <div class=" postreply pull-left">
                    <input type="file" name="uploaded_files[]" multiple>
                </div>
            <div class="clearfix"></div>
            <div class="pull-right postreply">
                <div class="pull-left"><button type="submit" class="btn btn-primary">{{__('Post Email Issue')}}</button></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</div>